## Description of the changes

<!--
Indicate the content of the MR at high level.
If a detailed description is needed, move it to a separate paragraph.

Please link to the issues/milestones addressed in this merge request

If it closes an issue, please indicate by writing `Close #<ISSUE_NUMBER>` which allows automatic issue resolution.
-->

### Check-list

- [ ] Make sure that a changelog entry is present
- [ ] Label the MR according to the content
- [ ] If this MR has a dependency on another MR, specify the order in which the MRs should be merged
- [ ] If the MR is done, please mark it as such by using the ~"Done - awaits merge" label, else mark it as "Draft: " by adding the prefix to the title
- [ ] Designate a reviewer for this MR once done
- [ ] Update the supported features and limitations [here](https://gitlab.cern.ch/tmrg/tmrg/-/blob/master/doc/source/triplication.rst)

### Reviewer check-list

- [ ] Make sure that the title describes the changes in the merge request
- [ ] Make sure that the description of the changes is relevant and it describes all the changes
- [ ] Make sure that the changelog entry is consistent with the changes.
If the first release on top was released, then make sure that a new running release is created incrementing the minor index (the version is in the format `major.minor.patch`)

<!--
Base labels.
Add with
/label ~<NAME OF THE LABEL>
-->
