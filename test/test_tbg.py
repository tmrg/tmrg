import os
import pytest
from .common import *
from tmrg.tbg import main as tbg_main

@pytest.fixture
def tbg(monkeypatch):
    return CliExecutor(monkeypatch, "tbg", main_function=tbg_main)

def test_tbg_no_arguments(tbg, capfd):
    assert tbg()
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["You have to specify verilog file name"])

def test_tbg_missing_file(tbg, capfd):
    assert tbg(["not_existing.v"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["File or directory does not exists"])

def test_tbg_simple_verilog(tbg, capsys):
    assert not tbg([file_in_test_dir("verilog/always.v")])
    assert_output_streams(capsys, expect_stdout_empty=False)

def test_tbg_multiple_top_files(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/topModule.v")])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_stderr_empty=False, expect_in_stderr=["The design has multiple top cells"])

def test_tbg_multiple_top_modules_specify(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/topModule.v"), "--top-module=m1"])
    assert_output_streams(capfd, expect_stdout_empty=False)

def test_tbg_include(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/include.v"), "--include", "--inc-dir", file_in_test_dir("verilog")])
    assert_output_streams(capfd, expect_stdout_empty=False)

def test_tbg_include_lib(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/libtest.v"), "--lib", file_in_test_dir("verilog/lib.v")])
    assert_output_streams(capfd, expect_stdout_empty=False)

def test_tbg_include_tmr_lib(tbg, capfd):
    assert not tbg([file_in_test_dir("systemverilog/use_lib.sv"), "--lib", file_in_test_dir("systemverilog/libTMR.sv")])
    assert_output_streams(capfd, expect_stdout_empty=False)

def test_tbg_stats(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/fsm01.v"), "--stats"])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["Total number of files parsed:", "Total number of lines parsed"])

def test_tbg_dut_name(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/tmrerr.v"), "--dut-name=dut"])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["test dut (","testTMR dut ("])

def test_tbg_dut_name_default(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/tmrerr.v")])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["test DUT (","testTMR DUT ("])

def test_tbg_tb_name(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/tmrerr.v"), "--tb-name=tb"])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["module tb;"])

def test_tbg_testbench_name_default(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/tmrerr.v")])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["module test_test;"])

def test_tbg_output_file(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/tmrerr.v"), "--o=tb.sv"])
    assert_output_streams(capfd)
    assert os.path.isfile('tb.sv')

def test_tbg_output_file_long(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/tmrerr.v"), "--output-file=tb.sv"])
    assert_output_streams(capfd)
    assert os.path.isfile('tb.sv')

def test_tbg_no_error(tbg, capfd):
    assert not tbg([file_in_test_dir("verilog/module_in_module.v")])
    assert_output_streams(capfd, expect_stdout_empty=False, forbid_in_stderr=["Unknown module instantiation! In module 'm1', instance name 'm2_inst' instance type 'm2'."])


class TestTbgOnSingleVerilogFile():
    @pytest.mark.parametrize(
        'verilog_file', [
            "verilog/comb00.v",
            "verilog/params6.v",
            "verilog/params7.v",
        ]
    )

    def test_tbg_on_file(self, tbg, capfd, verilog_file):
      basefile = file_in_test_dir(verilog_file)
      syntax_check(basefile, cmds=["iverilog"])
      result = tbg([basefile])
      assert result==False
      out, err = capfd.readouterr()
      tb_file = basefile.replace(".v", "_tb.v")
      assert bool(err)==False, "stderr_lenght:%d stdout_lenght:%d" % (len(err), len(out)) + err
      with open(tb_file, "w") as fout:
          fout.write(out)
      syntax_check(basefile, cmds=["iverilog %s" % tb_file])

class TestTbgOnSingleSystemVerilogFile():
    @pytest.mark.parametrize(
        'verilog_file', [
            "systemverilog/tbg_arrays.sv",
        ]
    )

    def test_tbg_on_file(self, tbg, capfd, verilog_file):
      basefile = file_in_test_dir(verilog_file)
      syntax_check(basefile, cmds=["iverilog -g2012"])
      x = tbg([file_in_test_dir(verilog_file)])
      out, err = capfd.readouterr()
      tb_file = file_in_test_dir(verilog_file).replace(".sv", "_tb.sv")
      assert bool(err)==False, "stderr_lenght:%d stdout_lenght:%d" % (len(err), len(out)) + err
      with open(tb_file, "w") as fout:
          fout.write(out)
      syntax_check(basefile, cmds=["iverilog -g2012 %s" % tb_file])
