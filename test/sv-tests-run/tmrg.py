#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from BaseRunner import BaseRunner


class tmrg(BaseRunner):
    def __init__(self):
        super().__init__(
            'tmrg', 'tmrg', {
                'parsing', 'elaboration'
            })

        self.submodule = ""
        self.url = "https://gitlab.cern.ch/tmrg/tmrg"

    def prepare_run_cb(self, tmp_dir, params):
        self.cmd = [self.executable]

        if params['top_module'] != '':
            self.cmd += ['--top_module=%s' + params['top_module']]

        for incdir in params['incdirs']:
            self.cmd.append('--inc-dir=' + incdir)

        self.cmd += params['files']

    def get_version_cmd(self):
        return [self.executable, "--version"]
