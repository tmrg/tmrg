#!/bin/bash

set -e

cd "$(dirname "$0")/sv-tests"
make -f ../sv-tests/Makefile RUNNERS_DIR=../sv-tests-run/ RUNNERS=tmrg
