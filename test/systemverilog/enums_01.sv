typedef enum logic[1:0] {A=2'b01, B=2'b10, C=2'b11} ciao_t;

typedef struct packed { logic a; } s_t;

module tb (
	input wire ciao_t e,
	input logic x,
	input wire s_t s,
	output wire test
);

typedef enum logic[2:0] {X=3'b01, Y=3'b10, W=3'b11, Z} mamma_t;

ciao_t miao;
mamma_t bella;

ciao_t miaoVoted = miao;

logic E;

initial begin
	bella = Z;
	E = bella | x & s.a;
	$display("size of ciao_t: %d", $size(miao));
end

always_comb begin
	miao = (miaoVoted == A) ? B : C;
end

endmodule
