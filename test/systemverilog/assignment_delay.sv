module test;
  wire a,b,c;

  assign #(1:2:3) a = b;
  assign #2 c = b; 
endmodule
