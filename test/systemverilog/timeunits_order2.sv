module test();
  // tmrg default triplicate
  timeunit 1ps / 1ps;
  logic [4:0] divider_counts;
  wire  [4:0] divider_countsVoted = divider_counts;
endmodule
