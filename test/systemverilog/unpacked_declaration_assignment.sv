module a (
	input logic b[5]
);

localparam C = 5;

wire d[C] = b;

endmodule
