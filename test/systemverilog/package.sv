package ciao;
localparam int PROVA = 1;
typedef struct packed {
	logic a;
	logic b;
} c_t;

typedef struct packed {
	c_t c;
	logic d;
} e_t;
endpackage

module a;

// tmrg default triplicate

wire [ciao::PROVA-1:0] test;

ciao::e_t try_this;
wire ciao::e_t try_thisVoted = try_this;

endmodule
