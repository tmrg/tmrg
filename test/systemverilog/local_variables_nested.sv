module test;
  logic enable;
  logic [1:0] tot;
  logic [1:0] toa;
  logic [1:0] tot0;
  logic [1:0] toa0;

  typedef struct packed {
    logic [1:0] tot;
    logic [1:0] toa;
  } test_t;

  typedef struct packed {
    logic [1:0] tot;
  } test1_t;

  always_comb begin
    if (enable) begin : toa_and_tot
      test_t ch_payload;
      ch_payload.toa = toa;
      ch_payload.tot = tot;

    end else begin : toa_only
      test1_t ch_payload;
      ch_payload.tot = tot0;
    end
  end
endmodule

