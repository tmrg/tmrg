module dut;

logic [9:0][9:0] multidim;

always_comb begin
	static int ciao = 0;

	for(int i=ciao; i<10; i++) begin
		automatic int i2 = 10 - 1 - i;

		multidim[i][i2] = 1'b1;
	end
end

endmodule
