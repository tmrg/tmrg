
module dut;

// tmrg default triplicate
// tmrg do_not_triplicate b

logic a, b;

always_comb begin
	{a, b} = {b, 1'b0};
end

endmodule
