module tb;

wire ciao0, ciao1;
logic ciao_is_0;
logic [5:0] done;

always_comb begin
	priority case ({ciao0, ciao1})
		2'b00: done[0] = 1'b1;
		2'b1z: done[0] = 1'b0;
	endcase
end

always_comb begin
	unique case ({ciao0, ciao1})
		2'b00: done[1] = 1'b1;
		2'b11: done[1] = 1'b0;
	endcase
end

always_comb begin
	priority casez ({ciao0, ciao1})
		2'b00: done[2] = 1'b1;
		2'b1z: done[2] = 1'b0;
	endcase
end

always_comb begin
	unique casez ({ciao0, ciao1})
		2'b00: done[3] = 1'b1;
		2'b11: done[3] = 1'b0;
	endcase
end

always_comb begin
	priority casex ({ciao0, ciao1})
		2'b00: done[4] = 1'b1;
		2'b1z: done[4] = 1'b0;
	endcase
end

always_comb begin
	unique casex ({ciao0, ciao1})
		2'b00: done[5] = 1'b1;
		2'b11: done[5] = 1'b0;
	endcase
end

endmodule
