module test #(
  parameter int BITS = 'd32,
  localparam int BITS_DDR_ADDR = $clog2(BITS / 2)
  )();
endmodule
