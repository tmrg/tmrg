package test_pkg;
	localparam int X = 1;
endpackage

package other_pkg;
	localparam int Y = 1;

	typedef enum logic [4:0] {
			ONE = 1'b1,
			ZERO = 1'b0
	} bool_t;
endpackage

module a import test_pkg::*; (
	input wire [X-1:0] p,
	output other_pkg::bool_t x,
	output wire other_pkg::bool_t y
);

//tmrg default triplicate
//tmrg do_not_triplicate y

assign y = x;

endmodule

