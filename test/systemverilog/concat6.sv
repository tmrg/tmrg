typedef struct packed {
	logic a;
	logic b;
} struct_t;

module tb;

//tmrg default triplicate
// tmrg do_not_triplicate a

wire struct_t a, b, c;

assign {c, b} = {2 {a}};

endmodule
