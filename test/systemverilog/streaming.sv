module test_abc #(
    parameter BITS = 16
) (
    input  wire [BITS-1:0] vector_i,
    output wire [BITS-1:0] first_one_o,
    output wire [BITS-1:0] next_mask_o
);


endmodule

module test #(
    parameter OUTPUTS = 8
);
  wire [OUTPUTS-1:0] last_available;
  wire [OUTPUTS-1:0] last_available_8;
  wire [OUTPUTS-1:0] last_available_reversed;
  test_abc #(
      .BITS(OUTPUTS)
  ) find_output (
      .vector_i   ({<<OUTPUTS{out_available_i}}),
      .first_one_o(last_available_reversed),
      .next_mask_o()
  );

  assign last_available_8 = {<<8 {last_available_reversed}};
  assign last_available = {<< {last_available_reversed}};

endmodule
