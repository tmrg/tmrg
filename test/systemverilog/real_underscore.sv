module test #(parameter real FREQ = 1280.0);
  localparam real O = 1_000_000.0 / FREQ;
  real A = 1_000.123_456;
endmodule
