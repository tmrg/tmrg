module b
  (output logic a,
   input logic b,
   input logic c,
   input logic d,
   input logic clk,
   output logic e
   );
  assign a = b + (* mode = "cla" *) c; // sets the value for the attribute mode
                                       // to be the string cla.
  always_ff @(posedge clk) begin
    e <= (b ? (* no_glitch *) c : d);
  end
endmodule
