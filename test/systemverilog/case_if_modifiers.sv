module tb;

wire [1:0] ciao;
logic ciao_is_0;
logic done;

always_comb begin
	priority if (ciao == 0)
		ciao_is_0 = 1'b1;
	else if (ciao == 1)
		ciao_is_0 = 1'b0;
end

always_comb begin
	unique if (ciao == 0);
	else if (ciao == 1)
		ciao_is_0 = 1'b0;
end

always_comb begin
	unique0 if (ciao == 0);
	else if (ciao == 1);
end

always_comb begin
	priority case (ciao)
		2'b00: done = 1'b1;
		2'b1z: done = 1'b0;
	endcase
end

always_comb begin
	unique case (ciao)
		2'b00: done = 1'b1;
		2'b11: done = 1'b0;
	endcase
end

always_comb begin
	unique0 case (ciao)
		2'b00: done = 1'b1;
		default: done = 1'b0;
	endcase
end

endmodule
