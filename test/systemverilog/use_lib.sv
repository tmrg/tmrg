module dut (clock, reset, data);

input wire clock;
input wire reset;
output wire data;

lib lib_i (
  .clock(clock),
  .reset(reset),
  .data(data)
);

endmodule
