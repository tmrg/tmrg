module test;
  logic [31:0] outputs;
  logic [2:0] selected_inputs;

  generate
    for (genvar i = 0; i <= 4; i++)
      assign outputs[i] = i * 4;
  endgenerate

  always_comb
    for (int i=0; i<3; i++)
      selected_inputs[i] = 1'b1;
endmodule
