module a(input clk, input reset);endmodule

module b();
  logic clk;
  logic rst;

  (* optimize_power=0 *)
  a synth1 (.clk(clk),.reset(rst));

  (* fsm_state *) logic [7:0] state1;
  (* fsm_state=1 *) logic [3:0] state2, state3;
  logic [3:0] reg1;                   // reg1 does NOT have fsm_state set
  (* fsm_state=0 *) logic [3:0] reg2; // nor does reg2
endmodule:b
