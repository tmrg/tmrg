module translate (
	input wire clock,
	input wire d
);
// tmrg translate off
wire this_will_not_be_in_the_translated_file;
// tmrg translate on

reg d_sampled;
always @(posedge clock)
	d_sampled <= d;

// tmrg copy start
`ifdef FORMAL
`ifndef TMR
	assert property (@(posedge clock) d |=> d_sampled);
`else
// tmrg ignore start
//this will be removed from the copy!
// tmrg ignore stop
	assert property (@(posedge clockA) dA |=> d_sampledA);
	assert property (@(posedge clockB) dB |=> d_sampledB);
	assert property (@(posedge clockC) dC |=> d_sampledC);
`endif
`endif
// tmrg copy stop

// tmrg ignore start
// tmrg copy start
initial $display("i'm afraid this will not be copied either");
// tmrg copy stop
// tmrg ignore stop
endmodule
