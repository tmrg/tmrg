module test (
  input wire [4:0][4:0] assign_matrix_i,
  input  wire  [4:0][7:0] in_data_i,
  output logic [4:0][7:0] out_data_o);
  always_comb begin
    out_data_o = 'h0;

    for (int out = 0; out < 5; out++)
    for (int in = 0; in < 5; in++)
    if (assign_matrix_i[out][in]) begin
      out_data_o[out] = in_data_i[in];
      break;
    end
  end
endmodule
