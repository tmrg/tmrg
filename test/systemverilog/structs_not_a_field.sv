typedef struct packed { logic a; } a_t;

module dut;

wire a_t a;
wire ok = a.a;
wire ko = a.b;

endmodule
