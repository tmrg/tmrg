module paramtest6
#(
  parameter int ADDR_WIDTH  = 16,
  parameter int DATA_WIDTH  = 16,
  parameter bit [DATA_WIDTH-1:0] RESET_VALUE = {DATA_WIDTH{1'b0}}
)
(
  input                  clk,
  input                  rst,
  input [ADDR_WIDTH-1:0] this_reg_addr,
  input [ADDR_WIDTH-1:0] address_in,
  input                  we,
  input [DATA_WIDTH-1:0] data_in,
  output[DATA_WIDTH-1:0] data_out
);
endmodule
