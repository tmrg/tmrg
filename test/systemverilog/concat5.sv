typedef struct packed {
	logic a;
	logic b;
} struct_t;

module tb;

//tmrg default triplicate
// tmrg do_not_triplicate a

struct_t a, b, c;

always begin
	{c, b} = {a, 2'b0};
end

endmodule
