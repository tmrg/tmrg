module tb;

//tmrg default triplicate
// tmrg do_not_triplicate a

logic a, b, c;
always begin
	{c, b} = {a, 1'b0};
end

endmodule
