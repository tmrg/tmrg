module translateTMR (
  input wire clockA,
  input wire clockB,
  input wire clockC,
  input wire dA,
  input wire dB,
  input wire dC
);
reg d_sampledA;
reg d_sampledB;
reg d_sampledC;

always @( posedge clockA )
  d_sampledA <= dA;

always @( posedge clockB )
  d_sampledB <= dB;

always @( posedge clockC )
  d_sampledC <= dC;
`ifdef FORMAL
`ifndef TMR
  assert property (@(posedge clock) d |=> d_sampled);
`else
assert property (@(posedge clockA) dA |=> d_sampledA);
  assert property (@(posedge clockB) dB |=> d_sampledB);
  assert property (@(posedge clockC) dC |=> d_sampledC);
`endif
`endif

endmodule

