module testTMR (
    input logic resetA,
    input logic resetB,
    input logic resetC,
    input logic [7:0] rwmemA[5],
    input logic [7:0] rwmemB[5],
    input logic [7:0] rwmemC[5]
);
  wor rwmemTmrErrorC;
  wor rwmemTmrErrorB;
  wor rwmemTmrErrorA;
  wire [7:0] rwmemVotedA[5];
  wire [7:0] rwmemVotedB[5];
  wire [7:0] rwmemVotedC[5];
  logic Clk_gated_enableA;
  logic Clk_gated_enableB;
  logic Clk_gated_enableC;
  assign Clk_gated_enableA = resetA || rwmemTmrErrorA;
  assign Clk_gated_enableB = resetB || rwmemTmrErrorB;
  assign Clk_gated_enableC = resetC || rwmemTmrErrorC;
  genvar gen_rwmemVoterA0;

  generate
    for (
        gen_rwmemVoterA0 = 0; gen_rwmemVoterA0 <= 4; gen_rwmemVoterA0 = gen_rwmemVoterA0 + 1
    ) begin : gen_rwmemVoterA0_fanout

      majorityVoter #(
          .WIDTH(8)
      ) rwmemVoterA (
          .inA(rwmemA[gen_rwmemVoterA0]),
          .inB(rwmemB[gen_rwmemVoterA0]),
          .inC(rwmemC[gen_rwmemVoterA0]),
          .out(rwmemVotedA[gen_rwmemVoterA0]),
          .tmrErr(rwmemTmrErrorA)
      );
    end

  endgenerate
  genvar gen_rwmemVoterB0;

  generate
    for (
        gen_rwmemVoterB0 = 0; gen_rwmemVoterB0 <= 4; gen_rwmemVoterB0 = gen_rwmemVoterB0 + 1
    ) begin : gen_rwmemVoterB0_fanout

      majorityVoter #(
          .WIDTH(8)
      ) rwmemVoterB (
          .inA(rwmemA[gen_rwmemVoterB0]),
          .inB(rwmemB[gen_rwmemVoterB0]),
          .inC(rwmemC[gen_rwmemVoterB0]),
          .out(rwmemVotedB[gen_rwmemVoterB0]),
          .tmrErr(rwmemTmrErrorB)
      );
    end

  endgenerate
  genvar gen_rwmemVoterC0;

  generate
    for (
        gen_rwmemVoterC0 = 0; gen_rwmemVoterC0 <= 4; gen_rwmemVoterC0 = gen_rwmemVoterC0 + 1
    ) begin : gen_rwmemVoterC0_fanout

      majorityVoter #(
          .WIDTH(8)
      ) rwmemVoterC (
          .inA(rwmemA[gen_rwmemVoterC0]),
          .inB(rwmemB[gen_rwmemVoterC0]),
          .inC(rwmemC[gen_rwmemVoterC0]),
          .out(rwmemVotedC[gen_rwmemVoterC0]),
          .tmrErr(rwmemTmrErrorC)
      );
    end

  endgenerate
endmodule

