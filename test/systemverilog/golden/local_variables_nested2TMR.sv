module testTMR (
  input wire [4:0] [4:0] assign_matrix_iA,
  input wire [4:0] [4:0] assign_matrix_iB,
  input wire [4:0] [4:0] assign_matrix_iC,
  input wire [4:0] [7:0] in_data_iA,
  input wire [4:0] [7:0] in_data_iB,
  input wire [4:0] [7:0] in_data_iC,
  output logic [4:0] [7:0] out_data_oA,
  output logic [4:0] [7:0] out_data_oB,
  output logic [4:0] [7:0] out_data_oC
);

always_comb
  begin
    out_data_oA = 'h0;
    for(int out = 0; out<5; out++)
      for(int in = 0; in<5; in++)
        if (assign_matrix_iA[out][in])
          begin
            out_data_oA[out] = in_data_iA[in];
            break;
          end

  end

always_comb
  begin
    out_data_oB = 'h0;
    for(int out = 0; out<5; out++)
      for(int in = 0; in<5; in++)
        if (assign_matrix_iB[out][in])
          begin
            out_data_oB[out] = in_data_iB[in];
            break;
          end

  end

always_comb
  begin
    out_data_oC = 'h0;
    for(int out = 0; out<5; out++)
      for(int in = 0; in<5; in++)
        if (assign_matrix_iC[out][in])
          begin
            out_data_oC[out] = in_data_iC[in];
            break;
          end

  end
endmodule

