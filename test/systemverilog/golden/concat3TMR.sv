module tbTMR;
wor aTmrError;
wire a;
logic b;
logic c;
logic aA;
logic aB;
logic aC;

always
  begin
    {c, b} = {b, a};
  end

majorityVoter aVoter (
    .inA(aA),
    .inB(aB),
    .inC(aC),
    .out(a),
    .tmrErr(aTmrError)
    );
endmodule

