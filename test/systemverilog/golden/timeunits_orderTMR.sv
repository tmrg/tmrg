module testTMR;
timeunit 1ps;
timeprecision 1ps;
wor divider_countsTmrErrorC;
wire [4:0] divider_countsVotedC;
wor divider_countsTmrErrorB;
wire [4:0] divider_countsVotedB;
wor divider_countsTmrErrorA;
wire [4:0] divider_countsVotedA;
logic [4:0] divider_countsA;
logic [4:0] divider_countsB;
logic [4:0] divider_countsC;

majorityVoter #(.WIDTH(5)) divider_countsVoterA (
    .inA(divider_countsA),
    .inB(divider_countsB),
    .inC(divider_countsC),
    .out(divider_countsVotedA),
    .tmrErr(divider_countsTmrErrorA)
    );

majorityVoter #(.WIDTH(5)) divider_countsVoterB (
    .inA(divider_countsA),
    .inB(divider_countsB),
    .inC(divider_countsC),
    .out(divider_countsVotedB),
    .tmrErr(divider_countsTmrErrorB)
    );

majorityVoter #(.WIDTH(5)) divider_countsVoterC (
    .inA(divider_countsA),
    .inB(divider_countsB),
    .inC(divider_countsC),
    .out(divider_countsVotedC),
    .tmrErr(divider_countsTmrErrorC)
    );
endmodule

