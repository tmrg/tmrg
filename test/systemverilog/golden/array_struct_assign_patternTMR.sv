module xTMR;
typedef struct packed { 
  logic  a;
  logic  b;
} c_t;
typedef struct packed { 
  c_t  c;
  logic  d;
} e_t;
logic aA;
logic aB;
logic aC;
logic bA;
logic bB;
logic bC;
c_t cA = '{a: aA, b: bA&1'b0};
c_t cB = '{a: aB, b: bB&1'b0};
c_t cC = '{a: aC, b: bC&1'b0};
e_t tA = '{c: cA, d: 1, default: 'hx};
e_t tB = '{c: cB, d: 1, default: 'hx};
e_t tC = '{c: cC, d: 1, default: 'hx};
endmodule

