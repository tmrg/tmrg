module case_tbTMR;
  reg [3:0] aA = 0;
  reg [3:0] aB = 0;
  reg [3:0] aC = 0;
  reg [3:0] bA = 0;
  reg [3:0] bB = 0;
  reg [3:0] bC = 0;

  always @* begin
    case (aA) inside
      1, 3: bA = 1;
      4'b01??: bA = 2;
      10, [5 : 6]: bA = 3;
      default: bA = 4;
    endcase
  end

  always @* begin
    case (aB) inside
      1, 3: bB = 1;
      4'b01??: bB = 2;
      10, [5 : 6]: bB = 3;
      default: bB = 4;
    endcase
  end

  always @* begin
    case (aC) inside
      1, 3: bC = 1;
      4'b01??: bC = 2;
      10, [5 : 6]: bC = 3;
      default: bC = 4;
    endcase
  end
endmodule

