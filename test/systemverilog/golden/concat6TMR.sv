typedef struct packed { 
  logic  a;
  logic  b;
} struct_t;
module tbTMR;
wire struct_t aC;
wire struct_t aB;
wire struct_t aA;
wire struct_t a;
wire struct_t bA;
wire struct_t bB;
wire struct_t bC;
wire struct_t cA;
wire struct_t cB;
wire struct_t cC;
assign {cA, bA} = {2{aA}};
assign {cB, bB} = {2{aB}};
assign {cC, bC} = {2{aC}};

fanout #(.WIDTH(2)) aFanout (
    .in(a),
    .outA(aA),
    .outB(aB),
    .outC(aC)
    );
endmodule

