module testTMR;
wire aA;
wire aB;
wire aC;
wire bA;
wire bB;
wire bC;
wire cA;
wire cB;
wire cC;
assign #(1:2:3) aA = bA;
assign #(1:2:3) aB = bB;
assign #(1:2:3) aC = bC;
assign #2 cA = bA;
assign #2 cB = bB;
assign #2 cC = bC;
endmodule

