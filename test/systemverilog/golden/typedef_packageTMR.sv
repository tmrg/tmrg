package my_pkg;
  typedef struct packed { 
    logic  a;
    logic  b;
  } c_t;
    typedef c_t d_t;
endpackage

module eTMR import my_pkg::d_t;;
wor dTmrErrorC;
wire d_t dVotedC;
wor dTmrErrorB;
wire d_t dVotedB;
wor dTmrErrorA;
wire d_t dVotedA;
d_t dA;
d_t dB;
d_t dC;

majorityVoter #(.WIDTH(2)) dVoterA (
    .inA(dA),
    .inB(dB),
    .inC(dC),
    .out(dVotedA),
    .tmrErr(dTmrErrorA)
    );

majorityVoter #(.WIDTH(2)) dVoterB (
    .inA(dA),
    .inB(dB),
    .inC(dC),
    .out(dVotedB),
    .tmrErr(dTmrErrorB)
    );

majorityVoter #(.WIDTH(2)) dVoterC (
    .inA(dA),
    .inB(dB),
    .inC(dC),
    .out(dVotedC),
    .tmrErr(dTmrErrorC)
    );
endmodule

