package my_pkg;
    typedef logic [1:0] e_t;
endpackage

module mTMR #(
  parameter my_pkg::e_t c = 2'b10
);
parameter my_pkg::e_t d = 2'b11;
my_pkg::e_t bA = 2'b10;
my_pkg::e_t bB = 2'b10;
my_pkg::e_t bC = 2'b10;
assign bA = c;
assign bB = c;
assign bC = c;
endmodule

