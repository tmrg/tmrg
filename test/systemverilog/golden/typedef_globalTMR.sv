typedef logic [99:0] tipo_t;
typedef tipo_t [99:0] supertipo_t;
module aTMR;
wor aaTmrErrorC;
wire supertipo_t aaVotedC;
wor aaTmrErrorB;
wire supertipo_t aaVotedB;
wor aaTmrErrorA;
wire supertipo_t aaVotedA;
supertipo_t aaA;
supertipo_t aaB;
supertipo_t aaC;

majorityVoter #(.WIDTH(10000)) aaVoterA (
    .inA(aaA),
    .inB(aaB),
    .inC(aaC),
    .out(aaVotedA),
    .tmrErr(aaTmrErrorA)
    );

majorityVoter #(.WIDTH(10000)) aaVoterB (
    .inA(aaA),
    .inB(aaB),
    .inC(aaC),
    .out(aaVotedB),
    .tmrErr(aaTmrErrorB)
    );

majorityVoter #(.WIDTH(10000)) aaVoterC (
    .inA(aaA),
    .inB(aaB),
    .inC(aaC),
    .out(aaVotedC),
    .tmrErr(aaTmrErrorC)
    );
endmodule

