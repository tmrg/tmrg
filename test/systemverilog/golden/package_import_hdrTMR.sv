package test_pkg;
  localparam int X = 1;
endpackage

package other_pkg;
  localparam int Y = 1;
  typedef enum logic [4:0] {
    ONE  = 1'b1,
    ZERO = 1'b0
  } bool_t;
endpackage

module aTMR
  import test_pkg::*;
(
    input wire [X - 1:0] pA,
    input wire [X - 1:0] pB,
    input wire [X - 1:0] pC,
    output other_pkg::bool_t xA,
    output other_pkg::bool_t xB,
    output other_pkg::bool_t xC,
    output wire other_pkg::bool_t y
);
  wor xTmrError;
  wire other_pkg::bool_t x;
  assign y = x;

  majorityVoter #(
      .WIDTH(5)
  ) xVoter (
      .inA(xA),
      .inB(xB),
      .inC(xC),
      .out({x}),
      .tmrErr(xTmrError)
  );
endmodule

