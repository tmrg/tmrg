module tbTMR;
wire aA;
wire aB;
wire aC;
wire bA;
wire bB;
wire bC;
wire cA;
wire cB;
wire cC;
assign cA = unsigned' (aA)  + bA;
assign cB = unsigned' (aB)  + bB;
assign cC = unsigned' (aC)  + bC;
endmodule

