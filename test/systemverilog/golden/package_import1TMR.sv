package my_package;
  parameter TOT_COARSE_BITS = 12;
  parameter TOT_FINE_BITS = 5;
  typedef struct packed { 
    logic [TOT_COARSE_BITS - 1:0] coarse;
    logic [TOT_FINE_BITS - 1:0] fine;
  } bero_tot_t;
endpackage

package my_package2;
  typedef struct packed { 
    my_package::bero_tot_t tot;
  } test_t;
endpackage

module testTMR;
my_package2::test_t aA;
my_package2::test_t aB;
my_package2::test_t aC;
endmodule

