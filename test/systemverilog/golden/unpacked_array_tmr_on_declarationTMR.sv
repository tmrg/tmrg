module aTMR;
wor ciaoTmrErrorC;
wire ciaoVotedC [10];
wor ciaoTmrErrorB;
wire ciaoVotedB [10];
wor ciaoTmrErrorA;
wire ciaoVotedA [10];
wire ciaoA [10];
wire ciaoB [10];
wire ciaoC [10];
genvar gen_ciaoVoterA0;

generate
  for(gen_ciaoVoterA0 = 0; gen_ciaoVoterA0<=9; gen_ciaoVoterA0 = gen_ciaoVoterA0 + 1)
    begin : gen_ciaoVoterA0_fanout

      majorityVoter ciaoVoterA (
          .inA(ciaoA[gen_ciaoVoterA0]),
          .inB(ciaoB[gen_ciaoVoterA0]),
          .inC(ciaoC[gen_ciaoVoterA0]),
          .out(ciaoVotedA[gen_ciaoVoterA0]),
          .tmrErr(ciaoTmrErrorA)
          );
    end

endgenerate
genvar gen_ciaoVoterB0;

generate
  for(gen_ciaoVoterB0 = 0; gen_ciaoVoterB0<=9; gen_ciaoVoterB0 = gen_ciaoVoterB0 + 1)
    begin : gen_ciaoVoterB0_fanout

      majorityVoter ciaoVoterB (
          .inA(ciaoA[gen_ciaoVoterB0]),
          .inB(ciaoB[gen_ciaoVoterB0]),
          .inC(ciaoC[gen_ciaoVoterB0]),
          .out(ciaoVotedB[gen_ciaoVoterB0]),
          .tmrErr(ciaoTmrErrorB)
          );
    end

endgenerate
genvar gen_ciaoVoterC0;

generate
  for(gen_ciaoVoterC0 = 0; gen_ciaoVoterC0<=9; gen_ciaoVoterC0 = gen_ciaoVoterC0 + 1)
    begin : gen_ciaoVoterC0_fanout

      majorityVoter ciaoVoterC (
          .inA(ciaoA[gen_ciaoVoterC0]),
          .inB(ciaoB[gen_ciaoVoterC0]),
          .inC(ciaoC[gen_ciaoVoterC0]),
          .out(ciaoVotedC[gen_ciaoVoterC0]),
          .tmrErr(ciaoTmrErrorC)
          );
    end

endgenerate
endmodule

