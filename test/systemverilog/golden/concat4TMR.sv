module tbTMR;
wire aC;
wire aB;
wire aA;
logic a;
logic bA;
logic bB;
logic bC;
logic cA;
logic cB;
logic cC;

always
  begin
    {cA, bA} = {aA, 1'b0};
  end

always
  begin
    {cB, bB} = {aB, 1'b0};
  end

always
  begin
    {cC, bC} = {aC, 1'b0};
  end

fanout aFanout (
    .in(a),
    .outA(aA),
    .outB(aB),
    .outC(aC)
    );
endmodule

