module tbTMR;
  wire aA;
  wire aB;
  wire aC;
  wire bA;
  wire bB;
  wire bC;
  assign {aA, bA} = 2'b0;
  assign {aB, bB} = 2'b0;
  assign {aC, bC} = 2'b0;
endmodule

