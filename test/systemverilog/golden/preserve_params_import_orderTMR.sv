module testTMR;
  parameter X = 0;
  localparam Y = 1;
  import pkg::Z1;
  localparam A = 1;
  import pkg::Z0;
  parameter B = 0;
  wor eTmrErrorC;
  wire [(Z1) : X] eVotedC;
  wor eTmrErrorB;
  wire [(Z1) : X] eVotedB;
  wor eTmrErrorA;
  wire [(Z1) : X] eVotedA;
  wire [Z1:X] eA;
  wire [Z1:X] eB;
  wire [Z1:X] eC;

  majorityVoter #(
      .WIDTH(((((Z1 - X) > 0) ? (Z1 - X) : -(Z1 - X)) + 1))
  ) eVoterA (
      .inA(eA),
      .inB(eB),
      .inC(eC),
      .out(eVotedA),
      .tmrErr(eTmrErrorA)
  );

  majorityVoter #(
      .WIDTH(((((Z1 - X) > 0) ? (Z1 - X) : -(Z1 - X)) + 1))
  ) eVoterB (
      .inA(eA),
      .inB(eB),
      .inC(eC),
      .out(eVotedB),
      .tmrErr(eTmrErrorB)
  );

  majorityVoter #(
      .WIDTH(((((Z1 - X) > 0) ? (Z1 - X) : -(Z1 - X)) + 1))
  ) eVoterC (
      .inA(eA),
      .inB(eB),
      .inC(eC),
      .out(eVotedC),
      .tmrErr(eTmrErrorC)
  );
endmodule

