module dutTMR;
logic [9:0] [9:0] multidimA;
logic [9:0] [9:0] multidimB;
logic [9:0] [9:0] multidimC;

always_comb
  begin
    static int ciao = 0;

    for(int i = ciao; i<10; i++)
    begin
      automatic int i2 = 10 - 1 - i;

      multidimA[i][i2] = 1'b1;
    end
  end

always_comb
  begin
    static int ciao = 0;

    for(int i = ciao; i<10; i++)
    begin
      automatic int i2 = 10 - 1 - i;

      multidimB[i][i2] = 1'b1;
    end
  end

always_comb
  begin
    static int ciao = 0;

    for(int i = ciao; i<10; i++)
    begin
      automatic int i2 = 10 - 1 - i;

      multidimC[i][i2] = 1'b1;
    end
  end
endmodule

