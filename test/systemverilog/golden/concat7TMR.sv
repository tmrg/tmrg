module tbTMR;
wire aA;
wire aB;
wire aC;
wire bA;
wire bB;
wire bC;
wire cA;
wire cB;
wire cC;
wire dA;
wire dB;
wire dC;
assign {dA, cA, bA} = {3{aA}};
assign {dB, cB, bB} = {3{aB}};
assign {dC, cC, bC} = {3{aC}};
endmodule

