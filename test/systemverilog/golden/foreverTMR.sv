module test;
reg half_period;
reg clk_320M_int = 0;
initial
  begin
        @( negedge clk_ref_i ) @( posedge clk_ref_i ) @( negedge clk_ref_i ) clk_320M_int = 1;
    forever
        #(half_period / 8) clk_320M_int = ~clk_320M_int;

  end
endmodule

