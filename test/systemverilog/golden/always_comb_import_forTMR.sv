module always_comb_import_forTMR;
  import my_package_pkg::ZERO;
  import my_package_pkg::ONE;
  import my_package_pkg::ARRAY_DIM;
  import my_package_pkg::START;
  import my_package_pkg::STOP;
  logic [(ARRAY_DIM-1) : 0] dataA;
  logic [(ARRAY_DIM-1) : 0] dataB;
  logic [(ARRAY_DIM-1) : 0] dataC;

  always_comb begin
    for (int i = 0; i < ARRAY_DIM; i++)
    if (i < START) dataA[i] = ZERO;
    else if ((i >= START) && (i < STOP)) dataA[i] = ONE;
    else if (i == STOP) dataA[i] = ZERO;
    else dataA[i] = ONE;
  end

  always_comb begin
    for (int i = 0; i < ARRAY_DIM; i++)
    if (i < START) dataB[i] = ZERO;
    else if ((i >= START) && (i < STOP)) dataB[i] = ONE;
    else if (i == STOP) dataB[i] = ZERO;
    else dataB[i] = ONE;
  end

  always_comb begin
    for (int i = 0; i < ARRAY_DIM; i++)
    if (i < START) dataC[i] = ZERO;
    else if ((i >= START) && (i < STOP)) dataC[i] = ONE;
    else if (i == STOP) dataC[i] = ZERO;
    else dataC[i] = ONE;
  end
endmodule : always_comb_import_forTMR

