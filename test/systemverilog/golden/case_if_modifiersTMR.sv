module tbTMR;
wor ciaoTmrError;
wire [1:0] ciao;
wire [1:0] ciaoA;
wire [1:0] ciaoB;
wire [1:0] ciaoC;
logic ciao_is_0A;
logic ciao_is_0B;
logic ciao_is_0C;
logic doneA;
logic doneB;
logic doneC;

always_comb
  begin
    priority if (ciaoA==0)
      ciao_is_0A = 1'b1;
    else
      if (ciaoA==1)
        ciao_is_0A = 1'b0;

  end

always_comb
  begin
    priority if (ciaoB==0)
      ciao_is_0B = 1'b1;
    else
      if (ciaoB==1)
        ciao_is_0B = 1'b0;

  end

always_comb
  begin
    priority if (ciaoC==0)
      ciao_is_0C = 1'b1;
    else
      if (ciaoC==1)
        ciao_is_0C = 1'b0;

  end

always_comb
  begin
    unique if (ciaoA==0)
      ;
    else
      if (ciaoA==1)
        ciao_is_0A = 1'b0;

  end

always_comb
  begin
    unique if (ciaoB==0)
      ;
    else
      if (ciaoB==1)
        ciao_is_0B = 1'b0;

  end

always_comb
  begin
    unique if (ciaoC==0)
      ;
    else
      if (ciaoC==1)
        ciao_is_0C = 1'b0;

  end

always_comb
  begin
    unique0 if (ciao==0)
      ;
    else
      if (ciao==1)
        ;

  end

always_comb
  begin
    priority case (ciaoA)
      2'b00 : doneA = 1'b1;
      2'b1z : doneA = 1'b0;
    endcase
  end

always_comb
  begin
    priority case (ciaoB)
      2'b00 : doneB = 1'b1;
      2'b1z : doneB = 1'b0;
    endcase
  end

always_comb
  begin
    priority case (ciaoC)
      2'b00 : doneC = 1'b1;
      2'b1z : doneC = 1'b0;
    endcase
  end

always_comb
  begin
    unique case (ciaoA)
      2'b00 : doneA = 1'b1;
      2'b11 : doneA = 1'b0;
    endcase
  end

always_comb
  begin
    unique case (ciaoB)
      2'b00 : doneB = 1'b1;
      2'b11 : doneB = 1'b0;
    endcase
  end

always_comb
  begin
    unique case (ciaoC)
      2'b00 : doneC = 1'b1;
      2'b11 : doneC = 1'b0;
    endcase
  end

always_comb
  begin
    unique0 case (ciaoA)
      2'b00 : doneA = 1'b1;
      default : doneA = 1'b0;
    endcase
  end

always_comb
  begin
    unique0 case (ciaoB)
      2'b00 : doneB = 1'b1;
      default : doneB = 1'b0;
    endcase
  end

always_comb
  begin
    unique0 case (ciaoC)
      2'b00 : doneC = 1'b1;
      default : doneC = 1'b0;
    endcase
  end

majorityVoter #(.WIDTH(2)) ciaoVoter (
    .inA(ciaoA),
    .inB(ciaoB),
    .inC(ciaoC),
    .out(ciao),
    .tmrErr(ciaoTmrError)
    );
endmodule

