module tbTMR;
wire ciao0A;
wire ciao0B;
wire ciao0C;
wire ciao1A;
wire ciao1B;
wire ciao1C;
logic ciao_is_0A;
logic ciao_is_0B;
logic ciao_is_0C;
logic [5:0] doneA;
logic [5:0] doneB;
logic [5:0] doneC;

always_comb
  begin
    priority case ({ciao0A, ciao1A})
      2'b00 : doneA[0] = 1'b1;
      2'b1z : doneA[0] = 1'b0;
    endcase
  end

always_comb
  begin
    priority case ({ciao0B, ciao1B})
      2'b00 : doneB[0] = 1'b1;
      2'b1z : doneB[0] = 1'b0;
    endcase
  end

always_comb
  begin
    priority case ({ciao0C, ciao1C})
      2'b00 : doneC[0] = 1'b1;
      2'b1z : doneC[0] = 1'b0;
    endcase
  end

always_comb
  begin
    unique case ({ciao0A, ciao1A})
      2'b00 : doneA[1] = 1'b1;
      2'b11 : doneA[1] = 1'b0;
    endcase
  end

always_comb
  begin
    unique case ({ciao0B, ciao1B})
      2'b00 : doneB[1] = 1'b1;
      2'b11 : doneB[1] = 1'b0;
    endcase
  end

always_comb
  begin
    unique case ({ciao0C, ciao1C})
      2'b00 : doneC[1] = 1'b1;
      2'b11 : doneC[1] = 1'b0;
    endcase
  end

always_comb
  begin
    priority casez ({ciao0A, ciao1A})
      2'b00 : doneA[2] = 1'b1;
      2'b1z : doneA[2] = 1'b0;
    endcase
  end

always_comb
  begin
    priority casez ({ciao0B, ciao1B})
      2'b00 : doneB[2] = 1'b1;
      2'b1z : doneB[2] = 1'b0;
    endcase
  end

always_comb
  begin
    priority casez ({ciao0C, ciao1C})
      2'b00 : doneC[2] = 1'b1;
      2'b1z : doneC[2] = 1'b0;
    endcase
  end

always_comb
  begin
    unique casez ({ciao0A, ciao1A})
      2'b00 : doneA[3] = 1'b1;
      2'b11 : doneA[3] = 1'b0;
    endcase
  end

always_comb
  begin
    unique casez ({ciao0B, ciao1B})
      2'b00 : doneB[3] = 1'b1;
      2'b11 : doneB[3] = 1'b0;
    endcase
  end

always_comb
  begin
    unique casez ({ciao0C, ciao1C})
      2'b00 : doneC[3] = 1'b1;
      2'b11 : doneC[3] = 1'b0;
    endcase
  end

always_comb
  begin
    priority casex ({ciao0A, ciao1A})
      2'b00 : doneA[4] = 1'b1;
      2'b1z : doneA[4] = 1'b0;
    endcase
  end

always_comb
  begin
    priority casex ({ciao0B, ciao1B})
      2'b00 : doneB[4] = 1'b1;
      2'b1z : doneB[4] = 1'b0;
    endcase
  end

always_comb
  begin
    priority casex ({ciao0C, ciao1C})
      2'b00 : doneC[4] = 1'b1;
      2'b1z : doneC[4] = 1'b0;
    endcase
  end

always_comb
  begin
    unique casex ({ciao0A, ciao1A})
      2'b00 : doneA[5] = 1'b1;
      2'b11 : doneA[5] = 1'b0;
    endcase
  end

always_comb
  begin
    unique casex ({ciao0B, ciao1B})
      2'b00 : doneB[5] = 1'b1;
      2'b11 : doneB[5] = 1'b0;
    endcase
  end

always_comb
  begin
    unique casex ({ciao0C, ciao1C})
      2'b00 : doneC[5] = 1'b1;
      2'b11 : doneC[5] = 1'b0;
    endcase
  end
endmodule

