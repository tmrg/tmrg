module aTMR;
wor bTmrError;
wire b;
wire bA;
wire bB;
wire bC;
initial
    wait (b==1'b0);
initial
    wait (b==1'b1) $display("CIAO");

majorityVoter bVoter (
    .inA(bA),
    .inB(bB),
    .inC(bC),
    .out(b),
    .tmrErr(bTmrError)
  );
endmodule

