typedef struct packed { 
  logic  a;
} a_t;
typedef enum logic [3:0] {A, B, C, D} b_t;
module m0TMR #(
  parameter a_t a = 0
);
endmodule

module m1TMR #(
  parameter a_t a = 0,
  parameter int x = 0, y = 0
);
endmodule

module m2TMR #(
  parameter a_t a = 0,
  parameter int x = 0, y = 0,
  localparam b_t b = C,
  parameter a_t c = 100
);
endmodule

module m3TMR #(
  parameter a_t a = 0,
  parameter int x = 0, y = 0,
  localparam b_t b = C,
  parameter a_t c = 100, d [1] = {0}, e [0:3] = {1,2,3,4},
  localparam f = 2
);
endmodule

module m4TMR;

m0TMR m0_i (
    
    );

m1TMR m1_i (
    
    );

m2TMR m2_i (
    
    );

m3TMR m3_i (
    
    );
endmodule

