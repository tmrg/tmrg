module package_import_widthTMR (
    input logic [my_package_pkg::WIDTHD - 1:0] dA,
    input logic [my_package_pkg::WIDTHD - 1:0] dB,
    input logic [my_package_pkg::WIDTHD - 1:0] dC,
    input logic [8 * my_package_pkg::WIDTHD - 1:0] d1A,
    input logic [8 * my_package_pkg::WIDTHD - 1:0] d1B,
    input logic [8 * my_package_pkg::WIDTHD - 1:0] d1C,
    input logic [0:my_package_pkg::WIDTHD - 1] d2A,
    input logic [0:my_package_pkg::WIDTHD - 1] d2B,
    input logic [0:my_package_pkg::WIDTHD - 1] d2C,
    input logic eA[my_package_pkg::WIDTHE - 1:0],
    input logic eB[my_package_pkg::WIDTHE - 1:0],
    input logic eC[my_package_pkg::WIDTHE - 1:0],
    input logic e1A[my_package_pkg::WIDTHE],
    input logic e1B[my_package_pkg::WIDTHE],
    input logic e1C[my_package_pkg::WIDTHE],
    input logic [my_package_pkg::WIDTHFP - 1:0] fA[my_package_pkg::WIDTHFU - 1:0],
    input logic [my_package_pkg::WIDTHFP - 1:0] fB[my_package_pkg::WIDTHFU - 1:0],
    input logic [my_package_pkg::WIDTHFP - 1:0] fC[my_package_pkg::WIDTHFU - 1:0],
    output logic [my_package_pkg::WIDTHQ - 1:0] qA,
    output logic [my_package_pkg::WIDTHQ - 1:0] qB,
    output logic [my_package_pkg::WIDTHQ - 1:0] qC,
    output logic rA[my_package_pkg::WIDTHR - 1:0],
    output logic rB[my_package_pkg::WIDTHR - 1:0],
    output logic rC[my_package_pkg::WIDTHR - 1:0],
    output logic [my_package_pkg::WIDTHSP - 1:0] sA[my_package_pkg::WIDTHSU - 1:0],
    output logic [my_package_pkg::WIDTHSP - 1:0] sB[my_package_pkg::WIDTHSU - 1:0],
    output logic [my_package_pkg::WIDTHSP - 1:0] sC[my_package_pkg::WIDTHSU - 1:0]
);
endmodule

