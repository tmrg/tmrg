module testTMR;
typedef struct packed { 
  logic [1:0] tot;
  logic [1:0] toa;
} test_t;
typedef struct packed { 
  logic [1:0] tot;
} test1_t;
logic enableA;
logic enableB;
logic enableC;
logic [1:0] totA;
logic [1:0] totB;
logic [1:0] totC;
logic [1:0] toaA;
logic [1:0] toaB;
logic [1:0] toaC;
logic [1:0] tot0A;
logic [1:0] tot0B;
logic [1:0] tot0C;
logic [1:0] toa0A;
logic [1:0] toa0B;
logic [1:0] toa0C;
logic [3:0] aA;
logic [3:0] aB;
logic [3:0] aC;

always_comb
  begin
    if (enableA)
      begin : toa_and_totA
                test_t ch_payload;

        ch_payload.toa = toaA;
        ch_payload.tot = totA;
      end
    else
      begin : toa_onlyA
                test1_t ch_payload;

        ch_payload.tot = tot0A;
      end
    aA = ch_payload;
  end

always_comb
  begin
    if (enableB)
      begin : toa_and_totB
                test_t ch_payload;

        ch_payload.toa = toaB;
        ch_payload.tot = totB;
      end
    else
      begin : toa_onlyB
                test1_t ch_payload;

        ch_payload.tot = tot0B;
      end
    aB = ch_payload;
  end

always_comb
  begin
    if (enableC)
      begin : toa_and_totC
                test_t ch_payload;

        ch_payload.toa = toaC;
        ch_payload.tot = totC;
      end
    else
      begin : toa_onlyC
                test1_t ch_payload;

        ch_payload.tot = tot0C;
      end
    aC = ch_payload;
  end
endmodule

