module paramtest7TMR #(
    parameter int signed ADDR_WIDTH = 16,
    parameter int unsigned DATA_WIDTH = 16,
    parameter bit [DATA_WIDTH - 1:0] RESET_VALUE = {DATA_WIDTH{1'b0}}
) (
  input clkA,
  input clkB,
  input clkC,
  input rstA,
  input rstB,
  input rstC,
  input [ADDR_WIDTH - 1:0] this_reg_addrA,
  input [ADDR_WIDTH - 1:0] this_reg_addrB,
  input [ADDR_WIDTH - 1:0] this_reg_addrC,
  input [ADDR_WIDTH - 1:0] address_inA,
  input [ADDR_WIDTH - 1:0] address_inB,
  input [ADDR_WIDTH - 1:0] address_inC,
  input weA,
  input weB,
  input weC,
  input [DATA_WIDTH - 1:0] data_inA,
  input [DATA_WIDTH - 1:0] data_inB,
  input [DATA_WIDTH - 1:0] data_inC,
  output [DATA_WIDTH - 1:0] data_outA,
  output [DATA_WIDTH - 1:0] data_outB,
  output [DATA_WIDTH - 1:0] data_outC
);
endmodule

