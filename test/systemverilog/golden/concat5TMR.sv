typedef struct packed { 
  logic  a;
  logic  b;
} struct_t;
module tbTMR;
wire struct_t aC;
wire struct_t aB;
wire struct_t aA;
struct_t a;
struct_t bA;
struct_t bB;
struct_t bC;
struct_t cA;
struct_t cB;
struct_t cC;

always
  begin
    {cA, bA} = {aA, 2'b0};
  end

always
  begin
    {cB, bB} = {aB, 2'b0};
  end

always
  begin
    {cC, bC} = {aC, 2'b0};
  end

fanout #(.WIDTH(2)) aFanout (
    .in(a),
    .outA(aA),
    .outB(aB),
    .outC(aC)
    );
endmodule

