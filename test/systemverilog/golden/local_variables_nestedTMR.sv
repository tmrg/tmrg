module testTMR;
typedef struct packed { 
  logic [1:0] tot;
  logic [1:0] toa;
} test_t;
typedef struct packed { 
  logic [1:0] tot;
} test1_t;
wor totTmrError;
wire [1:0] tot;
wor tot0TmrError;
wire [1:0] tot0;
wor toaTmrError;
wire [1:0] toa;
wor enableTmrError;
wire enable;
logic enableA;
logic enableB;
logic enableC;
logic [1:0] totA;
logic [1:0] totB;
logic [1:0] totC;
logic [1:0] toaA;
logic [1:0] toaB;
logic [1:0] toaC;
logic [1:0] tot0A;
logic [1:0] tot0B;
logic [1:0] tot0C;
logic [1:0] toa0A;
logic [1:0] toa0B;
logic [1:0] toa0C;

always_comb
  begin
    if (enable)
      begin : toa_and_tot
                test_t ch_payload;

        ch_payload.toa = toa;
        ch_payload.tot = tot;
      end
    else
      begin : toa_only
                test1_t ch_payload;

        ch_payload.tot = tot0;
      end
  end

majorityVoter enableVoter (
    .inA(enableA),
    .inB(enableB),
    .inC(enableC),
    .out(enable),
    .tmrErr(enableTmrError)
    );

majorityVoter #(.WIDTH(2)) toaVoter (
    .inA(toaA),
    .inB(toaB),
    .inC(toaC),
    .out(toa),
    .tmrErr(toaTmrError)
    );

majorityVoter #(.WIDTH(2)) tot0Voter (
    .inA(tot0A),
    .inB(tot0B),
    .inC(tot0C),
    .out(tot0),
    .tmrErr(tot0TmrError)
    );

majorityVoter #(.WIDTH(2)) totVoter (
    .inA(totA),
    .inB(totB),
    .inC(totC),
    .out(tot),
    .tmrErr(totTmrError)
    );
endmodule

