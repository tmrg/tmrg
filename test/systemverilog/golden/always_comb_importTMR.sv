module always_comb_importTMR (
    output logic [11:0] next_lsb_cnt_oA,
    output logic [11:0] next_lsb_cnt_oB,
    output logic [11:0] next_lsb_cnt_oC,
    output logic [2:0] next_msb_cnt_oA,
    output logic [2:0] next_msb_cnt_oB,
    output logic [2:0] next_msb_cnt_oC,
    output logic lsb_cnt_err_oA,
    output logic lsb_cnt_err_oB,
    output logic lsb_cnt_err_oC,
    input logic [11:0] lsb_cnt_iA,
    input logic [11:0] lsb_cnt_iB,
    input logic [11:0] lsb_cnt_iC,
    input logic [2:0] msb_cnt_iA,
    input logic [2:0] msb_cnt_iB,
    input logic [2:0] msb_cnt_iC
);
  import my_package_pkg::ZERO;
  import my_package_pkg::ONE;
  import my_package_pkg::LSB_CNT_MAX;

  always_comb begin
    if (lsb_cnt_iA == LSB_CNT_MAX) begin
      next_lsb_cnt_oA = 12'b0000_0000_0000;
      next_msb_cnt_oA = msb_cnt_iA + 3'b001;
      lsb_cnt_err_oA  = ZERO;
    end else if (lsb_cnt_iA > LSB_CNT_MAX) begin
      next_lsb_cnt_oA = lsb_cnt_iA;
      next_msb_cnt_oA = msb_cnt_iA;
      lsb_cnt_err_oA  = ONE;
    end else begin
      next_lsb_cnt_oA = lsb_cnt_iA + 12'b0000_0000_0001;
      next_msb_cnt_oA = msb_cnt_iA;
      lsb_cnt_err_oA  = ZERO;
    end
  end

  always_comb begin
    if (lsb_cnt_iB == LSB_CNT_MAX) begin
      next_lsb_cnt_oB = 12'b0000_0000_0000;
      next_msb_cnt_oB = msb_cnt_iB + 3'b001;
      lsb_cnt_err_oB  = ZERO;
    end else if (lsb_cnt_iB > LSB_CNT_MAX) begin
      next_lsb_cnt_oB = lsb_cnt_iB;
      next_msb_cnt_oB = msb_cnt_iB;
      lsb_cnt_err_oB  = ONE;
    end else begin
      next_lsb_cnt_oB = lsb_cnt_iB + 12'b0000_0000_0001;
      next_msb_cnt_oB = msb_cnt_iB;
      lsb_cnt_err_oB  = ZERO;
    end
  end

  always_comb begin
    if (lsb_cnt_iC == LSB_CNT_MAX) begin
      next_lsb_cnt_oC = 12'b0000_0000_0000;
      next_msb_cnt_oC = msb_cnt_iC + 3'b001;
      lsb_cnt_err_oC  = ZERO;
    end else if (lsb_cnt_iC > LSB_CNT_MAX) begin
      next_lsb_cnt_oC = lsb_cnt_iC;
      next_msb_cnt_oC = msb_cnt_iC;
      lsb_cnt_err_oC  = ONE;
    end else begin
      next_lsb_cnt_oC = lsb_cnt_iC + 12'b0000_0000_0001;
      next_msb_cnt_oC = msb_cnt_iC;
      lsb_cnt_err_oC  = ZERO;
    end
  end
endmodule : always_comb_importTMR

