typedef struct packed { 
  logic [31:0] ciao1;
  logic [15:0] ciao2;
  logic [7:0] ciao3;
  logic [7:0] ciao4;
} ciao64_t;
typedef struct packed { 
  logic [15:0] mamma1;
  logic [7:0] mamma2;
  logic [7:0] mamma3;
} mamma32_t;
typedef struct packed { 
  mamma32_t babbo1;
  logic [31:0] babbo2;
} babbo64_t;
typedef union packed { 
  ciao64_t ciao;
  babbo64_t babbo;
  logic [63:0] logico;
} unione_t;
module tbTMR;
wor eccomiTmrError;
wire unione_t eccomi;
unione_t eccomiA;
unione_t eccomiB;
unione_t eccomiC;
wire a = eccomi.ciao.ciao1;
wire b = eccomi.babbo.babbo1.mamma1;
wire c = eccomi.logico[0];
wire babbo64_t dA = eccomiA.babbo;
wire babbo64_t dB = eccomiB.babbo;
wire babbo64_t dC = eccomiC.babbo;
wire mamma32_t eA = eccomiA.babbo.babbo1;
wire mamma32_t eB = eccomiB.babbo.babbo1;
wire mamma32_t eC = eccomiC.babbo.babbo1;

majorityVoter #(.WIDTH(64)) eccomiVoter (
    .inA(eccomiA),
    .inB(eccomiB),
    .inC(eccomiC),
    .out(eccomi),
    .tmrErr(eccomiTmrError)
  );
endmodule

