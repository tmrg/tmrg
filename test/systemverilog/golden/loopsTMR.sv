module testTMR;
logic [31:0] outputsA;
logic [31:0] outputsB;
logic [31:0] outputsC;
logic [2:0] selected_inputsA;
logic [2:0] selected_inputsB;
logic [2:0] selected_inputsC;

generate
  for(genvar i = 0; i<=4; i++)
  begin
    assign outputsA[i] = i * 4;
    assign outputsB[i] = i * 4;
    assign outputsC[i] = i * 4;
  end
endgenerate

always_comb
  for(int i = 0; i<3; i++)
    selected_inputsA[i] = 1'b1;

always_comb
  for(int i = 0; i<3; i++)
    selected_inputsB[i] = 1'b1;

always_comb
  for(int i = 0; i<3; i++)
    selected_inputsC[i] = 1'b1;
endmodule

