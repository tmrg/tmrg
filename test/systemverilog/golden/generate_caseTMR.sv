module haTMR (
    input aA,
    input aB,
    input aC,
    input bA,
    input bB,
    input bC,
    output reg sumA,
    output reg sumB,
    output reg sumC,
    output reg coutA,
    output reg coutB,
    output reg coutC
);

  always @(aA or bA) {coutA, sumA} = aA + bA;


  always @(aB or bB) {coutB, sumB} = aB + bB;


  always @(aC or bC) {coutC, sumC} = aC + bC;
  initial $display("Half adder instantiation");
endmodule

module fa (
    input a,
    input b,
    input cin,
    output reg sum,
    output reg cout
);

  always @(a or b or cin) {cout, sum} = a + b + cin;
  initial $display("Full adder instantiation");
endmodule

module my_adderTMR (
    input  aA,
    input  aB,
    input  aC,
    input  bA,
    input  bB,
    input  bC,
    input  cinA,
    input  cinB,
    input  cinC,
    output sumA,
    output sumB,
    output sumC,
    output coutA,
    output coutB,
    output coutC
);
  parameter ADDER_TYPE = 1;
  wire a2A;

  wire a2B;

  wire a2C;

  generate
    case (ADDER_TYPE)
      0:
      haTMR u0 (
          .aA(aA),
          .aB(aB),
          .aC(aC),
          .bA(bA),
          .bB(bB),
          .bC(bC),
          .sumA(sumA),
          .sumB(sumB),
          .sumC(sumC),
          .coutA(coutA),
          .coutB(coutB),
          .coutC(coutC)
      );

      1: begin

        fa u1A (
            .a(aA),
            .b(bA),
            .cin(cinA),
            .sum(sumA),
            .cout(coutA)
        );


        fa u1B (
            .a(aB),
            .b(bB),
            .cin(cinB),
            .sum(sumB),
            .cout(coutB)
        );


        fa u1C (
            .a(aC),
            .b(bC),
            .cin(cinC),
            .sum(sumC),
            .cout(coutC)
        );
      end

    endcase
    if (ADDER_TYPE == 0) begin
      assign a2A = aA;

      assign a2B = aB;

      assign a2C = aC;
    end else begin

      fa u2A (.a(aA));


      fa u2B (.a(aB));


      fa u2C (.a(aC));
    end

  endgenerate
endmodule

