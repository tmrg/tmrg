module testTMR;
typedef struct packed { 
  logic b;
} b_t;
wor perip_hit_addrTmrError;
wire [2:0] perip_hit_addr;
function ciao;
  input int a;
  input int b;
  output logic c;
  input b_t d;
endfunction

logic [2:0] perip_hit_addrA;
logic [2:0] perip_hit_addrB;
logic [2:0] perip_hit_addrC;
initial
  begin
    logic [2:0] perip_hit_addr_init;

    if (!std::randomize(perip_hit_addr_init))
      $error("Randomization error!");

    $deposit(perip_hit_addr, perip_hit_addr_init);
  end
initial
  begin
    if (!std::randomize(perip_hit_addrA))
      $error("Randomization error!");

  end
initial
  begin
    if (!std::randomize(perip_hit_addrB))
      $error("Randomization error!");

  end
initial
  begin
    if (!std::randomize(perip_hit_addrC))
      $error("Randomization error!");

  end

majorityVoter #(.WIDTH(3)) perip_hit_addrVoter (
    .inA(perip_hit_addrA),
    .inB(perip_hit_addrB),
    .inC(perip_hit_addrC),
    .out(perip_hit_addr),
    .tmrErr(perip_hit_addrTmrError)
  );
endmodule

