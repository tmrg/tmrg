module tbTMR;
  test_t aA;
  test_t aB;
  test_t aC;
  logic  bA;
  logic  bB;
  logic  bC;
  logic  cA;
  logic  cB;
  logic  cC;
  initial begin
    aA.ciao = bA;
    cA = aA.bello;
  end
  initial begin
    aB.ciao = bB;
    cB = aB.bello;
  end
  initial begin
    aC.ciao = bC;
    cC = aC.bello;
  end
endmodule

