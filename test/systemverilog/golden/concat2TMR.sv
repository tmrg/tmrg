module tbTMR;
logic aA;
logic aB;
logic aC;
logic bA;
logic bB;
logic bC;
logic cA;
logic cB;
logic cC;

always
  begin
    {cA, bA} = {bA, aA};
  end

always
  begin
    {cB, bB} = {bB, aB};
  end

always
  begin
    {cC, bC} = {bC, aC};
  end
endmodule

