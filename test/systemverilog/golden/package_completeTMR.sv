package my_pkg;
  localparam int Z = 1'b0;
  parameter logic O = 1'b1;
  typedef struct packed {logic a;} b_t;
  typedef struct packed {
    b_t [2:0] b;
    logic c;
  } d_t;
  typedef enum logic [1:0] {
    A,
    B,
    C,
    D = 2'b11
  } e_t;
endpackage

module aTMR
  import my_pkg::Z, my_pkg::O, my_pkg::d_t;
(
    input wire fA,
    input wire fB,
    input wire fC
);
  wor dTmrErrorC;
  wire d_t dVotedC;
  wor dTmrErrorB;
  wire d_t dVotedB;
  wor dTmrErrorA;
  wire d_t dVotedA;
  d_t dA;
  d_t dB;
  d_t dC;
  assign dA.b[0].a = Z | O;
  assign dB.b[0].a = Z | O;
  assign dC.b[0].a = Z | O;

  majorityVoter #(
      .WIDTH(4)
  ) dVoterA (
      .inA(dA),
      .inB(dB),
      .inC(dC),
      .out(dVotedA),
      .tmrErr(dTmrErrorA)
  );

  majorityVoter #(
      .WIDTH(4)
  ) dVoterB (
      .inA(dA),
      .inB(dB),
      .inC(dC),
      .out(dVotedB),
      .tmrErr(dTmrErrorB)
  );

  majorityVoter #(
      .WIDTH(4)
  ) dVoterC (
      .inA(dA),
      .inB(dB),
      .inC(dC),
      .out(dVotedC),
      .tmrErr(dTmrErrorC)
  );
endmodule

module bTMR
  import my_pkg::Z, my_pkg::*;
(
    input wire fA,
    input wire fB,
    input wire fC
);
  wor zTmrErrorC;
  wire my_pkg::e_t zVotedC;
  wor gTmrErrorC;
  wire my_pkg::d_t [1:0] gVotedC;
  wor zTmrErrorB;
  wire my_pkg::e_t zVotedB;
  wor gTmrErrorB;
  wire my_pkg::d_t [1:0] gVotedB;
  wor zTmrErrorA;
  wire my_pkg::e_t zVotedA;
  wor gTmrErrorA;
  wire my_pkg::d_t [1:0] gVotedA;
  my_pkg::d_t [1:0] gA;
  my_pkg::d_t [1:0] gB;
  my_pkg::d_t [1:0] gC;
  assign gA[1].b[0].a = my_pkg::O;
  assign gB[1].b[0].a = my_pkg::O;
  assign gC[1].b[0].a = my_pkg::O;
  my_pkg::e_t zA;
  my_pkg::e_t zB;
  my_pkg::e_t zC;

  majorityVoter #(
      .WIDTH(8)
  ) gVoterA (
      .inA(gA),
      .inB(gB),
      .inC(gC),
      .out(gVotedA),
      .tmrErr(gTmrErrorA)
  );

  majorityVoter #(
      .WIDTH(2)
  ) zVoterA (
      .inA(zA),
      .inB(zB),
      .inC(zC),
      .out({zVotedA}),
      .tmrErr(zTmrErrorA)
  );

  majorityVoter #(
      .WIDTH(8)
  ) gVoterB (
      .inA(gA),
      .inB(gB),
      .inC(gC),
      .out(gVotedB),
      .tmrErr(gTmrErrorB)
  );

  majorityVoter #(
      .WIDTH(2)
  ) zVoterB (
      .inA(zA),
      .inB(zB),
      .inC(zC),
      .out({zVotedB}),
      .tmrErr(zTmrErrorB)
  );

  majorityVoter #(
      .WIDTH(8)
  ) gVoterC (
      .inA(gA),
      .inB(gB),
      .inC(gC),
      .out(gVotedC),
      .tmrErr(gTmrErrorC)
  );

  majorityVoter #(
      .WIDTH(2)
  ) zVoterC (
      .inA(zA),
      .inB(zB),
      .inC(zC),
      .out({zVotedC}),
      .tmrErr(zTmrErrorC)
  );
endmodule

module cTMR;

  aTMR a ();

  bTMR b ();
endmodule

