module test;
  task not_inline;
    input a;
	input b;
    output c;

	c = a ^ b;
  endtask

  task automatic mux(input logic a, output logic b);
	  b = a;
  endtask

  // tmrg default triplicate
  // tmrg do_not_triplicate ciao_out_notmr
  logic ciao;
  logic ciao_out;
  logic ciao_out_notmr;

  initial begin
	  #1 mux(ciao, ciao_out);
  end

  initial begin
	  #1 not_inline(ciao, 1'b1, ciao_out_notmr);
  end
endmodule

