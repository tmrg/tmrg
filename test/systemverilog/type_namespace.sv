module a;
endmodule

typedef struct packed {
	logic x;
} b_t;

typedef struct packed {
	b_t b;
} c_t;

module d;
c_t y;
c_t yVoted = y;

a a_i();
endmodule
