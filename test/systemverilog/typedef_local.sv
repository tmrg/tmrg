module a;
typedef logic l_t;

l_t b;
l_t bVoted = b;
endmodule

module c;

l_t d;
l_t dVoted = d;

endmodule

module e;
a a();
c c();
endmodule
