typedef struct packed {
	logic a;
} a_t;

typedef enum logic [3:0] {
	A, B, C, D
} b_t;

module m0 #(parameter a_t a = 0);
endmodule

module m1 #(parameter a_t a = 0, parameter int x = 0, y = 0);
endmodule

module m2 #(parameter a_t a = 0, parameter int x = 0, y = 0, localparam b_t b = C, parameter a_t c = 100);
endmodule

module m3 #(parameter a_t a = 0, parameter int x = 0, y = 0, localparam b_t b = C, parameter a_t c = 100, d[1] = {0}, e[0:3] = {1, 2, 3, 4}, localparam f = 2);
endmodule

module m4;

m0 m0_i ();
m1 m1_i ();
m2 m2_i ();
m3 m3_i ();

endmodule
