typedef enum logic[0:0] { ON = 1'b1, OFF = 1'b0 } state1_t;

module a;
typedef enum logic[1:0] { A = 2'b1, B = 2'b0 } state2_t;

state2_t ciao;
state2_t ciaoVoted = ciao;

endmodule

module b;
typedef enum logic[1:0] { C = 2'b1, D = 2'b0 } state3_t;

state1_t miao;
state1_t miaoVoted = miao;
state1_t viao = miaoVoted;

state2_t ciao;
wire state2_t ciaoVoted = ciao;

cane_t ciao2;
wire cane_t ciao2Voted = ciao2;

endmodule

module c;
a a();
b b();
endmodule
