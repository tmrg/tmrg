module tb;
wire a, b, c, d;
assign {d, c, b} = {3 {a}};
endmodule
