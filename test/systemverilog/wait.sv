module a;

wire b;

initial wait(b == 1'b0);
initial wait(b == 1'b1) $display("CIAO");

endmodule
