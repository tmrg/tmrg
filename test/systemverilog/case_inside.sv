module case_tb ();
	reg [3:0] a = 0;
	reg [3:0] b = 0;
	always @* begin
		case(a) inside
			1, 3: b = 1;
			4'b01??: b = 2;
			10, [5:6]: b = 3;
			default b = 4;
		endcase
	end
endmodule
