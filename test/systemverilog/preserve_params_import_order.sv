module test;

parameter X  = 0;
localparam Y = 1;
import pkg::Z1;
localparam A = 1;
import pkg::Z0;
parameter B  = 0;

wire [Z1:X] e;
wire [Z1:X] eVoted = e;

endmodule
