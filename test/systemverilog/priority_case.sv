module test;
  logic [1:0] a;
  logic b;

  always_comb begin
    priority case (a)
      2'b00: b = 0;
      default:b = 1;
    endcase
  end
endmodule
