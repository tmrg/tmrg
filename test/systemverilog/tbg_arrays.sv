module test 
(input wire a [3:0],
 output reg b [4:0],
 input wire [2:0] c,
 output reg [1:0] d,
 input wire [4:0] e [2:0],
 output reg [1:3] f [0:2],
 input wire g,
 output reg h
);
endmodule

