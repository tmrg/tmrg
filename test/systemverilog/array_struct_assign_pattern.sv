module x;

typedef struct packed {
	logic a;
	logic b;
} c_t;

typedef struct packed {
	c_t c;
	logic d;
} e_t;

logic a, b;

c_t c = '{a: a, b: b & 1'b0};
e_t t = '{c: c, d: 1, default: 'hx};

endmodule
