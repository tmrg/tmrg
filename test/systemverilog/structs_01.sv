`define W 6

typedef struct packed {
	logic [1:0] field0;
	logic [`W:0] field1;
	logic [`W*8-1:1] [1:0] [3:4] field2;
} st0;

module test (
	input st0  [1:0] i0,
	input wire [4:0] [1:0] i1,
	input wire clk,
	output wire p
);

typedef struct packed {
	st0 field3;
	logic [1:0] [4:0] [1:0] field4;
} st1;

st1 test;
st1 testVoted = test;

assign test.field3 = i0;
always_ff @(posedge clk) begin
	test.field4[0] = i1;
	test.field4[1] = test.field4[0];
end

assign p = (^ testVoted.field3.field0) ^ (^ testVoted.field3.field1) ^ (^ testVoted.field3.field2) ^ (^ testVoted.field4);

endmodule
