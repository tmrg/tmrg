package my_pkg;
  localparam int Z = 1'b0;
  parameter logic O = 1'b1;
  typedef struct packed {
    logic a;
  } b_t;
  typedef struct packed {
    b_t [2:0] b;
    logic c;
  } d_t;
  typedef enum logic [1:0] {A, B, C, D = 2'b11} e_t;
endpackage

module a import my_pkg::Z, my_pkg::O, my_pkg::d_t; (input wire f);
d_t d;
wire d_t dVoted = d;
assign d.b[0].a = Z | O;
endmodule

module b import my_pkg::Z, my_pkg::*; (input wire f);
my_pkg::d_t [1:0] g;
wire my_pkg::d_t [1:0] gVoted = g;

assign g[1].b[0].a = my_pkg::O;
my_pkg::e_t z;
wire my_pkg::e_t zVoted = z;
endmodule

module c;
a a();
b b();
endmodule
