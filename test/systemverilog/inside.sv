typedef enum logic[3:0] {ADD, SUB, MUL, DIV, MOD, OR, AND, XOR} opcode_t;

module dut;
localparam bit ciao = 1;
localparam opcode_t allowed = ADD;
localparam opcode_t arithmetic_ops[5] = {ADD, SUB, MUL, DIV, MOD};
localparam opcode_t logical_ops [3] = {OR, AND, XOR};

opcode_t opcode;
logic b;
always_comb
	b = opcode inside {arithmetic_ops};

logic c;
always_comb begin
	if (opcode inside logical_ops) begin
		c = 1'b0;
	end else begin
		c = b;
	end
end

endmodule
