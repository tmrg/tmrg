module test;
  typedef struct packed {logic b;} b_t;
  function ciao;
	  input int a, b;
	  output logic c;
	  input b_t d;
  endfunction

  logic [2:0] perip_hit_addr;

  initial begin
    logic [2:0] perip_hit_addr_init;
    if (!std::randomize(perip_hit_addr_init)) $error("Randomization error!");
    $deposit(perip_hit_addr, perip_hit_addr_init);
  end

  initial begin
    if (!std::randomize(perip_hit_addr)) $error("Randomization error!");
  end
endmodule
