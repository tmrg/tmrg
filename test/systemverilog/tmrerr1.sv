module test
  (input logic reset,
   input logic [7:0] rwmem [5]
   );

  wire [7:0] rwmemVoted [5];
  assign rwmemVoted = rwmem;
  wire   rwmemTmrError = 1'b0;
  logic  Clk_gated_enable;
  assign Clk_gated_enable = reset || rwmemTmrError;
endmodule
