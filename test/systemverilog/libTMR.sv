module libTMR (
  input wire clockA,
  input wire clockB,
  input wire clockC,
  input wire resetA,
  input wire resetB,
  input wire resetC,
  output wire data
);
wor dTmrError;
wire d;
logic dA;
logic dB;
logic dC;

always @( posedge clockA )
  begin
    if (resetA)
      dA <= 1'b0;
    else
      dA <= ~dA;
  end

always @( posedge clockB )
  begin
    if (resetB)
      dB <= 1'b0;
    else
      dB <= ~dB;
  end

always @( posedge clockC )
  begin
    if (resetC)
      dC <= 1'b0;
    else
      dC <= ~dC;
  end
assign data = d;

majorityVoter dVoter (
    .inA(dA),
    .inB(dB),
    .inC(dC),
    .out(d),
    .tmrErr(dTmrError)
    );
endmodule



module majorityVoter #(
  parameter WIDTH = 1
)( 
  input wire  [WIDTH-1:0] inA,
  input wire  [WIDTH-1:0] inB,
  input wire  [WIDTH-1:0] inC,
  output wire [WIDTH-1:0] out,
  output reg              tmrErr
);
  assign out = (inA&inB) | (inA&inC) | (inB&inC);
  always @(inA or inB or inC) begin
    if (inA!=inB || inA!=inC || inB!=inC)
      tmrErr = 1;
    else
      tmrErr = 0;
  end
endmodule
