`define TRENTADUE 32

typedef struct packed {
	logic [`TRENTADUE-1:0] ciao1;
} pater32_t;

typedef union packed {
	pater32_t ciao;
	logic [31:0] logico;
} unione_t;

typedef union packed {
	logic [31:0] logico;
	pater32_t ciao;
} unione_inverso_t;

module tb;
unione_t eccomi;
unione_inverso_t qui;

// tmrg default do_not_triplicate
// tmrg triplicate eccomi

endmodule
