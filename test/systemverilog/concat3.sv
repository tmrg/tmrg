module tb;

//tmrg default triplicate
// tmrg do_not_triplicate c
// tmrg do_not_triplicate b

logic a, b, c;
always begin
	{c, b} = {b, a};
end

endmodule
