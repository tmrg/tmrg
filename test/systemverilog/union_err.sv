typedef struct packed {
	logic [31:0] ciao1;
} ciao64_t;

typedef struct packed {
	logic [15:0] mamma1;
} mamma32_t;

typedef union packed {
	ciao64_t     ciao;
	mamma32_t    mamma;
} unione_t;

module tb;
unione_t eccomi;

// tmrg default do_not_triplicate
// tmrg triplicate eccomi
// tmrg triplicate d e

wire a = eccomi.ciao.ciao1;
wire b = eccomi.babbo.babbo1.mamma1;
wire babbo64_t d = eccomi.babbo;
wire mamma32_t e = eccomi.babbo.babbo1;

endmodule
