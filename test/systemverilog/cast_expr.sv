module tb;

wire a, b, c;

assign c = unsigned'(a) + b;

endmodule
