package importhis;
	typedef logic thisisactuallylogic_t;
	parameter logic equaltoone = 1'b1;
endpackage

module reorderme();
localparam int DEPTH = 2;
typedef struct packed {
	logic [DEPTH-1:0] field;
} struct_t;
import importhis::thisisactuallylogic_t;
typedef enum thisisactuallylogic_t {ALPHA = (DEPTH>30), BETA = (DEPTH+1 < 25)} enum_t;
import importhis::equaltoone;
localparam struct_t structparam = '{field: {DEPTH {equaltoone}}};
parameter enum_t enumparam = ALPHA;
wire enum_t enumparamwire = enumparam;
endmodule
