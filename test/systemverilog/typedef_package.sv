package my_pkg;

	typedef struct packed {
		logic a;
		logic b;
	} c_t;

	typedef c_t d_t;

endpackage

module e import my_pkg::d_t;;

d_t d;
d_t dVoted = d;

endmodule
