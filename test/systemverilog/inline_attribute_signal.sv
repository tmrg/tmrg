module a (input [5:0] b);
  (* ATTRIBUTE = "value" *) reg x;
  (* ATTRIBUTE = "value" *) reg v [5];
  (* power_saving,ATTRIBUTE = "value" *) reg u [3:0];
  (* power_saving,
   ATTRIBUTE = "value" *) reg uu [3:0];
  (* DONT_TOUCH = 10 *) wire y;
  (* DONT_TOUCH *) wire w [3];
  (* DONT_TOUCH = "yes" *) wire [5:0] bVoted = b;
  (* DONT_TOUCH = "yes" *) wire [5:0] cVoted;
  assign cVoted = b;
endmodule
