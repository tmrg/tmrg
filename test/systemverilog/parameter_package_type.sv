package my_pkg;
  typedef logic [1:0] e_t;
endpackage

module m #(parameter my_pkg::e_t c = 2'b10) ();
  parameter my_pkg::e_t d = 2'b11;
  my_pkg::e_t b = 2'b10;
  assign b = c;
endmodule

