module a;

wire #1 t = 1'b1;

logic a, b, c;
wire d, e, f;

nor (highz1, strong0) n1 (d, a, b);
nand #2 t_nand[0:3] (e, d, c);
bufif0 (f, d, e);

endmodule
