module tb;

//tmrg default triplicate

logic a, b, c;
always begin
	{c, b} = {b, a};
end

endmodule
