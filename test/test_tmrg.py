import os
import pytest
from .common import *
from tmrg.tmrg import main as tmrg_main

@pytest.fixture
def tmrg(monkeypatch):
    return CliExecutor(monkeypatch, "tmrg", main_function=tmrg_main)

def test_tmrg_no_arguments(tmrg, capfd):
    assert tmrg()
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["No modules found. Please refer to the documentation"])

def test_tmrg_missing_file(tmrg, capfd):
    assert tmrg(["not_existing.v"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["File or directory does not exists"])

def test_tmrg_simple_verilog(tmrg, capsys):
    assert not tmrg([file_in_test_dir("verilog/always.v")])
    assert_output_streams(capsys)

def test_tmrg_multiple_top_files(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/topModule.v")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["The design has multiple top cells"])

def test_tmrg_multiple_top_modules_specify(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/topModule.v"), "--top-module=m1"])
    assert_output_streams(capfd)

def test_tmrg_include(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/include.v"), "--include", "--inc-dir", file_in_test_dir("verilog")])
    assert_output_streams(capfd)

def test_tmrg_include2(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/include2.v"), "--include", "--inc-dir", file_in_test_dir("verilog")])
    assert_output_streams(capfd)

def test_tmrg_include3(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/include3.v"), "--include", "--inc-dir", file_in_test_dir("verilog")])
    assert_output_streams(capfd)

def test_tmrg_include4(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/include4.v"), "--include", "--inc-dir", file_in_test_dir("verilog")])
    assert_output_streams(capfd)

def test_tmrg_include_lib(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/libtest.v"), "--lib", file_in_test_dir("verilog/lib.v")])
    assert_output_streams(capfd)

def test_tmrg_include_tmr_lib(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/use_lib.sv"), "--lib", file_in_test_dir("systemverilog/libTMR.sv")])
    assert_output_streams(capfd)

def test_tmrg_stats(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/fsm01.v"), "--stats"])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["Total number of files parsed:", "Total number of lines parsed"])

def test_tmrg_bare_minimum(tmrg, capfd):
    verilog_file = "verilog/defparam.v"
    assert not tmrg([file_in_test_dir(verilog_file), "-b"])
    basename = os.path.basename(verilog_file)
    assert_output_streams(capfd)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
    assert os.path.isfile(golden_file)
    tmr_file = basename.replace(".v", "TMR.v")
    equal_check(golden_file, tmr_file)

def test_tmrg_warning_delay_reference(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/delay.v")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["Variable del is in a delay expression. Marking it as do_not_triplicate.", "Variable del1 is in a delay expression. Marking it as do_not_triplicate."])

def test_tmrg_warning_left_hand_side_concatenation_on_file(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/concat_fail.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Block contains both type of elements (should and should not be triplicated!) in one expression.",
        "This request will not be properly processed!",
        "Elements: a b",
    ])

def test_tmrg_warning_unknown_tmrg_directive(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/unknown_tmrg_directive.v")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Could not process tmrg directive: `tmrg explode` (in module tb)",
        "Directive of type explode is not supported.",
    ])

def test_tmrg_warning_typedef_local(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/typedef_local.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Referencing type `l_t`, which has not been declared in `module c`'s namespace. Available types: ``.",
        "Unable to evaluate the size of type `l_t`. Using the `$size` system task."
    ])

def test_tmrg_error_illegal_assignment_concat(tmrg, capfd):
    assert tmrg([file_in_test_dir("systemverilog/illegal_assignment_concat.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["Net declarations with assignment don't support tokens of type concat on the LHS.",])

def test_tmrg_maintain_elaboration_order(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/elaboration_order_b.sv"), file_in_test_dir("systemverilog/elaboration_order_a.sv")])
    assert_output_streams(capfd)

def test_tmrg_warning_mistaken_elaboration_order(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/elaboration_order_a.sv"), file_in_test_dir("systemverilog/elaboration_order_b.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Referencing type `b_t`, which has not been declared in `module a`'s namespace. Available types: ``.",
        "Referencing type `b_t`, which has not been declared in `module a`'s namespace. Available types: ``.",
        "Unable to evaluate the size of type `b_t`. Using the `$size` system task.",
    ])

def test_tmrg_warning_unpacked_declaration_assignment(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/unpacked_declaration_assignment.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["Unpacked net declaration with assignment is not SV-compliant. TMR will still work but non-triplicated simulations may not."])

def test_tmrg_warning_package_notfound_tmr(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/package_notfound_tmr.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Referencing type `undefined_t` from package `x_pkg`, which has not been declared. Available packages: `std`.",
        "Unable to evaluate the size of type `x_pkg::undefined_t`. Using the `$size` system task.",
    ])

def test_tmrg_error_package_item_notfound(tmrg, capfd):
    assert tmrg([file_in_test_dir("systemverilog/package_item_notfound.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Module a tried to import non-existent item b from package x_pkg."
    ])

def test_tmrg_error_struct_field_not_existing(tmrg, capfd):
    assert tmrg([file_in_test_dir("systemverilog/structs_not_a_field.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Referencing field `b` of signal `a`, whose structure/union `a_t` doesn't contain a field with that name. Available fields are: a."
    ])

def test_tmrg_error_conflict_net_over_genvar(tmrg, capfd):
    error = "Found the net declaration of `i`, but there is already a genvar with the same name in module `dut`!. This is currently not supported."
    assert tmrg([file_in_test_dir("systemverilog/conflict_genvar1.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])
    assert tmrg([file_in_test_dir("systemverilog/conflict_genvar4.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])
    assert tmrg([file_in_test_dir("systemverilog/conflict_genvar5.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])

def test_tmrg_error_conflict_genvar_over_net(tmrg, capfd):
    error = "Found a genvar called `i`, but there is already a net with the same name in module `dut`!. This is currently not supported."
    assert tmrg([file_in_test_dir("systemverilog/conflict_genvar2.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])
    assert tmrg([file_in_test_dir("systemverilog/conflict_genvar3.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])
    assert tmrg([file_in_test_dir("systemverilog/conflict_genvar6.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])

def test_tmrg_error_conflict_local_variable_over_net(tmrg, capfd):
    error = "Found a local variable called `c`, but there is already a net with the same name in module `dut`!. This is currently not supported."
    assert tmrg([file_in_test_dir("systemverilog/conflict_local_variable1.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])
    assert tmrg([file_in_test_dir("systemverilog/conflict_local_variable3.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])

def test_tmrg_error_conflict_net_over_local_variable(tmrg, capfd):
    error = "Found the net declaration of `c`, but there is already a local variable with the same name in module `dut`!. This is currently not supported."
    assert tmrg([file_in_test_dir("systemverilog/conflict_local_variable2.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])
    assert tmrg([file_in_test_dir("systemverilog/conflict_local_variable4.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[error])

def test_tmrg_error_not_a_struct(tmrg, capfd):
    assert tmrg([file_in_test_dir("systemverilog/structs_not_a_struct_01.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
       "Referencing field `b` of signal `a`, whose type `a_t` is neither a structure nor a union."
    ])
    assert tmrg([file_in_test_dir("systemverilog/structs_not_a_struct_02.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
       "Referencing field `b` of signal `a`, whose type `int` is neither a structure nor a union."
    ])

def test_tmrg_error_union_size(tmrg, capfd):
    assert tmrg([file_in_test_dir("systemverilog/union_err.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Field mamma (type: mamma32_t) of union unione_t has size 16. Expecting it to be 32."
    ])

def test_tmrg_warning_unnamed_gates(tmrg, capfd):
    assert not tmrg([file_in_test_dir("systemverilog/unnamed_gates.sv")])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Found verilog gate level instantiation in module `a`. This is generally NOT supported.",
        "Please carefully review the generated code.",
        "Found verilog gate level instantiation in module `a`. This is generally NOT supported.",
        "Please carefully review the generated code.",
        "Found verilog gate level instantiation in module `a`. This is generally NOT supported.",
        "Please carefully review the generated code.",
    ])

def test_tmrg_warning_unpacked_array_tmr_on_declaration(tmrg, capfd):
    """This test differs from TestTmrgOnSingleSystemverilogFileVsGolden because
    the source RTL file here contains illegal SV syntax. TMRG supports it, but throws
    a warning that needs to be checked, and generates a SV-compliant output. As a result,
    syntax checks are only performed on the triplicated file, and not the source.
    """
    verilog_file = "systemverilog/unpacked_array_tmr_on_declaration.sv"

    assert not tmrg([file_in_test_dir(verilog_file),"--no-header","--no-common-definitions"])
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".sv", "TMR.sv")
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Unpacked net declaration with assignment is not SV-compliant. TMR will still work but non-triplicated simulations may not."
    ])
    assert os.path.isfile(tmr_file)
    syntax_check(tmr_file, cmds=["iverilog -g2012","verible-verilog-syntax"],common=True)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".sv","TMR.sv"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file)

def test_tmrg_warning_undefined_struct_with_field_access(tmrg, capfd):
    """This test differs from TestTmrgOnSingleSystemverilogFileVsGolden because
    the source RTL file here contains incomplete SV syntax. TMRG supports it, but throws
    a warning that needs to be checked, and generates a SV-compliant output.
    """
    verilog_file = "systemverilog/structs_02.sv"

    assert not tmrg([file_in_test_dir(verilog_file),"--no-header","--no-common-definitions"])
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".sv", "TMR.sv")
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Referencing type `test_t`, which has not been declared in `module tb`'s namespace. Available types: ``.",
        "Referencing type `test_t`, which has not been declared in `module tb`'s namespace. Available types: ``.",
        "Referencing type `test_t`, which has not been declared in `module tb`'s namespace. Available types: ``."
    ])
    assert os.path.isfile(tmr_file)
    syntax_check(tmr_file, cmds=["iverilog -g2012","verible-verilog-syntax"],common=True)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".sv","TMR.sv"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file)

def test_tmrg_hierahical(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/hier/m1.v"), file_in_test_dir("verilog/hier/m2.v"), file_in_test_dir("verilog/hier/m3.v"), file_in_test_dir("verilog/hier/m4.v"), file_in_test_dir("verilog/hier/m5.v"), file_in_test_dir("verilog/hier/top.v")])
    assert_output_streams(capfd)

def test_tmrg_hierahical_duplicates(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/hier/m1.v"), file_in_test_dir("verilog/hier/m2.v"), file_in_test_dir("verilog/hier/m2.v"), file_in_test_dir("verilog/hier/m3.v"), file_in_test_dir("verilog/hier/m4.v"), file_in_test_dir("verilog/hier/m5.v"), file_in_test_dir("verilog/hier/m2.v"), file_in_test_dir("verilog/hier/top.v")])
    assert_output_streams(capfd)

def test_tmrg_output_log(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/fsm01.v"), "--log", "fsm01.log", "-v" ])
    assert_output_streams(capfd, expect_stderr_empty=False)
    assert os.path.isfile("fsm01.log")

def test_tmrg_generate_bug_report(tmrg, capfd):
    assert not tmrg([file_in_test_dir("verilog/fsm01.v"), "--generate-report"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["Creating zip archive with bug report"])
    # FIXME: check if archive exists

def test_tmrg_copy_unmatched(tmrg, capfd):
    verilog_file = "verilog/copy_unmatched.v"
    assert not tmrg([file_in_test_dir(verilog_file), "--no-header","--no-common-definitions"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
            "Found an unprocessed tmrg `translate` directive.",
            "This can happen if it is unmatched or if it is interleaved with other translate/copy/ignore directives.",
            "Please review your code in order to make sure the directives are interpreted correctly."
    ])
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".v", "TMR.v")
    assert os.path.isfile(tmr_file)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file, format_before=False)

def test_tmrg_copy_multiple_top(tmrg, capfd):
    verilog_file = "verilog/copy_multiple_top.v"
    syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog"])
    assert not tmrg([file_in_test_dir(verilog_file), "--no-header","--no-common-definitions"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr="The design has multiple top cells! Output may not be correct!")
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".v", "TMR.v")
    syntax_check(tmr_file, cmds=["iverilog"],common=True)
    assert os.path.isfile(tmr_file)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file)

def test_tmrg_copy_illegal_syntax(tmrg, capfd):
    verilog_file = "verilog/copy_illegal_syntax.v"
    assert not tmrg([file_in_test_dir(verilog_file), "--no-header","--no-common-definitions"])
    assert_output_streams(capfd)
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".v", "TMR.v")
    assert os.path.isfile(tmr_file)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file, format_before=False)

def test_tmrg_comments_preserve(tmrg, capfd):
    verilog_file = "verilog/comments.v"
    assert not tmrg([file_in_test_dir(verilog_file), "--no-header", "--no-common-definitions", "--comments"])
    assert_output_streams(capfd)
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".v", "TMR.v")
    assert os.path.isfile(tmr_file)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file, format_before=False)

def test_tmrg_pragma_preserve(tmrg, capfd):
    verilog_file = "verilog/custom_pragma.v"
    assert not tmrg([file_in_test_dir(verilog_file), "--no-header", "--no-common-definitions", "--pragmas", "asicsupercharge,fixitall,unusedpragma"])
    assert_output_streams(capfd)
    basename = os.path.basename(verilog_file)
    tmr_file = basename.replace(".v", "TMR.v")
    assert os.path.isfile(tmr_file)
    golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
    assert os.path.isfile(golden_file)
    equal_check(golden_file,tmr_file, format_before=False)

def test_tmrg_assignment_mistake(tmrg, capfd):
    verilog_file = "verilog/tmrg_assignment_mistake.v"
    syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog"])
    assert not tmrg([file_in_test_dir(verilog_file), "--no-header","--no-common-definitions"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=[
        "Detected voted assignment `belloVoted` doesn't match the right hand side net `brutto`!",
        "Detected voted assignment `bruttoVoted` doesn't have a net reference on the right hand side: `brutto&cancane`",
    ])

class TestTmrgOnSingleVerilogFileVsGolden():
    @pytest.mark.parametrize(
        'verilog_file', [
            "verilog/argumentless_task.v",
            "verilog/comment_inline_tmrg.v",
            "verilog/comments_among_ports.v",
            "verilog/defparam.v",
            "verilog/defparam_no_tmr.v",
            "verilog/fanin_dont_touch.v",
            "verilog/superlint.v",
            "verilog/synopsys_pragma.v",
            "verilog/tmrerr.v",
            "verilog/translate_outside.v",
        ]
    )

    def test_tmrg_on_file(self, tmrg, capfd, verilog_file):
        syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog"])
        assert not tmrg([file_in_test_dir(verilog_file),"--no-header","--no-common-definitions"])
        basename = os.path.basename(verilog_file)
        tmr_file = basename.replace(".v", "TMR.v")
        assert_output_streams(capfd)
        assert os.path.isfile(tmr_file)
        syntax_check(tmr_file, cmds=["iverilog"],common=True)
        golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".v","TMR.v"))
        assert os.path.isfile(golden_file)
        equal_check(golden_file,tmr_file)

class TestTmrgOnSingleVerilogFile():
    @pytest.mark.parametrize(
        'verilog_file', [
            "verilog/always.v",
            "verilog/alwaysComma.v",
            "verilog/case01.v",
            "verilog/dreg.v",
            "verilog/function.v",
            "verilog/index.v",
            "verilog/lib.v",
            "verilog/params3.v",
            "verilog/test03.v",
            "verilog/translate.v",
            "verilog/always.v",
            "verilog/case02.v",
            "verilog/generate01.v",
            "verilog/assigment03.v",
            "verilog/generate04.v",
            "verilog/initial01.v",
            "verilog/logic.v",
            "verilog/params4.v",
            "verilog/params5.v",
            "verilog/testsRadu.v",
            "verilog/var.v",
            "verilog/instanceInOut.v",
            "verilog/ansiPorts.v",
            "verilog/comb00.v",
            "verilog/defines.v",
            "verilog/hier01.v",
            "verilog/sysCall.v",
            "verilog/inlineif01.v",
            "verilog/mux.v",
            "verilog/params.v",
            "verilog/delay2.v",
            "verilog/tmr1.v",
            "verilog/wire.v",
            "verilog/arrays.v",
            "verilog/comb02.v",
            "verilog/forLoop.v",
            "verilog/for.v",
            "verilog/hier02.v",
            "verilog/instantiation.v",
            "verilog/netdeclaration.v",
            "verilog/portDeclaration.v",
            "verilog/port_names.v",
            "verilog/tmr2.v",
            "verilog/assigment.v",
            "verilog/compDirectives.v",
            "verilog/fsm01.v",
            "verilog/hier03.v",
            "verilog/instTmrError.v",
            "verilog/notxor.v",
            "verilog/slice01.v",
            "verilog/noports.v",
            "verilog/tmr3.v",
            "verilog/ifdef.v",
            "verilog/underscore.v",
            "verilog/begin.v",
            "verilog/complexInst.v",
            "verilog/function2.v",
            "verilog/function3.v",
            "verilog/params2.v",
            "verilog/test02.v",
            "verilog/tmrError01.v",
            "verilog/tmrError04.v",
            "verilog/tmrError05.v",
            "verilog/tmrError06.v",
            "verilog/tmrErrorExclude.v",
            "verilog/vectorRange.v",
            "verilog/generate05.v",
            "verilog/generate06.v",
            "verilog/wor01.v",
            "verilog/wor02.v",
            "verilog/generate_if.v",
            "verilog/force_release.v",
            "verilog/signed_literal.v",
            "verilog/default_nettype.v",
            "verilog/compDirectivesComment.v",
            "verilog/casez01.v",
            "verilog/casez02.v",
            "verilog/casex01.v",
            "verilog/casex02.v",
            "verilog/multidimentionalarray.v",
            "verilog/full_case.v",
            "verilog/full_parallel_case.v",
            "verilog/parallel_case.v",
            "verilog/parallel_full_case.v",
            "verilog/case_item_expression.v",
            "verilog/fanin.v",
            "verilog/reg_initial_value1.v",
            "verilog/reg_initial_value2.v",
            "verilog/reg_initial_value3.v",
            "verilog/inline_attribute_instance.v",
            "verilog/inline_attribute_module_a.v",
            "verilog/inline_attribute_module_b.v",
            "verilog/inline_attribute_module_c.v",
            "verilog/inline_attribute_module_d.v",
            "verilog/inline_attribute_module_e.v",
            "verilog/inline_attribute_module_f.v",
            "verilog/inline_attribute_signal.v",
            "verilog/inline_attribute_case.v",
            "verilog/label.v",
            "verilog/macros.v",
            "verilog/tmrerr.v",
            "verilog/empty_signals.v",
            "verilog/parameter_switch.v",
            "verilog/comments_2slash.v",
            "../examples/slice.v",
            "../examples/clockGating01.v",
            "../examples/comb02.v",
            "../examples/comb05.v",
            "../examples/dff.v",
            "../examples/fsm03.v",
            "../examples/inst02.v",
            "../examples/resetBlock02.v",
            "../examples/tmrOut01.v",
            "../examples/clockGating02.v",
            "../examples/clockGating02.v",
            "../examples/configcell.v",
            "../examples/comb03.v",
            "../examples/comb06.v",
            "../examples/fsm01.v",
            "../examples/fsm04.v",
            "../examples/inst03.v",
            "../examples/resetBlock03.v",
            "../examples/vote01.v",
            "../examples/comb01.v",
            "../examples/comb04.v",
            "../examples/comb07.v",
            "../examples/fsm02.v",
            "../examples/inst01.v",
            "../examples/resetBlock01.v",
            "../examples/resetBlock04.v",
            "../examples/vote02.v",
            "../examples/pipelineWithSeuCoutner.v",
        ]
    )

    def test_tmrg_on_file(self, tmrg, capfd, verilog_file):
      syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog"])
      assert not tmrg([file_in_test_dir(verilog_file)])
      basename = os.path.basename(verilog_file)
      expected_tmr_file = basename.replace(".v", "TMR.v")
      assert_output_streams(capfd)
      assert os.path.isfile(expected_tmr_file)
      syntax_check(expected_tmr_file, cmds=["iverilog"])


class TestTmrgOnSingleSystemVerilogFile():
    @pytest.mark.parametrize(
        'verilog_file', [
            "systemverilog/endmodule_label_dff.sv",
            "systemverilog/real_signals.sv",
            "systemverilog/copy_ok.sv",
            "systemverilog/dff_logic.sv",
            "systemverilog/dff_logic_port.sv",
            "systemverilog/dff_always_ff.sv",
            "systemverilog/dff_always_ff01.sv",
            "systemverilog/dff_always_ff02.sv",
            "systemverilog/dff_always_ff03.sv",
            "systemverilog/dff_always_ff04.sv",
            "systemverilog/decoder_using_unique_case.sv",
            "systemverilog/decoder_using_unique_casex.sv",
            "systemverilog/decoder_using_unique_casez.sv",
            "systemverilog/always_comb_01.sv",
            "systemverilog/always_comb_02.sv",
            "systemverilog/always_comb_03.sv",
            "systemverilog/always_comb_04.sv",
            "systemverilog/always_comb_05.sv",
            "systemverilog/always_comb_06.sv",
            "systemverilog/always_latch_01.sv",
            "systemverilog/always_latch_02.sv",
            "systemverilog/always_latch_03.sv",
            "systemverilog/always_latch_04.sv",
            "systemverilog/clog2.sv",
            "systemverilog/empty_parameters.sv",
            "systemverilog/empty_parameters_signals.sv",
            "systemverilog/for_loops.sv",
            "systemverilog/assignment_operators.sv",
            "systemverilog/case_if_modifiers.sv",
            "systemverilog/forloop_generate01.sv",
            "systemverilog/forloop_generate02.sv",
            "systemverilog/function_automatic.sv",
            "systemverilog/function_static.sv",
            "systemverilog/rhs_assign.sv",
            "systemverilog/forinalways.sv",
            "systemverilog/forinalwaysff.sv",
            "systemverilog/forinalwayscomb.sv",
            "systemverilog/ifndef.sv",
            "systemverilog/inside.sv",
            "systemverilog/unpacked_array_ranges.sv",
            "systemverilog/unpacked_array_sizes.sv",
            "systemverilog/forloopcast.sv",
            "systemverilog/unpacked_array_ranges_port.sv",
            "systemverilog/unpacked_array_sizes_port.sv",
            "systemverilog/multidimentionalarray_port_ranges.sv",
            "systemverilog/multidimentionalarray_port_sizes.sv",
            "systemverilog/package_notfound.sv",
            "systemverilog/localparam.sv",
            "systemverilog/parameter.sv",
            "systemverilog/parameter1.sv",
            "systemverilog/rhs_trucation.sv",
            "systemverilog/issue_59.sv",
            "systemverilog/unpacked_array_fanout.sv",
            "systemverilog/blocking_procedural_assignment_andequal.sv",
            "systemverilog/blocking_procedural_assignment_divideequal.sv",
            "systemverilog/blocking_procedural_assignment_equal.sv",
            "systemverilog/blocking_procedural_assignment_minusequal.sv",
            "systemverilog/blocking_procedural_assignment_moduloequal.sv",
            "systemverilog/blocking_procedural_assignment_multiequal.sv",
            "systemverilog/blocking_procedural_assignment_orequal.sv",
            "systemverilog/blocking_procedural_assignment_plusequal.sv",
            "systemverilog/blocking_procedural_assignment_shiftlequal.sv",
            "systemverilog/blocking_procedural_assignment_shiftllequal.sv",
            "systemverilog/blocking_procedural_assignment_shiftrequal.sv",
            "systemverilog/blocking_procedural_assignment_shiftrrequal.sv",
            "systemverilog/structs_01.sv",
            "systemverilog/enums_01.sv",
            "systemverilog/enums_03.sv",
            "systemverilog/type_namespace.sv",
            "systemverilog/reg_initial_value1.sv",
            "systemverilog/reg_initial_value2.sv",
            "systemverilog/reg_initial_value3.sv",
            "systemverilog/inline_attribute_signal.sv",
            "systemverilog/inline_attribute_lrm_a.sv",
            "systemverilog/inline_attribute_lrm_b.sv",
            "systemverilog/label.sv",
            "systemverilog/label1.sv",
            "systemverilog/label2.sv",
            "systemverilog/tmrerr.sv",
            "systemverilog/real_underscore.sv",
            "systemverilog/streaming.sv",
            "systemverilog/task_automatic.sv",
            "systemverilog/priority_case.sv",
            "systemverilog/union.sv",
            "systemverilog/union_macro.sv",
            "asicworld/systemverilog/return_value_function.sv",
        ]
    )

    def test_tmrg_on_file(self, tmrg, capfd, verilog_file):
      syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog -g2012","verible-verilog-syntax"])
      assert not tmrg([file_in_test_dir(verilog_file)])
      basename = os.path.basename(verilog_file)
      expected_tmr_file = basename.replace(".sv", "TMR.sv")
      assert_output_streams(capfd)
      assert os.path.isfile(expected_tmr_file)
      syntax_check(expected_tmr_file, cmds=["iverilog -g2012","verible-verilog-syntax"])


class TestTmrgOnSingleSystemVerilogFileOnlyIverilog():
    """This class is to test a SystemVerilog file only using iverilog.
    Verible can be too permissive then testing the output of TMRG, leading to bugs
    splipping throught the CI. If this happens, add the files for this list instead
    of using TestTmrgOnSingleSystemVerilogFile"""
    @pytest.mark.parametrize(
        'verilog_file', [
            #"systemverilog/issue_53.sv",
        ]
    )

    def test_tmrg_on_file(self, tmrg, capfd, verilog_file):
      syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog -g2012"])
      assert not tmrg([file_in_test_dir(verilog_file)])
      basename = os.path.basename(verilog_file)
      expected_tmr_file = basename.replace(".sv", "TMR.sv")
      assert_output_streams(capfd)
      assert os.path.isfile(expected_tmr_file)
      syntax_check(expected_tmr_file, cmds=["iverilog -g2012"])


class TestTmrgOnSingleSystemVerilogFileOnlyVerible():
    """This class is to test a SystemVerilog file only using Verible.
    Some syntaxes are not allowed in iVerilog even if supported by Systeverilog LRM.
    If this happens, add the files for this list instead of using
    TestTmrgOnSingleSystemVerilogFile"""
    @pytest.mark.parametrize(
        'verilog_file', [
            "systemverilog/inline_attribute_signal_voter.sv",
        ]
    )

    def test_tmrg_on_file(self, tmrg, capfd, verilog_file):
      syntax_check(file_in_test_dir(verilog_file), cmds=["verible-verilog-syntax"])
      assert not tmrg([file_in_test_dir(verilog_file)])
      basename = os.path.basename(verilog_file)
      expected_tmr_file = basename.replace(".sv", "TMR.sv")
      assert_output_streams(capfd)
      assert os.path.isfile(expected_tmr_file)
      syntax_check(expected_tmr_file, cmds=["verible-verilog-syntax"])


class TestTmrgOnSingleSystemverilogFileVsGolden():
    @pytest.mark.parametrize(
        'verilog_file', [
            "systemverilog/tmrerr.sv",
            "systemverilog/tmrerr1.sv",
            "systemverilog/package_import_width.sv",
            "systemverilog/package_import.sv",
            "systemverilog/parameter3.sv",
            "systemverilog/always_comb_import.sv",
            #"systemverilog/always_comb_import_unused_function.sv",
            "systemverilog/always_comb_importstar.sv",
            "systemverilog/always_comb_import_for.sv",
            "systemverilog/always_ff_import_for_01.sv",
            "systemverilog/array_struct_assign_pattern.sv",
            "systemverilog/assignment_delay.sv",
            "systemverilog/casexz_modifiers.sv",
            "systemverilog/case_inside.sv",
            "systemverilog/cast_expr.sv",
            "systemverilog/concat.sv",
            "systemverilog/concat2.sv",
            "systemverilog/concat3.sv",
            "systemverilog/concat4.sv",
            "systemverilog/concat5.sv",
            "systemverilog/concat6.sv",
            "systemverilog/concat7.sv",
            "systemverilog/for_create_beginend.sv",
            "systemverilog/forever.sv",
            "systemverilog/func_call_namespace.sv",
            "systemverilog/generate_case.sv",
            "systemverilog/local_variables_nested.sv",
            "systemverilog/local_variables_nested1.sv",
            "systemverilog/local_variables_nested2.sv",
            "systemverilog/net_lifetime.sv",
            "systemverilog/package_import1.sv",
            "systemverilog/package_import2.sv",
            "systemverilog/package_import_hdr.sv",
            "systemverilog/package_complete.sv",
            "systemverilog/package_compdirective.sv",
            "systemverilog/parameter_custom_type.sv",
            "systemverilog/parameter_package_type.sv",
            "systemverilog/preserve_params_import_order.sv",
            "systemverilog/preserve_params_import_types_order.sv",
            "systemverilog/timeunit.sv",
            "systemverilog/timeunit1.sv",
            "systemverilog/timeunit2.sv",
            "systemverilog/timeunits_order.sv",
            "systemverilog/timeunits_order2.sv",
            "systemverilog/typedef_global.sv",
            "systemverilog/typedef_package.sv",
            "systemverilog/loops.sv",
            "systemverilog/wait.sv",
        ]
    )

    def test_tmrg_on_file(self, tmrg, capfd, verilog_file):
        syntax_check(file_in_test_dir(verilog_file), cmds=["iverilog -g2012","verible-verilog-syntax"])
        assert not tmrg([file_in_test_dir(verilog_file),"--no-header","--no-common-definitions"])
        basename = os.path.basename(verilog_file)
        tmr_file = basename.replace(".sv", "TMR.sv")
        assert_output_streams(capfd)
        assert os.path.isfile(tmr_file)
        syntax_check(tmr_file, cmds=["iverilog -g2012","verible-verilog-syntax"],common=True)
        golden_file = file_in_test_dir(verilog_file.replace("verilog/","verilog/golden/").replace(".sv","TMR.sv"))
        assert os.path.isfile(golden_file)
        equal_check(golden_file,tmr_file)
