import os
import pytest
from .common import *
from tmrg.wrg import main as wrg_main

@pytest.fixture
def wrg(monkeypatch):
    return CliExecutor(monkeypatch, "wrg", main_function=wrg_main)

def test_wrg_no_arguments(wrg, capfd):
    assert wrg()
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["You have to specify verilog file name"])

def test_wrg_missing_file(wrg, capfd):
    assert wrg(["not_existing.v"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["File or directory does not exists"])

def test_wrg_version(wrg, capfd):
    assert not wrg(["--version"])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["WRG"])

def test_wrg_help(wrg, capfd):
    assert not wrg(["--help"])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["Usage: wrg", "Options:", "--verbose"])

def test_wrg_invalid_option(wrg, capfd):
    assert wrg(["--no-such-option"])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["no such option"])
    
def test_wrg_working(wrg, capfd):
    assert not wrg([file_in_test_dir("verilog/comb00.v"),])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_in_stdout=["comb00Wrapper",])

def test_wrg_error_in_verilog(wrg, capfd):
    file_name = os.path.join(os.path.dirname(__file__), "systemverilog/elaboration_order_b.sv")
    assert wrg([file_name])
    assert_output_streams(capfd, expect_stderr_empty=False, expect_in_stderr=["No modules found. Please refer to the documentation using 'tmrg --help' or 'tmrg --doc'"])

def test_wrg_working_very_verbose(wrg, capfd):
    assert not wrg([file_in_test_dir("verilog/comb00.v"), '-vv'])
    assert_output_streams(capfd, expect_stdout_empty=False, expect_stderr_empty=False, expect_in_stderr=["Module comb00",], expect_in_stdout=["comb00Wrapper",])

