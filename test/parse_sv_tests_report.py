#!/usr/bin/env python3
"""Convert sv-tests CSV report into JUnit-style XML report"""
import xml.etree.ElementTree as etree
import csv
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("csv_report", help="CSV input report file name")
parser.add_argument("xml_report", help="XML Junit output file name")
args = parser.parse_args()

with open(args.csv_report, "r", encoding="utf-8") as infile:
    tests = list(csv.DictReader(infile, delimiter=","))


root = etree.Element("testsuites")
ts = etree.SubElement(root, "testsuite", attrib={"name": "sv-tests"})
for test in tests:
    tc = etree.SubElement(ts, "testcase", attrib={"name": test["TestName"]})
    if test["Pass"] == "False":
        etree.SubElement(
            tc, "failure", attrib={"message": f"Exit code: {test['ExitCode']}"}
        )

with open(args.xml_report, "w", encoding="utf-8") as outfile:
    outfile.write(etree.tostring(root, encoding="unicode"))
