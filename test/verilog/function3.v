module functionTest;
 // tmrg do_not_triplicate a b c d myfunction
  
 function [2:1] myfunction1;
   input a, b, c, d;
   begin
     myfunction1 [1] = 1'b0;
     myfunction1 = ((a+b) + (c-d));
   end
 endfunction

 function [6:3] myfunction2;
   input a, b, c, d;
   begin
     myfunction2 [6] = 1'b0;
     myfunction2 = ((a+b) + (c-d));
   end
 endfunction

 reg clk, q1, q2;
 reg [3:0] data;

 always @(posedge clk)
  begin
    q1=|myfunction1(data[0],data[1],data[2],data[3]);
    q2=|myfunction2(data[3],data[2],data[1],data[0]);
  end
endmodule
