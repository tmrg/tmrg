module delay(input clk, input d, output reg q, input state_d, output reg state_q);
  
  // Registers
  always @(posedge clk) begin
    q <= #1 d;
    state_q <= #1 state_d;
  end
endmodule

