module i (
	input wire Clock,
	output wire Ciao
);
//tmrg do_not_touch
endmodule


module t (
	input wire Clock,
	input wire In,
	output logic Out
);

//tmrg default triplicate
//tmrg do_not_triplicate Out

logic tmp;
always @(posedge Clock)
	tmp <= In;

wire ClockA = Clock;
always @(posedge ClockA)
	Out <= tmp;

i example (
	.Clock(ClockA),
	.Ciao ()
);

endmodule
