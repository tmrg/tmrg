module test
  (input reset,
   input [7:0] rwmem
   );

  wire [7:0] rwmemVoted = rwmem;
  wire   rwmemTmrError = 1'b0;
  wire  Clk_gated_enable = reset || rwmemTmrError;
endmodule
