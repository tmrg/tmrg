module child (output wire o);

//tmrg do_not_touch

parameter CIAO = 1;
parameter CIAONE = 1;
endmodule

module top;

//tmrg default do_not_triplicate
wire w;

defparam child0.CIAO = 2;
defparam child0.CIAONE = 20;
child child0(.o(w));

endmodule
