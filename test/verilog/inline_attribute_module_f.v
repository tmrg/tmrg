(* A,B="yes" *) module f
  ((* C="no" *) input c,
   (* D *) output d,
   (* E,F=1 *)output [4:0] e);
  assign d = c;
  assign e = 5'h10;
endmodule
