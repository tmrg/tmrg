(* AttribUte = "Value0" *) module b
  (input c,
   output d,
   output [4:0] e);
  assign d = c;
  assign e = 5'h10;
endmodule
