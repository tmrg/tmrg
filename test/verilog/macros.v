module test;
`define W 5
wire [`W-1:0] a;
wire [`W-1:0] c = a - `W'b001;
endmodule
