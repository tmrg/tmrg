module test
  (input logic[3:0] in,
   output logic[1:0] out0,
   output logic[1:0] out1
   );
  //tmrg default triplicate
  assign {out1, out0} = in;
endmodule
