module a (
	// lint_checking stop
	input wire acea,
	// lint_checking stop
	input wire clock
	// lint_checking stop
);

endmodule

module b;

wire clock;
a a1 (
	.clock(clock),
	// lint_checking off
	.acea(1'b1)
), a2 (
	// lint_checking on
	.clock(clock)
	// lint_checking off
);

endmodule

module c (
	// lint_checking a
	cane,
	// lint_checking b
	gatto,
	// lint_checking c
	gatto2
);
input wire cane;
output wire gatto;
inout wire gatto2;
// tmrg default triplicate
// tmrg do_not_triplicate gatto

b thisisit (
	// lint_checking all of this
);

endmodule
