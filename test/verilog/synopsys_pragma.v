module test(input clock_i,
            input reset_i,
            output stream_output_o);
  logic shifter2sel_r;
  logic [1:0] shifter;

  //synopsys async_set_reset "reset_i"
  always @(negedge clock_i or posedge reset_i)
    if (reset_i) shifter2sel_r <= 1'b1;
    else shifter2sel_r <= ~shifter2sel_r;

  assign stream_output_o = (clock_i) ? shifter[0] : shifter[1];
endmodule
