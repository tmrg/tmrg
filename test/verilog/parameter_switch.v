module mux;
endmodule

module mux1;
endmodule

module test1 #(parameter BEHAVIORAL_MUX = 1);
  if (BEHAVIORAL_MUX == 1) begin : gen_behavioral_mux
    mux mux ();
  end else begin : gen_and_or_mux
    mux1 mux();
  end
endmodule
