module testTMR (
    input resetA,
    input resetB,
    input resetC,
    input [7:0] rwmemA,
    input [7:0] rwmemB,
    input [7:0] rwmemC
);
  wor rwmemTmrErrorC;
  wire [7:0] rwmemVotedC;
  wor rwmemTmrErrorB;
  wire [7:0] rwmemVotedB;
  wor rwmemTmrErrorA;
  wire [7:0] rwmemVotedA;
  wire Clk_gated_enableA = resetA || rwmemTmrErrorA;
  wire Clk_gated_enableB = resetB || rwmemTmrErrorB;
  wire Clk_gated_enableC = resetC || rwmemTmrErrorC;

  majorityVoter #(
      .WIDTH(8)
  ) rwmemVoterA (
      .inA(rwmemA),
      .inB(rwmemB),
      .inC(rwmemC),
      .out(rwmemVotedA),
      .tmrErr(rwmemTmrErrorA)
  );

  majorityVoter #(
      .WIDTH(8)
  ) rwmemVoterB (
      .inA(rwmemA),
      .inB(rwmemB),
      .inC(rwmemC),
      .out(rwmemVotedB),
      .tmrErr(rwmemTmrErrorB)
  );

  majorityVoter #(
      .WIDTH(8)
  ) rwmemVoterC (
      .inA(rwmemA),
      .inB(rwmemB),
      .inC(rwmemC),
      .out(rwmemVotedC),
      .tmrErr(rwmemTmrErrorC)
  );
endmodule
