module buffer (
  input I,
  output Z
);
endmodule

module dutTMR;
wor originTmrErrorC;
wire originVotedC;
wor originTmrErrorB;
wire originVotedB;
wor originTmrErrorA;
wire originVotedA;
wire single;
wire originA;
wire originB;
wire originC;

buffer b (
    .I(originA),
    .Z(single)
    );

majorityVoter originVoterA (
    .inA(originA),
    .inB(originB),
    .inC(originC),
    .out(originVotedA),
    .tmrErr(originTmrErrorA)
    );

majorityVoter originVoterB (
    .inA(originA),
    .inB(originB),
    .inC(originC),
    .out(originVotedB),
    .tmrErr(originTmrErrorB)
    );

majorityVoter originVoterC (
    .inA(originA),
    .inB(originB),
    .inC(originC),
    .out(originVotedC),
    .tmrErr(originTmrErrorC)
    );
endmodule

