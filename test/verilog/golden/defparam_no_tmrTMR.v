module child (
  output wire o
);
parameter CIAO = 1;
parameter CIAONE = 1;
endmodule

module topTMR;
wire w;
defparam child0.CIAO = 2;
defparam child0.CIAONE = 20;

child child0 (
    .o(w)
    );
endmodule

