module dut;
  parameter CIAO = 1;
  initial $display("CIAO: %d", CIAO);
endmodule

module topTMR;

  dut dutA ();

  dut dutB ();

  dut dutC ();
endmodule

module tbTMR;
  defparam top.dutA.CIAO = 99, top.dutB.CIAO = 99, top.dutC.CIAO = 99;
  topTMR top ();
endmodule

