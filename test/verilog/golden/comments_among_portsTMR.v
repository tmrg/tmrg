module aTMR (
  // lint_checking stop
  input wire aceaA,
  input wire aceaB,
  input wire aceaC,
  // lint_checking stop
  input wire clockA,
  input wire clockB,
  input wire clockC
  // lint_checking stop
);
endmodule

module bTMR;
wire clockA;

wire clockB;

wire clockC;

aTMR a1 (
    .clockA(clockA),
    .clockB(clockB),
    .clockC(clockC),
    // lint_checking off
    .aceaA(1'b1),
    .aceaB(1'b1),
    .aceaC(1'b1)
  ), a2 (
    // lint_checking on
    .clockA(clockA),
    .clockB(clockB),
    .clockC(clockC)
    // lint_checking off
  );
endmodule

module cTMR (
  // lint_checking a
  caneA,
  caneB,
  caneC,
  // lint_checking b
  gatto,
  // lint_checking c
  gatto2A,
  gatto2B,
  gatto2C
);
input wire caneA;
input wire caneB;
input wire caneC;
output wire gatto;
inout wire gatto2A;
inout wire gatto2B;
inout wire gatto2C;

bTMR thisisit (
    // lint_checking all of this
  );
endmodule

