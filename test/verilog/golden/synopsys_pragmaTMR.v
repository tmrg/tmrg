module testTMR (
  input clock_iA,
  input clock_iB,
  input clock_iC,
  input reset_iA,
  input reset_iB,
  input reset_iC,
  output stream_output_oA,
  output stream_output_oB,
  output stream_output_oC
);
logic shifter2sel_rA;
logic shifter2sel_rB;
logic shifter2sel_rC;
logic [1:0] shifterA;
logic [1:0] shifterB;
logic [1:0] shifterC;
// synopsys async_set_reset reset_iA
// synopsys async_set_reset reset_iB
// synopsys async_set_reset reset_iC

always @( negedge clock_iA or posedge reset_iA )
  if (reset_iA)
    shifter2sel_rA <= 1'b1;
  else
    shifter2sel_rA <= ~shifter2sel_rA;

always @( negedge clock_iB or posedge reset_iB )
  if (reset_iB)
    shifter2sel_rB <= 1'b1;
  else
    shifter2sel_rB <= ~shifter2sel_rB;

always @( negedge clock_iC or posedge reset_iC )
  if (reset_iC)
    shifter2sel_rC <= 1'b1;
  else
    shifter2sel_rC <= ~shifter2sel_rC;
assign stream_output_oA = (clock_iA) ? shifterA[0] : shifterA[1];
assign stream_output_oB = (clock_iB) ? shifterB[0] : shifterB[1];
assign stream_output_oC = (clock_iC) ? shifterC[0] : shifterC[1];
endmodule

