module dut;

//tmrg do_not_touch
parameter CIAO = 1;
initial $display("CIAO: %d", CIAO);

endmodule

module top;

// tmrg default triplicate
dut dut();

endmodule

module tb;

defparam top.dut.CIAO = 99;
top top();

endmodule
