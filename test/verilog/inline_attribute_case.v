module test
   (input clk,
    input [5:0] a,
    input [3:0] b,
    input [3:0] c,
    input [2:0] sel,
    output reg [3:0] out,
    output reg [3:0] out1

   );

  always @(posedge clk) begin
    (* full_case *)
    case (sel)
      3'b100: out <= a[3:0];
      3'b100: out <= b;
      3'b100: out <= c;
    endcase
  end

  always @(*) begin
    (* full_case *)
    case (sel)
      3'b100: out1 = a[3:0];
      3'b100: out1 = b;
      3'b100: out1 = c;
    endcase
  end
endmodule
