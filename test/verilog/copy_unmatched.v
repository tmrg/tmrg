module dut;

// tmrg copy start
wire this_will_be_preserved;
// tmrg copy stop
// tmrg ignore start
wire this_will_be_removed;
// tmrg ignore stop
// tmrg copy start
wire this_also_will_be_preserved;
// tmrg copy stop
// tmrg translate on
wire this_will_be_triplicated;
// tmrg translate off
wire this_will_also_be_removed;
// tmrg translate on
endmodule

