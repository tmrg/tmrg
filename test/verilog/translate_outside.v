//tmrg translate off
`define CIAO 1
// tmrg translate on

module translate_test(
  input i1,
  input [1:0] i2,i3,
  output x);
  // tmrg translate off
  assign x=|i1 & ^i3;
  // tmrg translate on
  // tmrg translate off
  `ifdef ABV_ON
  assert property (@(posedge i1) i2==2'b11 |-> i3 ==2'b00);
  `endif
  // tmrg translate on
endmodule

