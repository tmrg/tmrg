`define DEL 500

module delay;

  // tmrg do_not_triplicate del2
  // tmrg do_not_triplicate del_ref

  wire [5:0] del = 5;
  wire [1:0] del1 = 1;
  wire [5:0] del2 = 5;

  wire [5:0] del3 = 5;
  wire [5:0] del_ref = del3;

  initial
    begin
      #1;
      #100;
      #(100);
      #1000_000;
      #12.5ns;
      #500us;
      #(12.5*1ns);
      #(500*1us);
      #del;
      #(del*1ns);
      #(del1*1ns);
      #`DEL;
      #(`DEL+1);
      #(`DEL+del);
      #(del);
      #del_ref;
    end    
endmodule

