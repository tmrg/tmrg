module dut;

localparam cancane = 0;

wire brutto;
wire belloVoted = brutto;
wire bruttoVoted = brutto & cancane;

wire ciaoA = brutto;
wire ciaoB = brutto & ciaoA;

endmodule
