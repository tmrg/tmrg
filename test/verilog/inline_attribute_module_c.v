(* AttribUte1 = "0" *) module c #(parameter A = 5)
  (output [A-1:0] e);
  assign e = A;
endmodule
