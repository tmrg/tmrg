module a (input clk);
endmodule

module b(clk);
  input wire clk;
  (* ATTR = "no_" *) a a_i(.clk(clk));
endmodule

module c;
  wire clk;
  (* DONT_TOUCH=1*)(* ATTR = "no_" *) a a_i(.clk(clk));
  (* DONT_TOUCH*)(* ATTR= "no_" *) b b_o(.clk(clk));
endmodule
