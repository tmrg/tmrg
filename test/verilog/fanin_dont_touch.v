module buffer ( input I, output Z);
	// tmrg do_not_touch
  endmodule
  
module dut;

//tmrg do_not_triplicate single
wire single;

wire origin;
wire originVoted = origin;
wire originA = origin;

// tmrg do_not_triplicate b
buffer b(
	.I (originA),
	.Z (single)
);

endmodule
  