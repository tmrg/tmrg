module a (input [5:0] b);
  (* ATTRIBUTE = "value" *) reg s = 1'b0;
  (* ATTRIBUTE = "value" *) reg x;
  (* ATTRIBUTE = "value"*) reg [5:0] v;
  (* ATTRIBUTE = "value" *) reg u[3:0];
  (*DONT_TOUCH = "true" *) wire y;
  (* DONT_TOUCH,ATTRIBUTE="no" *) wire w [3:0];
  (* DONT_TOUCH,
   ATTRIBUTE="no" *) wire z [3:0];
  (* DONT_TOUCH = "yes" *) wire [5:0] bVoted;
  assign bVoted = b;
endmodule
