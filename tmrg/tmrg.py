#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) CERN and the TMRG authors.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import traceback
import os
import filecmp
import copy
import time
import getpass
import socket
import datetime
import hashlib
import zipfile
import logging
import sys
import textwrap
import shutil
from optparse import OptionParser, OptionGroup
from .verilog_elaborator import VerilogElaborator, ErrorMessage, readFile, Signal, Structure, Union, Interface, Enumeration
try:
    from cPyparsing import ParseResults, alphas, alphanums, Literal, Regex, Suppress, OneOrMore, oneOf, Group, Optional
except:
    from pyparsing import ParseResults, alphas, alphanums, Literal, Regex, Suppress, OneOrMore, oneOf, Group, Optional
from .toolset import tmrg_version, epilog, runCommand, makeSureDirExists, startDocumentation

def mul(l):
    if not l:
        return 0

    r = 1
    for i in l:
        r *= l

    return r

class CmdConstrainParser:
    def __init__(self):
        self.semi = Literal(";")
        self.lpar = Literal("(")
        self.rpar = Literal(")")
        self.equals = Literal("=")
        self.constraints = {"triplicate": set(), "do_not_triplicate": set(), "default": True, "tmr_error": False}

        identLead = alphas+"$_"
        identBody = alphanums+"$_"
        # pylint: disable=too-many-function-args
        identifier1 = Regex(r"\.?["+identLead+"]["+identBody+"]*(\.["+identLead+"]["+identBody+"]*)*"
                            ).setName("baseIdent")
        identifier2 = Regex(r"\\\S+").setParseAction(lambda t: t[0][1:]).setName("escapedIdent")
        identifier = (identifier1 | identifier2).setResultsName("id")

        self.directive_doNotTriplicate = (Suppress("do_not_triplicate") +
                                          OneOrMore(identifier)).setResultsName("directive_do_not_triplicate")
        self.directive_triplicate = (Suppress("triplicate") + OneOrMore(identifier)
                                     ).setResultsName("directive_triplicate")
        self.directive_default = (Suppress("default") + oneOf("triplicate do_not_triplicate") +
                                  Group(Optional(identifier))).setResultsName("directive_default")
        self.directive_tmr_error = (Suppress("tmr_error") + oneOf("true false") +
                                    Group(Optional(identifier))).setResultsName("directive_tmr_error")
        self.directive_do_not_touch = (Suppress("do_not_touch") + OneOrMore(identifier)
                                       ).setResultsName("directive_do_not_touch")

        self.directiveItem = (self.directive_triplicate |
                              self.directive_doNotTriplicate |
                              self.directive_default |
                              self.directive_tmr_error |
                              self.directive_do_not_touch
                              )

    def parse(self, s):
        try:
            return self.directiveItem.parseString(s)
        except:
            raise ErrorMessage("Error during parsing command line provided constrains (%s)" % s)

class VoterOrFanout:
    def __init__(self):
        self.a = ""
        self.b = ""
        self.c = ""
        self.u = ""
        self.error_net = ""
        self.base_signal = None
        self.group = ""
        self.add_wires = ""

class TMR(VerilogElaborator):
    def __init__(self, options, args):
        VerilogElaborator.__init__(self, options, args, cnfgName="tmrg")
        self.EXT = ('A', 'B', 'C')
        self.__voterPresent = False
        self.__fanoutPresent = False
        self.__init_tripclicate_callbacks()

        # command line specified config files
        for fname in self.options.config:
            if os.path.exists(fname):
                logging.debug("Loading command line specified config file from %s" % fname)
                self.config.read(fname)
                if self.options.generateBugReport:
                    bn = os.path.basename(fname)
                    fcopy = os.path.join(self.options.bugReportDir, "cmd_%s.cfg" % bn)
                    logging.debug("Coping  command line specified config file from '%s' to '%s'" % (fname, fcopy))
                    shutil.copyfile(fname, fcopy)
            else:
                raise ErrorMessage("Command line specified config file does not exists at %s" % fname)
        if "tmr_dir" in dir(self.options) and self.options.tmr_dir:
            logging.debug("Setting tmr_dir to %s" % self.options.tmr_dir)
            self.config.set("tmrg", "tmr_dir", self.options.tmr_dir)

        if "rtl_dir" in dir(self.options) and self.options.rtl_dir:
            logging.debug("Setting rtl_dir to %s" % self.options.rtl_dir)
            self.config.set("tmrg", "rtl_dir", self.options.rtl_dir)

        if "tmr_suffix" in dir(self.options) and self.options.tmr_suffix:
            logging.debug("Setting tmr_suffix to %s" % self.options.tmr_suffix)
            self.config.set("tmrg", "tmr_suffix", self.options.tmr_suffix)

        if self.config.has_option('tmrg', 'files'):
            files = self.config.get("tmrg", "files").split()
            for file in files:
                file = file.strip()
                logging.debug("Adding file from config file %s" % file)
                self.args.append(file)

        if self.config.has_option('tmrg', 'libs'):
            files = self.config.get("tmrg", "libs").split()

            for file in files:
                if len(file) == 0:
                    continue
                file = file.strip()
                logging.debug("Adding lib file from config file : %s" % file)
                self.libFiles.append(file)

        # parse command line constrains
        ccp = CmdConstrainParser()
        self.cmdLineConstrains = {}
        for c in self.options.constrain:
            tokens = ccp.parse(c)
            name = tokens.getName()
            if name == "directive_triplicate" or name == "directive_do_not_triplicate":
                tmrVal = False
                if name == "directive_triplicate":
                    tmrVal = True
                for _id in tokens:
                    if _id.find(".") >= 0:
                        module, net = _id.split(".")
                        logging.info("Command line constrain '%s' for net '%s' in module '%s'" %
                                         (name, net, module))
                        if not module in self.cmdLineConstrains:
                            self.cmdLineConstrains[module] = {}
                        self.cmdLineConstrains[module][net] = tmrVal
                    else:
                        logging.info("Command line constrain '%s' for net '%s'" % (name, net))
                        net = _id
                        if not "global" in self.cmdLineConstrains:
                            self.cmdLineConstrains["global"] = {}
                        self.cmdLineConstrains["global"][net] = tmrVal
            elif name == "directive_default":
                tmrVal = False
                if tokens[0].lower() == "triplicate":
                    tmrVal = True
                if len(tokens[1]) > 0:
                    for module in tokens[1]:
                        logging.info("Command line constrain '%s' for module '%s' (value:%s)" %
                                         (name, module, str(tmrVal)))
                        if not module in self.cmdLineConstrains:
                            self.cmdLineConstrains[module] = {}
                        self.cmdLineConstrains[module]["default"] = tmrVal
                else:
                    if not "global" in self.cmdLineConstrains:
                        self.cmdLineConstrains["global"] = {}
                    self.cmdLineConstrains["global"]["default"] = tmrVal
            elif name == "directive_tmr_error":
                tmrErr = False
                if tokens[0].lower() == "true":
                    tmrErr = True
                if len(tokens[1]) > 0:
                    for module in tokens[1]:
                        logging.info("Command line constrain '%s' for module '%s' (value:%s)" %
                                         (name, module, str(tmrErr)))
                        if not module in self.cmdLineConstrains:
                            self.cmdLineConstrains[module] = {}
                        self.cmdLineConstrains[module]["tmr_error"] = tmrErr
                else:
                    if not "global" in self.cmdLineConstrains:
                        self.cmdLineConstrains["global"] = {}
                    self.cmdLineConstrains["global"]["default"] = tmrErr
            elif name == "directive_do_not_touch":
                for module in tokens:
                    if not module in self.cmdLineConstrains:
                        self.cmdLineConstrains[module] = {}
                    if not "constraints" in self.cmdLineConstrains[module]:
                        self.cmdLineConstrains[module]["constraints"] = {}
                    self.cmdLineConstrains[module]["constraints"]["dnt"] = True
                    logging.info("Command line constrain '%s' for module '%s')" % (name, module))

            else:
                logging.warning("Unknown constrain '%s'" % name)
        if len(self.args) == 0:
            rtl_dir = self.config.get("tmrg", "rtl_dir")
            logging.debug("No input arguments specified. All files from rtl_dir (%s) will be parsed." % rtl_dir)
            self.args = [rtl_dir]

    def __init_tripclicate_callbacks(self):
        # scan class looking for triplicator functions
        self.triplicator = {}
        for member in dir(self):
            if member.find("_TMR__triplicate_") == 0:
                token = member[len("_TMR__triplicate_"):].lower()
                self.triplicator[token] = getattr(self, member)
                logging.debug("Found triplicator for %s" % token)

    def __triplicate_comment(self, tokens):
        words = tokens.get("content").split()

        # is this tmrg vote?
        if len(words) > 2 and (words[0].lower(), words[1].lower()) == ("tmrg", "vote"):
            for i in words[2:]:
                self.voting_nets.append((False, i))

        # is this synopsys/cadence?
        if len(words) > 2 and words[0].lower() in ("synopsys", "cadence"):
            new_identifiers = []

            for original in words[2:]:
                original = original.replace('"', '')
                if original not in self.current_namespace.nets:
                    logging.warning("Synopsys directive `%s` references net `%s`, which doesn't exist. Leaving it as it is." % (tokens[0], original))
                    new_identifiers.append(original)
                    continue

                if not self.current_namespace.nets[original].triplicate:
                    new_identifiers.append(original)
                    continue

                else:
                    for i in self.EXT:
                        new_identifiers.append(original + i)

            new_tokens = ParseResults([], name="container")
            for id in new_identifiers:
                cpy = tokens.deepcopy()
                cpy["content"] = " " + " ".join([words[0], words[1], id])
                cpy[0] = "// " + cpy["content"]

                new_tokens.append(cpy)

            return new_tokens

        return tokens

    def __triplicate_functiondecl(self, tokens):
        # Function declarations won't be triplicated
        return tokens

    def __triplicate(self, tokens, i=""):
        debug = 0
        if debug:
            print(i, "_(1 in )_ >", tokens.getName(), str(tokens)[:100])
        i += "    "
        if isinstance(tokens, ParseResults):
            name = str(tokens.getName()).lower()
            offset = name.find("@")
            if offset != -1:
                name = name[0:offset]
            if len(tokens) == 0:
                return tokens
            if name in self.triplicator:
                logging.debug("Found triplicator for %s" % name)
                if debug:
                    print(i, "_(2 tmr)_>", str(tokens)[:100])
                tokens = self.triplicator[name](tokens)
            else:
                logging.debug("No triplicator for %s : %s" % (name, tokens))
                newTokens = ParseResults([], name=tokens.getName())
                if debug:
                    print(i, "_(3 lis)_ >", str(tokens)[:100])
                i += "    "
                for j in range(len(tokens)):
                    if isinstance(tokens[j], ParseResults):
                        tmrToks = self.__triplicate(tokens[j], i+"    ")
                        if isinstance(tmrToks, list):
                            for idx, otokens in enumerate(tmrToks):
                                newTokens.append(otokens)
                                if debug:
                                    print(i, "_(4 out)_>", str(otokens)[:100])
                        else:
                            newTokens.append(tmrToks)
                            tid = id(tokens[j])
                            for kname in tokens.keys():
                                if id(tokens[kname]) == tid:
                                    newTokens[kname] = tmrToks
                            if debug:
                                print(i, "_(4 out)_>", str(tmrToks)[:100])
                    else:
                        newTokens.append(tokens[j])
                        tid = id(tokens[j])
                        for kname in tokens.keys():
                            if id(tokens[kname]) == tid:
                                newTokens[kname] = tokens[j]
                        if debug:
                            print(i, "_(4 out)_>", str(tokens[j])[:100])
                if debug:
                    print(i, "_(new)_>", newTokens.getName(), str(newTokens)[:100])
                return newTokens
        else:
            # we have a string!
            if debug:
                print(i, "_(str2)_>", str(tokens[j])[:100])
        if debug:
            print(i, "_(ret)_>", str(tokens)[:100])
        return tokens

    @staticmethod
    def _append_tmr(net, letter):
        pointpos = net.find(".")

        if pointpos == -1:
            return net + letter

        return net[0:pointpos] + letter + net[pointpos:]

    def __triplicate_always(self, tokens):
        result = ParseResults([], "container")

        # check if the module needs triplication
        ids = self.getLeftRightHandSide(tokens)
        tmr = self.shouldTriplicate(ids)

        logging.debug("["+tokens.getName()+ " block]")
        logging.debug("      Left :"+" ".join(sorted(ids["left"])))
        logging.debug("      Right:"+" ".join(sorted(ids["right"])))
        logging.debug("      TMR  :"+str(tmr))

        if not tmr:
            self._addVotersIfTmr(ids["right"], addWires="output")
            return tokens

        self._addFanoutsIfTmr(ids["right"], addWires="output")

        for i in self.EXT:
            cpy = tokens.deepcopy()
            self.smartreplace(cpy, i)
            self.appendToBlockName(cpy, i)
            result.append(cpy)

        return result

    def __triplicate_initialStmt(self, tokens):
        return self.__triplicate_always(tokens)

    def __special_continuousassign(self, tokens):
        assigns = tokens.get("assignments")
        if len(assigns) != 1:
            logging.debug("More than 1 assigns")
            return False

        right = assigns[0].get("expr@rvalue")

        if len(right) != 1 or not (isinstance(right[0], str) or isinstance(right[0], ParseResults) and right[0].getName() == "reg_reference"):
            logging.debug("We have %d elements on the RHS. right[0] is of type: %s" % (len(right), str(type(right[0])) if not isinstance(right[0], ParseResults) else right[0].getName()))
            return False

        left  = self.vf.format(assigns[0].get("lvalue")[0])
        right = self.vf.format(right)

        if not len(self.voting_nets):
            return False

        if not (right, left) in self.voting_nets:
            logging.debug("(%s, %s) not among the %d voting nets" % (right, left, len(self.voting_nets)))

            if (False, left) in self.voting_nets:
                logging.debug("Detected %s as the unvoted counterpart of voted net %s" % (right, left))
                self.voting_nets.remove( (False, left) )
                self.voting_nets.append( (right, left) )
            else:
                logging.debug("(False, %s) not among the %d voting nets" % (left, len(self.voting_nets)))
                return False

        bits = " ".join([str(i) for i in self.current_namespace.nets[right].packed])
        logging.info("TMR voting %s -> %s (bits: %s)" % (right, left, bits))

        a = right+self.EXT[0]
        b = right+self.EXT[1]
        c = right+self.EXT[2]
        for ext in self.EXT:
            voter = VoterOrFanout()
            voter.a         = a
            voter.b         = b
            voter.c         = c
            voter.u         = "%s%s" % (left, ext)
            voter.error_net   = "%sTmrError%s" % (right, ext)
            voter.base_signal = self.current_namespace.nets[right]
            voter.group       = ext
            voter.add_wires   = ""

            inst_name = "%sVoter%s" % (right, ext)
            self._registerVoter(inst_name, voter)

        return True

    def __triplicate_continuousassign(self, tokens):
        # check if the module needs triplication
        ids = self.getLeftRightHandSide(tokens)
        tmr = self.shouldTriplicate(ids)

        logging.debug("[Continuous Assign block]")
        logging.debug("      Left :"+" ".join(sorted(ids["left"])))
        logging.debug("      Right:"+" ".join(sorted(ids["right"])))
        logging.debug("      TMR  :"+str(tmr))

        # Is special assignment to be translated to voted?
        special = self.__special_continuousassign(tokens)
        if special:
            return []

        if not tmr:
            logging.info("Adding voters, if needed, for signals: " + " ".join(ids["right"]))
            self._addVotersIfTmr(ids["right"], group="", addWires="output")
            return tokens

        logging.info("Adding fanouts, if needed, for signals: " + " ".join(ids["right"]))
        self._addFanoutsIfTmr(ids["right"], addWires="output")
        result = ParseResults([], "container")
        for i in self.EXT:
            cpy = tokens.deepcopy()
            self.smartreplace(cpy, i)
            result.append(cpy)

        return result

    def __triplicate_task(self, tokens):
        # Don't triplicate
        # If not present, will try to triplicate ports!
        return tokens

    def __triplicate_fields(self, tokens):
        # Don't triplicate
        # If not present, will try to triplicate members!
        return tokens

    def __triplicate_interfacedecl(self, tokens):
        if_name = tokens.get("id")[0]
        tokens["id"][0] = if_name + "TMR"

        res = []
        for outer_elem in tokens.get("elems"):
            if outer_elem.getName() == "signal":
                elem = outer_elem.get("netDecl")

                do_tmr = None
                names = []
                for reg in elem.get("identifiers"):
                    reg_name = reg.get("name")[0]
                    this_tmr = self.current_namespace.types[if_name].signals[reg_name].triplicate

                    if this_tmr is None:
                        this_tmr = self.current_namespace.types[if_name].default_tmr

                    if this_tmr is not None:
                        if do_tmr is None:
                            do_tmr = this_tmr

                        elif this_tmr != do_tmr:
                            raise ErrorMessage("Mixing signals to be triplicated and not to be triplicated in a single declaration, in interface %s. This is not supported." % reg_name)

                    names.append(reg_name)

                if do_tmr:
                    for i in self.EXT:
                        cpy = outer_elem.deepcopy()

                        for name in names:
                            self.replace(cpy, name, name + i)

                        res.append(cpy)
                else:
                    res.append(outer_elem)

            elif outer_elem.getName() == "modport":
                ports = outer_elem.get("ports")
                for ip, p in enumerate(ports):
                    signals = p.get("signals")
                    newsignals = ParseResults([], name=signals.getName())

                    for s in signals:
                        if self.current_namespace.types[if_name].signals[s[0]].triplicate or (self.current_namespace.types[if_name].signals[s[0]].triplicate is None and self.current_namespace.types[if_name].default_tmr):
                            for e in self.EXT:
                                cpy = s.deepcopy()
                                cpy[0] += e
                                newsignals.append(cpy)
                        else:
                            newsignals.append(s)

                    ports[ip]["signals"] = newsignals

                res.append(outer_elem)

        tokens["elems"] = res
        return tokens

    def __triplicate_netdecl(self, tokens):
        logging.debug("[net decl]")
        logging.debug("      left : "+str(tokens.get("identifiers")))

        # Check if it's an interface instantiation
        is_interface = False
        if "custom_type" in tokens and "".join(tokens.get("custom_type")) in self.current_namespace.types:
            _type = self.current_namespace.types["".join(tokens.get("custom_type"))]

            if isinstance(_type, Interface):
                is_interface = True

        to_tmr = []
        not_to_tmr = []

        for identifier in tokens.get("identifiers"):
            name = identifier.get("name")[0]
            if name in self.current_namespace.nets:
                if self.current_namespace.nets[name].triplicate:
                    to_tmr.append(identifier)
                else:
                    not_to_tmr.append(identifier)
            else:
                if len(name) and name[0] == '`':
                    logging.warning("Define %s" % name)
                else:
                    logging.warning("Unknown net '%s' (TMR may malfunction)" % name)

        # Triplicating an interface instantiation
        if is_interface:
            if len(to_tmr) and len(not_to_tmr):
                raise ErrorMessage("Please split interface declarations in multiple statements")

            if len(to_tmr):
                tokens["custom_type"][-1] += "TMR"
                return tokens

            return tokens

        result = ParseResults([], "container")

        # Identifiers not to be triplicated: append as they are
        for identifier in not_to_tmr:
            cpy = tokens.deepcopy()
            cpy["identifiers"] = [identifier]
            result.append(cpy)

        # Identifiers to be triplicated, append once per group
        for identifier in to_tmr:
            for i in self.EXT:
                cpy = tokens.deepcopy()
                cpy["identifiers"] = [identifier.deepcopy()]
                cpy["identifiers"][0]["name"][0] += i
                result.append(cpy)

        return result

    def __triplicate_netDeclWAssign(self, tokens):
        ids = self.getLeftRightHandSide(tokens)
        tmr = self.shouldTriplicate(ids)

        logging.debug("[net declaration with assigment]")
        logging.debug("      Left :"+" ".join(sorted(ids["left"])))
        logging.debug("      Right:"+" ".join(sorted(ids["right"])))
        logging.debug("      TMR  :"+str(tmr))

        # A simple assignment is A = B
        left = tokens.get("assignments")[0].get("lvalue")[0]
        right = tokens.get("assignments")[0].get("expr@rvalue")

        left_reg = False
        right_reg = False

        if left.getName() == "reg_reference":
            left = left.get("name")[0]
            left_reg = True

            if len(right) == 1 and isinstance(right[0], ParseResults) and right[0].getName() == "reg_reference":
                right = right[0].get("name")[0]

                right_reg = True

        # Process simple assignments to look for voters, fanins, fanouts
        if left_reg and right_reg:
            # check if this is explicit fanout
            if left[-1] in self.EXT and left[:-1] == right:
                logging.info("Removing declaration of '%s' (it comes from a fanout of %s)" % (left, right))
                return ParseResults([], name=tokens.getName())

            # check fanin
            if right[-1] in self.EXT and right[:-1] == left:
                logging.info("Removing declaration of '%s' (it was declared for fanin)" % (left))
                return ParseResults([], name=tokens.getName())

            if len(self.voting_nets) and (right, left) in self.voting_nets:
                bits = " ".join([str(i.size) for i in self.current_namespace.nets[right].packed])
                logging.info("TMR voting %s -> %s (bits: %s)" % (right, left, bits))

                a = right+self.EXT[0]
                b = right+self.EXT[1]
                c = right+self.EXT[2]
                for ext in self.EXT:
                    voter = VoterOrFanout()
                    voter.a         = a
                    voter.b         = b
                    voter.c         = c
                    voter.u         = "%s%s" % (left, ext)
                    voter.error_net   = "%sTmrError%s" % (right, ext)
                    voter.base_signal = self.current_namespace.nets[right]
                    voter.group       = ext
                    voter.add_wires   = "output"

                    inst_name = "%sVoter%s" % (right, ext)
                    self._registerVoter(inst_name, voter)

                return ParseResults([], name=tokens.getName())

        if left_reg:
            # check if this is part of explicit voter
            if left[-1] in self.EXT and left[:-1] in self.current_namespace.nets:
                logging.info("Explicit fanin '%s' part of '%s' (group %s)" % (left, left[:-1], left[-1]))
                for name in list(ids["right"]):
                    self._addVoter(name, addWires="output")
                return tokens

        # FIX ME !!!!!!!!!! quick and dirty !!!!!!
        # commented to allow access to specific tmrerror signals !
        # and "tmrError" in self.current_namespace.nets:
        if left == "tmrError":
            logging.info("Found module-wide TMR Error signal (%s). Removing its declaration here, will be added later." % left)
            return []

        if left.endswith("tmrError"):
            base_name = left[0:-8]

            if base_name in [name for (name, _) in self.current_namespace.instances]:
                logging.info("Found TMR Error signal for net `%s` (%s). Removing its declaration here, will be added later." % (base_name, left))
                return []

        if left.endswith("TmrError"):
            base_name = left[0:-8]

            if base_name in self.current_namespace.nets:
                logging.info("Found TMR Error signal for net `%s` (%s). Removing its declaration here, will be added later." % (base_name, left))
                return []


        # in any other case, triplicate right hand side
        result = ParseResults([], "container")
        if not tmr:
            logging.info("Adding voters, if needed, for signals: " + " ".join(ids["right"]))
            self._addVotersIfTmr(ids["right"], addWires="output")
            return tokens

        logging.info("Adding fanouts, if needed, for signals: " + " ".join(ids["right"]))
        self._addFanoutsIfTmr(ids["right"], addWires="output")

        for i in self.EXT:
            cpy = tokens.deepcopy()
            self.smartreplace(cpy, i)
            result.append(cpy)
        return result

    def ___triplicate_untouchable_module_instantiation(self, tokens):
        identifier = tokens.get("moduleName")[0]
        instances = tokens.get("moduleInstances")

        new_instances = {
            'non_tmr' : [],
            'A' : [],
            'B' : [],
            'C' : []
        }

        for instance in instances:
            iname = instance[0][0]

            found_instances = [inst for inst in self.current_namespace.instances if inst[0] == iname]
            itmr = found_instances[0][1].triplicate

            # If the instance is to be triplicated, triplicate it as a whole
            if itmr:

                # Instantiate it 3 times with appropriate signal names in the connectivity
                for post in self.EXT:
                    instCpy = instance.deepcopy()

                    instCpy[0][0] += post ;# Append suffix to instance name
                    for port in instCpy[1]:
                        # Can't support implicit connections
                        if len(port) == 1:
                            raise ErrorMessage("Implicit connections in module instantiation are not supported\nProblematic instance: `%s` (module: `%s`)" % (iname, identifier))

                        # Disconnected port: nothing to be done
                        if len(port[3]) == 0:
                            continue

                        # Change signal references in connectivity
                        self.smartreplace(port[3], post, check=False)

                    new_instances[post].append(instCpy)

            # If the instance is not to be triplicated
            else:
                new_instances["non_tmr"].append(instance)

            # Add voters and/or fanouts for the signals in the connectivity
            for port in instance[1]:
                if port.getName() == "comment":
                    continue

                dname = port[1]

                # Non-existent port for module
                if not dname in self.global_namespace.modules[identifier].io:
                    raise ErrorMessage("Module '%s' does not have port '%s'" % (identifier, dname))

                # Unconnected port: skip
                if len(port[3]) == 0:
                    continue

                # Ahm don't know what this is for.
                if isinstance(port[3][0], str):
                    continue

                dir = self.global_namespace.modules[identifier].io[dname].direction
                ids = self.getLeftRightHandSide(port[3])
                # Iterate over signals on the right hand side
                for sname in ids["right"]:
                    if sname not in self.current_namespace.nets:
                        logging.debug("Wire '%s' does not exist in the net database" % sname)
                        logging.debug("The connection will not be changed.")
                        continue

                    stmr = self.current_namespace.nets[sname].triplicate
                    logging.debug("      %s (%s) -> %s (tmr:%s)" % (dname, dir, sname, str(stmr)))

                    # Instance is triplicated but the signal is not.
                    if itmr and not stmr:
                        # If port is input, must fanout the non-triplicated signal to the triplicated port
                        if dir == "input":
                            self._addFanout(sname, addWires="output")

                        # If port is input, must vote the triplicated port to the non-triplicated signal
                        else:
                            self._addVoter(sname, addWires="input")

                    # Instance is not triplicated but the signal is.
                    elif not itmr and stmr:
                        # If port is input, must vote the triplicated signal to the non-triplicated port
                        if dir == "input":
                            self._addVoter(sname, addWires="output")

                        # If port is output, must fanout the non-triplicated port to the triplicated signal
                        else:
                            self._addFanout(sname, addWires="input")


        tokens_base = tokens.deepcopy()
        tokens_base["moduleInstances"] = ParseResults([], name=instances.getName())

        new_tokens = ParseResults([], name="container")
        for k in new_instances:
            if len(new_instances[k]):
                tmp = tokens_base.deepcopy()
                tmp["moduleInstances"] = new_instances[k]
                new_tokens.append(tmp)

        return new_tokens

    def ___triplicate_touchable_module_instantiation(self, tokens):
        identifier = tokens.get("moduleName")[0]
        instances = tokens.get("moduleInstances")

        identifierTMR = identifier+"TMR"

        # Update module name
        tokens["moduleName"][0] = identifierTMR

        # New instances
        new_instances = ParseResults([], name=instances.getName())

        for instance in instances:
            iname = instance[0][0]

            # Can we touch the instance?
            if "dntinst" in self.current_namespace.constraints and iname in self.current_namespace.constraints["dntinst"]:
                logging.debug("Instance '%s' has property do_not_touch." % iname)
                new_instances.append(instance)
                continue

            ports = instance[1]
            instCpy = instance.deepcopy()
            newPorts = ParseResults([], name=ports.getName())

            for port in ports:
                if port.getName() == "comment":
                    newPorts.append(port)
                    continue

                if port.getName() != "namedPortConnection":
                    raise ErrorMessage("Cannot support unnamed port connections in triplicated instance: %s." % iname)

                dport = port[1]  # skip dot
                sport = port[3]

                if dport is not None and not dport in self.global_namespace.modules[identifier].nets:
                    raise ErrorMessage("Module '%s' does not have port '%s'" % (identifier, dport))

                dportTmr = self.global_namespace.modules[identifier].nets[dport].triplicate
                logging.debug("      %s (tmr:%s) -> %s" % (dport, dportTmr, self.vf.format(sport)))

                # Port is NOT to be triplicated!
                if not dportTmr:
                    # Just copy it as it is
                    newPorts.append(port)

                # Port is to be triplicated
                elif dportTmr:
                    for post in self.EXT:
                        portCpy = port.deepcopy()
                        portCpy[1] = portCpy[1]+post
                        self.smartreplace(portCpy[3], post, check=False)
                        newPorts.append(portCpy)

                # Add voters or fanouts
                ids = self.getLeftRightHandSide(sport)
                for sname in ids['right']:
                    stmr = self.current_namespace.nets[sname].triplicate
                    dtype = self.global_namespace.modules[identifier].io[dport].direction

                    # Instance is triplicated but the signal is not.
                    if dportTmr and not stmr:
                        # If port is input, must fanout the non-triplicated signal to the triplicated port
                        if dtype == "input":
                            self._addFanout(sname, addWires="output")

                        # If port is input, must vote the triplicated port to the non-triplicated signal
                        else:
                            self._addVoter(sname, addWires="input")

                    # Instance is not triplicated but the signal is.
                    elif not dportTmr and stmr:
                        # If port is input, must vote the triplicated signal to the non-triplicated port
                        if dtype == "input":
                            self._addVoter(sname, addWires="output")

                        # If port is output, must fanout the non-triplicated port to the triplicated signal
                        else:
                            self._addFanout(sname, addWires="input")

            # TODO ADD TMR ERROR !!!!!!!!!!!!
            # "tmrError" in self.modules[identifier].nets:
            if self.global_namespace.modules[identifier].constraints["tmrErrorOut"]:
                for post in self.EXT:
                    netName = "%stmrError%s" % (iname, post)
                    logging.debug("Adding net '%s' for module '%s'" % (netName, identifier))
                    tmrErrOut = self.vp.namedPortConnection.parseString(".tmrError%s(%s)" % (post, netName))[0]
                    self._addTmrErrorWire(post, netName)
                    newPorts.append(tmrErrOut)

            instCpy[1] = newPorts
            new_instances.append(instCpy)

        tokens["moduleInstances"] = new_instances

        return tokens

    def __triplicate_ModuleInstantiation(self, tokens):
        identifier = tokens.get("moduleName")[0]
        instances = [i[0][0] for i in tokens.get("moduleInstances")]
        logging.debug("[module instances]")
        logging.debug("      Module name    :"+identifier)
        logging.debug("      Instance names :" + ", ".join(instances))

        # Do we know the module? If not, raise an error.
        if not identifier in self.global_namespace.modules:
            logging.error("")
            logging.error("      Module %s is unknown" % identifier)
            raise ErrorMessage("      Module %s is unknown" % identifier)

        # If we cannot touch the module...
        if "dnt" in self.global_namespace.modules[identifier].constraints and self.global_namespace.modules[identifier].constraints["dnt"]:
            logging.debug("      Module '%s' will not be touched (id:%s)" % (identifier, instances))

            return self.___triplicate_untouchable_module_instantiation(tokens)

        # If the instance can be touched
        else:
            return self.___triplicate_touchable_module_instantiation(tokens)

    def __triplicate_defparam(self, tokens):
        # Check if hierarchical references are triplicated
        for param in tokens.get("assignments"):
            name_split = [param[0][0].get("name")[0]]
            name_split.extend([x[0] for x in param[0][0].get("range_or_field")])

            chain = []
            namespace = self.current_namespace
            for i in range(len(name_split)-1):
                instname = name_split[i]

                # Get the actual instance
                inst = [x for x in namespace.instances if x[0] == instname]
                inst = inst[0][1]

                # Fetch the module, which will be the namespace for the next instance lookup
                namespace = self.global_namespace.modules[inst.module_name]

                # What do we need to know?
                triple_inst = "dnt" in namespace.constraints and namespace.constraints["dnt"] and inst.triplicate

                chain.append( (instname, triple_inst) )

            new_names = [name_split[-1]]
            for i in range(len(chain)-1, -1, -1):
                next_new_names = []

                for new_name_value in new_names:
                    # This instance is dnt! It will be triplicated
                    if chain[i][1]:
                        for ext in self.EXT:
                            next_new_names.append(chain[i][0] + ext + "." + new_name_value)
                    else:
                        next_new_names.append(chain[i][0] + "." + new_name_value)
                
                new_names = next_new_names

            new_defparam = tokens.deepcopy()
            new_defparam["assignments"] = []

            assignment_tpl = tokens.get("assignments")[0]
            for name in new_names:
                new_assignment = assignment_tpl.deepcopy()
                new_assignment[0][0]["name"][0] = name
                new_assignment[0][0]["range_or_field"] = []

                new_defparam["assignments"].append(new_assignment)

        return new_defparam

    def __triplicate_gateDecl(self, tokens):
        logging.warning("Found verilog gate level instantiation in module `%s`. This is generally NOT supported.", self.current_namespace.name)
        logging.warning("Please carefully review the generated code.")
        gate = tokens[0]
        instances = tokens.get("instances")
        logging.debug("[gate instances]")
        logging.debug("      gate: %s", gate)
        logging.debug("      instances: %s", ", ".join([i.get("name_and_range")[0][0] if "name_and_range" in i else "(unnamed)" for i in instances]))
        logging.debug("      Will NOT be touched")

        nets_tmr = set()
        newInstances = ParseResults([], name=instances.getName())

        for instance in instances:
            for port in instance.get("ports"):
                ids = self.getLeftRightHandSide(port)
                for rid in ids["right"]:
                    net_tmr = self.current_namespace.nets[rid].triplicate
                    nets_tmr.add(net_tmr)

            if len(nets_tmr) != 1:
                raise ErrorMessage("Gate named '%s' connects to triplicated and not triplicated wires!" % (instance))

            tmr = next(iter(nets_tmr))
            if not tmr:
                instanceCpy = instance.deepcopy()
                newInstances.append(instanceCpy)
                continue

            # Triplicate instances
            for post in self.EXT:
                instanceCpy = instance.deepcopy()

                # Change instance name
                if "name_and_range" in instanceCpy:
                    instanceCpy["name_and_range"][0] = instanceCpy["name_and_range"][0] + post

                for port in instanceCpy.get("ports"):
                    ids = self.getLeftRightHandSide(port)

                    for rid in ids["right"]:
                        net_tmr = self.current_namespace.nets[rid].triplicate
                        self.replace(port, rid, rid+post)

                newInstances.append(instanceCpy)

        tokens["instances"] = newInstances
        return tokens

    def __slice_module(self, tokens):
        result = ParseResults([], "container")
        header = tokens[1]
        moduleName = header[1]

        # generate slice
        slice = tokens.deepcopy()
        slice[1][1] = str(moduleName)+"_slice"
        wrapperWires = []
        portsToAdd = []
        portsToAddVoted = []

        if len(self.voting_nets):
            newModuleItems = ParseResults([], name=tokens[2].getName())
            for moduleItem in slice[2]:
                ids = self.getLeftRightHandSide(moduleItem)
                vote = False
                voteNet = ""
                if moduleItem.getName() == "moduleInstantiation":
                    modName = moduleItem.get("moduleName")[0]
                    if not "dnt" in self.global_namespace.modules[modName].constraints or not self.global_namespace.modules[modName].constraints["dnt"]:
                        raise ErrorMessage(
                            "Error during slicing. Module '%s' should have directive 'do_not_touch'" % modName)

                if moduleItem.getName() == "netDeclWAssign":
                    if len(ids["right"]) == 1 and len(ids["left"]) == 1:
                        for net, netVoted in self.voting_nets:
                            if netVoted in ids["left"] and net in ids["right"]:
                                vote = True
                                voteNet = net
                if not vote:
                    newModuleItems.append(moduleItem)
                else:
                    inst = voteNet+"Voter"
                    logging.info("Instantiating voter %s" % inst)
                    net = self.global_namespace.modules[moduleName].nets[voteNet]
                    type_ref = net.get_type(cache=True)
                    type_is_builtin = not net.data_type or (type_ref and type_ref.is_builtin())

                    voter = VoterOrFanout()
                    voter.u           = voteNet+"Voted"
                    voter.a           = voteNet+"A"
                    voter.b           = voteNet+"B"
                    voter.c           = voteNet+"C"
                    voter.error_net   = voteNet+"TmrError"
                    voter.add_wires   = "input"
                    voter.base_signal = net
                    self._registerVoter(inst, voter)

                    packed = " ".join([str(i) for i in net.packed])
                    unpacked = " ".join([str(i) for i in net.unpacked])

                    newModuleItems.insert(0, self.vp.standardTypePortBody.parseString("input  %s%s%s;" % (packed, voter.a, unpacked))[0])
                    newModuleItems.insert(0, self.vp.standardTypePortBody.parseString("input  %s%s%s;" % (packed, voter.b, unpacked))[0])
                    newModuleItems.insert(0, self.vp.standardTypePortBody.parseString("input  %s%s%s;" % (packed, voter.c, unpacked))[0])
                    newModuleItems.insert(0, self.vp.standardTypePortBody.parseString("output %s%s%s;" % (packed, voteNet, unpacked))[0])

                    parser_element = self.vp.customTypeNetDecl if not type_is_builtin else self.vp.standardTypeNetDecl
                    newModuleItems.insert(0, parser_element.parseString("%s %s%s%s;" % (net.data_type or "wire", packed, voter.u, unpacked))[0])
                    portsToAddVoted.append(voter.a)
                    portsToAddVoted.append(voter.b)
                    portsToAddVoted.append(voter.c)
                    portsToAdd.append(voteNet)

                    add_to_wrapperWires, add_to_newModuleItems, _ = self.__generateVoterOrFanout(True, inst, "")
                    wrapperWires.extend(add_to_wrapperWires)
                    newModuleItems.extend(add_to_newModuleItems)

            slice[2] = newModuleItems
            if (len(portsToAdd) or len(portsToAddVoted)) and "ports" not in slice[1]:
                slice[1]["ports"] = []

            for port in portsToAdd + portsToAddVoted:
                slice[1]["ports"].append(self.vp.port.parseString(port)[0])

        # generate wrapper
        wrapper = tokens.deepcopy()

        portList = []
        # triplicate module header | add tmr signals
        if "ports" in wrapper[1]:
            #print "w",wrapper
            ports = wrapper[1].get("ports")
            #print ports
            newports = ParseResults([], name=ports.getName())
            for port in ports:
                if port.getName() == "port":
                    portName = port[0].get("identifier").get("name")[0]
                    portList.append(portName)

                    if not portName in self.current_namespace.nets:
                        logging.warning("Net '%s' unknown." % portName)
                        continue

                    doTmr = self.current_namespace.nets[portName].triplicate
                    portstr = "Port %s -> " % (portName)

                    if doTmr:
                        sep = ""
                        for post in self.EXT:
                            newport = portName+post
                            portCpy = port.deepcopy()
                            portCpy[0]["identifier"]["name"][0] = newport
                            newports.append(portCpy)

                            portstr += sep+newport
                            sep = ", "
                    else:
                        portCpy = port.deepcopy()
                        newports.append(portCpy)

                    logging.debug(portstr)

                else: # if port.getName() == "portHdr"
                    ids = port.get("typed").get("identifiers") if "typed" in port else port.get("untyped").get("identifiers")
                    for id in ids:
                        portName = id.get("name")[0]
                        portList.append(portName)

                        if not portName in self.current_namespace.nets:
                            logging.warning("Net '%s' unknown." % portName)
                            continue

                        doTmr = self.current_namespace.nets[portName]["tmr"]
                        portstr = "Port %s -> " % (portName)

                        if doTmr:
                            sep = ""
                            for post in self.EXT:
                                portCpy = port.deepcopy()
                                newPortName = portName+post
                                self.replace(portCpy, portName, newPortName)
                                newports.append(portCpy)
                                portstr += sep+newPortName
                                sep = ", "
                        else:
                            newports.append(port)

                        logging.debug(portstr)

            if "tmrError" in self.current_namespace.nets:
                groups = set(self.current_namespace.voters.keys()) | set(self.current_namespace.tmrErrNets.keys())
                for group in sorted(groups):
                    newport = "tmrError%s" % group
                    newports.append(newport)
                    logging.debug("Port %s" % (newport))

            wrapper[1]["ports"] = newports

        newModuleItems = ParseResults([], name=tokens[2].getName())
        for moduleItem in wrapper[2]:
            if moduleItem.getName() == "portBody":
                newModuleItems.append(self.__triplicate(moduleItem))

        for wire in wrapperWires:
            newModuleItems.append(wire)

        for ext in self.EXT:
            instName = wrapper[1][1]+ext
            modName = slice[1][1]
            portStr = ""
            sep = ""
            for port in portList + portsToAdd:
                portStr += sep+".%s(%s%s)" % (port, port, ext)
                sep = ","
            for port in portsToAddVoted:
                portStr += sep+".%s(%s)" % (port, port)
            newModuleItems.append(self.vp.moduleInstantiation.parseString(
                "%s %s(%s);" % (modName, instName, portStr))[0])

        wrapper[1][1] = str(moduleName)+"TMR"
        wrapper[2] = newModuleItems

        result.append(slice)
        result.append(wrapper)
        return result

    def __generateVoterOrFanout(self, voter_not_fanout, name, group=""):
        item = None
        if voter_not_fanout:
            item = self.current_namespace.voters[group][name]
            logging.info("Instantiating voter %s" % name)
        else:
            item = self.current_namespace.fanouts[name]
            logging.info("Instantiating fanout %s" % name)

        resPre, resPost, resErr = ([], [], [])

        _range = " ".join([str(i) for i in item.base_signal.packed])
        if _range:
            _range += " "

        new_type = item.base_signal.copy()
        new_type.name = item.u
        new_type.net_type = "wire"
        _type = new_type.full_type()

        type_ref = new_type.get_type(cache=True)
        type_is_builtin = not new_type.data_type or (type_ref and type_ref.is_builtin())
        voter_size = new_type.voter_size(cache=True)

        width = "#(.WIDTH(%s)) " % voter_size if voter_size != 1 else ""

        _arange = " ".join([str(i) for i in item.base_signal.unpacked])
        if _arange:
            _arange = " "+_arange

        parser_element = self.vp.customTypeNetDecl if not type_is_builtin else self.vp.standardTypeNetDecl

        if voter_not_fanout:
            if item.add_wires == "output":
                logging.debug("Adding output %s %s%s%s;" % (_type, _range, item.u, _arange))
                resPre.append(parser_element.parseString("%s %s%s%s;" % (_type, _range, item.u, _arange))[0])

            elif item.add_wires == "input":
                logging.debug("Adding input %s %s, %s, %s" % (_type, item.a, item.b, item.c))
                resPre.append(parser_element.parseString("%s %s%s%s;" % (_type, _range, item.a, _arange))[0])
                resPre.append(parser_element.parseString("%s %s%s%s;" % (_type, _range, item.b, _arange))[0])
                resPre.append(parser_element.parseString("%s %s%s%s;" % (_type, _range, item.c, _arange))[0])

            cell = "majorityVoter" + self.options.common_cells_postfix
            if "majority_voter_cell" in self.current_namespace.constraints:
                cell = self.current_namespace.constraints["majority_voter_cell"]

            if "tmrError" in self.current_namespace.nets:
                resErr.append(item.error_net)
            else:
                resPre.append(self.vp.standardTypeNetDecl.parseString("wor %s;" % item.error_net)[0])

        else:
            if item.add_wires == "output":
                logging.debug("Adding output %s %s, %s , %s" % (_type, item.a, item.b, item.c))
                resPre.append(parser_element.parseString("%s %s %s %s;" % (_type, _range, item.a, _arange))[0])
                resPre.append(parser_element.parseString("%s %s %s %s;" % (_type, _range, item.b, _arange))[0])
                resPre.append(parser_element.parseString("%s %s %s %s;" % (_type, _range, item.c, _arange))[0])

            elif item.add_wires == "input":
                logging.debug("Adding input %s %s" % (_type, item.u))
                resPre.append(parser_element.parseString("%s %s %s %s;" % (_type, _range, item.u, _arange))[0])

            cell = "fanout" + self.options.common_cells_postfix
            if "fanout_cell" in self.current_namespace.constraints:
                cell = self.current_namespace.constraints["fanout_cell"]

        index_str = ""
        genstr = ""
        indent = ""
        # Generate the for loops if this is an unpacked array
        if item.base_signal.unpacked:
            genstr = "generate\n"

            for i, unpacked_range in enumerate(item.base_signal.unpacked):
                varname = "gen_%s%d" % (name, i)
                genvar = "genvar %s;\n" % varname
                index_str += "["+varname+"]"

                resPost.append(self.vp.genVarDecl.parseString(genvar)[0])

                try:
                    left_lte_right = unpacked_range.left <= unpacked_range.right
                except TypeError:
                    _start = "((%s>%s) ? %s : %s )" % (unpacked_range.left, unpacked_range.right, unpacked_range.right, unpacked_range.left)
                    _stop  = "((%s>%s) ? %s : %s )" % (unpacked_range.left, unpacked_range.right, unpacked_range.left, unpacked_range.right)
                else:
                    _start = unpacked_range.left if left_lte_right else unpacked_range.right
                    _stop  = unpacked_range.right if left_lte_right else unpacked_range.left

                _for   = "%sfor(%s=%s; %s <= %s; %s=%s+1) begin : %s_fanout\n" % (indent, varname, _start, varname, _stop, varname, varname, varname)

                genstr += _for + "\n"
                indent += "  "

        # Generate the voter/fanout itself
        _type_is_enum = isinstance(type_ref, Enumeration)
        out_start, out_end = ("{", "}") if _type_is_enum else ("", "")
        genstr += indent + cell + " %s%s (\n" % (width, name)
        if voter_not_fanout:
            genstr += indent + "  .inA(%s%s),\n" % (item.a, index_str)
            genstr += indent + "  .inB(%s%s),\n" % (item.b, index_str)
            genstr += indent + "  .inC(%s%s),\n" % (item.c, index_str)
            genstr += indent + "  .out(%s%s%s%s),\n" % (out_start, item.u, index_str, out_end)
            genstr += indent + "  .tmrErr(%s)\n" % (item.error_net)
        else:
            genstr += indent + "  .in(%s%s),\n"   % (item.u, index_str)
            genstr += indent + "  .outA(%s%s%s%s),\n" % (out_start, item.a, index_str, out_end)
            genstr += indent + "  .outB(%s%s%s%s),\n" % (out_start, item.b, index_str, out_end)
            genstr += indent + "  .outC(%s%s%s%s)\n"  % (out_start, item.c, index_str, out_end)
        genstr += indent + ");\n"

        # Generate the for loop endings if this is an unpacked array
        if item.base_signal.unpacked:
            for i in range(len(item.base_signal.unpacked)):
                indent = indent[:-2]
                genstr += "%send\n" % indent

            genstr += "endgenerate"

        # Select the parser
        parser = self.vp.generate if item.base_signal.unpacked else self.vp.moduleInstantiation
        resPost.append(parser.parseString(genstr)[0])

        return resPre, resPost, resErr

    def __triplicate_module(self, tokens):
        header = tokens[1]

        moduleName = header[1]
        self.current_namespace = self.global_namespace.modules[moduleName]
        if "dnt" in self.global_namespace.modules[moduleName].constraints and self.global_namespace.modules[moduleName].constraints["dnt"]:
            logging.info("Module '%s' is not to be touched" % moduleName)
            return tokens

        if "slicing" in self.global_namespace.modules[moduleName].constraints:
            logging.info("Module '%s' is to be sliced" % moduleName)
            return self.__slice_module(tokens)

        header[1] = str(moduleName)+"TMR"
        logging.debug("")
        logging.debug("= "*50)
        logging.debug("Module %s -> %s" % (moduleName, header[1]))
        logging.debug("= "*50)

        logging.debug("- module body "+"- "*43)

        moduleBody = tokens[2]
        moduleBody = self.__triplicate(moduleBody)
        tokens[2] = moduleBody

        logging.debug("- module header "+"- "*42)

        # triplicate module header | add tmr signals
        newports = ParseResults([], name="")
        if "ports" in header:
            ports = header.get("ports")
            for port in ports:
                newports.append(self.__triplicate(port))

        if self.current_namespace.constraints["tmrErrorOut"]:
            groups = set(self.current_namespace.voters.keys()) | set(self.current_namespace.tmrErrNets.keys())
            for group in sorted(groups):
                newport_name = "tmrError%s" % group
                newport = None
                if self.current_namespace.portMode == "ANSI":
                    newport = self.vp.standardTypePortHdr.parseString("output %s" % newport_name)[0]
                else:
                    newport = self.vp.port.parseString(newport_name)[0]

                newports.append(newport)
                logging.debug("Port %s" % (newport))

        if len(newports):
            header["ports"] = newports

        logging.debug("- voters & fanouts  "+"- "*40)
        groups = set(self.current_namespace.voters.keys()) | set(self.current_namespace.tmrErrNets.keys())
        for group in sorted(groups):
            errSignals = set()
            if group in sorted(self.current_namespace.voters):
                for voter_name in sorted(self.current_namespace.voters[group]):
                    resPre, resPost, resErr = self.__generateVoterOrFanout(True, voter_name, group)

                    for k in resPre:
                        loc = 0
                        if k.getName() == "netDecl" and "custom_type" in k:
                            _type = "".join(k.get("custom_type"))
                            for i,t in enumerate(moduleBody):
                                if t.getName() in ["structDecl", "enumDecl", "typedefDecl", "unionDecl"] and t.get("id")[0] == _type:
                                    loc = i+1

                        moduleBody.insert(loc, k)

                    for k in resPost:
                        moduleBody.append(k)

                    for k in resErr:
                        errSignals.add(k)

            if group in sorted(self.current_namespace.tmrErrNets):
                errSignals = errSignals | self.current_namespace.tmrErrNets[group]

            # add wires for all error signals
            if len(errSignals):
                for signal in sorted(errSignals):
                    moduleBody.insert(0, self.vp.standardTypeNetDecl.parseString("wor %s;" % signal)[0])

            # after all voters are added, we can create an "OR" of all them
            if "tmrError" in self.current_namespace.nets:
                if self.current_namespace.constraints["tmrErrorOut"]:
                    if self.current_namespace.portMode == "non-ANSI":
                        moduleBody.insert(0, self.vp.standardTypePortBody.parseString("output tmrError%s;" % group)[0])
                else:
                    if not self.current_namespace.constraints["tmrErrorOut"]:
                        moduleBody.insert(0, self.vp.standardTypeNetDecl.parseString("wire tmrError%s;" % group)[0])
                        logging.debug("Adding wire tmrError%s;" % group)

                sep = ""
                asgnStr = "assign tmrError%s=" % group

                if len(errSignals):
                    for signal in sorted(errSignals):
                        signalRaw = signal[:-len("TmrError"+group)]

                        if "tmr_error_exclude" in self.current_namespace.constraints and signalRaw in self.current_namespace.constraints["tmr_error_exclude"]:
                            logging.debug("Removing signal '%s' from tmrError", signal)
                            continue

                        asgnStr += sep+signal
                        sep = "|"
                else:
                    asgnStr += "1'b0"
                asgnStr += ";"
                moduleBody.append(self.vp.continuousAssign.parseString(asgnStr)[0])

        for fanout_name in sorted(self.current_namespace.fanouts):
            resPre, resPost, resErr = self.__generateVoterOrFanout(False, fanout_name)

            for k in resPre:
                moduleBody.insert(0, k)

            for k in resPost:
                moduleBody.append(k)

        # detect if user created constrains which could generate invalid code
        voter_outputs = []
        for group in sorted(groups):
            if group in sorted(self.current_namespace.voters):
                for voter in sorted(self.current_namespace.voters[group]):
                    voter_outputs.append(self.current_namespace.voters[group][voter].u)

        for fanout in sorted(self.current_namespace.fanouts):
            _in = self.current_namespace.fanouts[fanout].u
            if _in in voter_outputs:
                logging.warning("Signal '%s' is connected to fanout input and voter output." % (_in))
                logging.warning("Probably the resulting code does not reflect the design intent.")
                logging.warning("Please consider upgrading tmrg directives.")

        paramPos = 0
        for i, item in enumerate(moduleBody):
            try:
                token_type = item.getName()
            except Exception as e:
                print(e)
                print(item)
            else:
                if token_type not in ("paramDecl", "package_import", "structDecl", "enumDecl", "typedefDecl", "unionDecl", "timeunitsDecl"):
                    continue

                logging.debug("Moving %s to the front '%s'" % (token_type, str(item)))
                moduleBody.insert(paramPos, item)
                paramPos += 1
                del moduleBody[i+1]

        self.current_namespace = self.global_namespace
        return [tokens]

    def __triplicate_portbody(self, tokens):
        key = "typed" if "typed" in tokens else "untyped"

        ids = tokens.get(key).get("identifiers")

        before = str(ids)

        newtokens = ParseResults([], name=ids.getName())
        for i in ids:
            name = i.get("name")[0]
            if name not in self.current_namespace.nets or not self.current_namespace.nets[name].triplicate:
                nt = i.deepcopy()
                newtokens.append(nt)
                continue

            if isinstance(self.current_namespace.nets[name], Interface):
                raise ErrorMessage("Interfaces are not currently supported.")

            for e in self.EXT:
                cpy = i.deepcopy()
                self.smartreplace(cpy, e)
                newtokens.append(cpy)

        tokens[key]["identifiers"] = newtokens

        after = str(tokens.get(key).get("identifiers"))
        logging.debug("%s %s -> %s" % (tokens.get("dir"), before, after))
        return tokens

    def __triplicate_porthdr(self, tokens):
        return self.__triplicate_portbody(tokens)

    def __triplicate_portnotansi(self, tokens):
        newtokens = ParseResults([], "container")
        name = tokens.get("identifier").get("name")[0]
        if name not in self.current_namespace.nets or not self.current_namespace.nets[name].triplicate:
            logging.debug("(not-ANSI) %s -> %s" % (tokens , tokens))
            return tokens

        for e in self.EXT:
            cpy = tokens.deepcopy()
            self.smartreplace(cpy, e)
            newtokens.append(cpy)

        logging.debug("(not-ANSI) %s -> %s" % (tokens, newtokens))
        return newtokens

    def checkIfContains(self, tokens, label):
        def _check(tokens, label):
            if isinstance(tokens, ParseResults):
                for tok in tokens:
                    res = _check(tok, label)
                    if res:
                        return True
                return False
            else:
                return label == tokens
        return _check(tokens, label)

    def replace(self, tokens, _from, _to):
        def _replace(tokens, _from, _to):
            if isinstance(tokens, ParseResults):
                for k, v in tokens.items():
                    if not isinstance(v, ParseResults):
                        #check_and_replace(_from, tokens, k, _to)
                        continue

                    if v.getName() == "name":
                        if _from == v[0]:
                            tokens[k][0] = _to
                    else:
                        _replace(v, _from, _to)

        return _replace(tokens, _from, _to)

    def smartreplace(self, tokens, ext, done=None, check=True):
        # Not a ParseResults, probably text, just ignore
        if not isinstance(tokens, ParseResults):
            return

        if done is None:
            done = []

        keys = list(tokens.keys())
        keys.extend(list(range(len(tokens))))
        for k in keys:
            v = tokens[k]

            if id(tokens[k]) in done:
                continue
            else:
                done.append(id(tokens[k]))

            # Not a ParseResults, probably text, just ignore
            if not isinstance(v, ParseResults):
                continue

            # Only replace "name" results. Otherwise, probe.
            if v.getName() != "name":
                self.smartreplace(v, ext, done, check)
                continue

            netname, netfield = self.split_name(v[0])
            #print("DEADBEEF %s (%s) is %s -> %s" % (netname, v[0], _from, _to))

            # Net not known. Ignore.
            if netname not in self.current_namespace.nets:
                continue

            # Net not to be triplicated. Ignore.
            #if check and not self.current_namespace.nets[netname]["tmr"]:
            #    continue
            # ... actually we have fanouts, and should triplicate these names as well!

            # If accessing one of the copies of a triplicated net, just ignore
            if netname[-1] in self.EXT and netname[0:-1] in self.current_namespace.nets:
                continue

            # If a struct, treat it accordingly.
            _type = self.current_namespace.nets[netname].data_type
            if _type in self.current_namespace.types and (isinstance(self.current_namespace.types[_type], Structure) or isinstance(self.current_namespace.types[_type], Union)):
                if netfield:
                    tokens[k][0] = netname + ext + "." + netfield
                else:
                    tokens[k][0] = netname + ext

                continue

            # Looks like a struct, but it's not. Throw error.
            elif netfield:
                raise ValueError("Trying to access field %s of %s, which is neither a structure nor a union." % (netfield, netname))

            # Not a struct field reference.
            tokens[k][0] = netname + ext

    def appendToBlockName(self, tokens, postfix, done=None):
        # Not a ParseResults, probably text, just ignore
        if not isinstance(tokens, ParseResults):
            return

        if done is None:
            done = []

        keys = list(tokens.keys())
        keys.extend(list(range(len(tokens))))
        for k in keys:
            v = tokens[k]

            if id(tokens[k]) in done:
                continue
            else:
                done.append(id(tokens[k]))

            # Not a ParseResults, probably text, just ignore
            if not isinstance(v, ParseResults):
                continue

            if tokens[k].getName() == "id@blockName":
                tokens[k][0] += postfix
            else:
                self.appendToBlockName(tokens[k], postfix, done)

    def replaceDot(self, tokens, post):
        def _replace(tokens, post):
            if isinstance(tokens, ParseResults):
                for i in range(len(tokens)):
                    if isinstance(tokens[i], ParseResults):
                        _replace(tokens[i], post)
                    else:
                        if tokens[i][0] == '.':
                            tokens[i] += post

        return _replace(tokens, post)

    def shouldTriplicate(self, ids):
        leftTMR = False
        leftNoTMR = False
        logging.debug("SHOULDTRIPLICATE - Will check nets: %s" % (ids["left"]))
        for net in ids["left"]:
            # Check if struct
            netname, netfield = self.split_name(net)

            if netname not in self.current_namespace.nets:
                if netname[0] == '`':
                    logging.debug("SHOULDTRIPLICATE - Found macro %s. Ignoring." % net)

                elif net in self.current_namespace.params:
                    logging.debug("SHOULDTRIPLICATE - Found parameter %s. Ignoring." % net)

                elif net in self.current_namespace.genvars:
                    logging.debug("SHOULDTRIPLICATE - Found genvar %s. Ignoring." % net)

                elif net in self.current_namespace.local_variables:
                    logging.debug("SHOULDTRIPLICATE - Found local variable %s. Ignoring." % net)

                elif self._is_enum_element(net):
                    logging.debug("SHOULDTRIPLICATE - Found enum element %s. Ignoring." % net)

                elif netname in [name for (name, _) in self.current_namespace.instances]:
                    logging.debug("SHOULDTRIPLICATE - Found instance name %s. Assuming %s is a hierarchical reference. Ignoring." % (netname, net))

                else:
                    logging.warning("%s cannot be recognized as neither macro, parameter, enumeration element, nor hierarchical reference. Leaving it untouched." % net)

            else:
                if self.current_namespace.nets[netname].triplicate:
                    logging.debug("SHOULDTRIPLICATE - Found net %s. Triplicate: %s" % (netname, "True"))
                    leftTMR = True

                if not self.current_namespace.nets[netname].triplicate:
                    logging.debug("SHOULDTRIPLICATE - Found net %s. Triplicate: %s" % (netname, "False"))
                    leftNoTMR = True

                if not leftTMR and not leftNoTMR:
                    logging.debug("SHOULDTRIPLICATE - Found net %s. Triplicate: %s" % (netname, "Default"))

        if leftTMR and leftNoTMR:
            logging.error("Block contains both type of elements (should and should not be triplicated!) in one expression.")
            logging.error("This request will not be properly processed!")
            logging.error("Elements: %s" % (" ".join(sorted(ids["left"]))))
            return False

        return leftTMR

    def _registerVoter(self, name, voter):
        if not voter.group in self.current_namespace.voters:
            self.current_namespace.voters[voter.group] = {}
            logging.info("Creating TMR error group %s" % voter.group)

        if not name in self.current_namespace.voters[voter.group]:
            logging.debug("Adding voter '%s' to group '%s' (extended)" % (name, voter.group))
            logging.debug("    %s %s %s -> %s & %s" % (voter.a, voter.b, voter.c, voter.u, voter.error_net))

            self.current_namespace.voters[voter.group][name] = voter
            self.__voterPresent = True

    def _addVoter(self, net_name, group="", addWires=""):
        voter = VoterOrFanout()
        voter.a = net_name+self.EXT[0]
        voter.b = net_name+self.EXT[1]
        voter.c = net_name+self.EXT[2]
        voter.u = "%s" % (net_name)
        voter.error_net = "%sTmrError" % (net_name)
        voter.base_signal = self.current_namespace.nets[net_name]
        voter.group = group
        voter.add_wires = addWires

        inst_name = "%sVoter" % (net_name)
        self._registerVoter(inst_name, voter)

    def _addVotersIfTmr(self, idList, group="", addWires="output"):
        for netId in idList:
            netname, _ = self.split_name(netId)
            if self.current_namespace.nets[netname].triplicate and not self.current_namespace.nets[netname].dont_touch:
                logging.debug("Net %s needs a Voter!" % netname)
                self._addVoter(netname, group, addWires)
            else:
                logging.debug("Net %s doesn't need a Voter." % netname)

    def _addFanout(self, netID, addWires=""):
        inst = netID+"Fanout"
        if not netID in self.current_namespace.nets:
            logging.warning("Net %s unknown in addFanout!" % netID)
            return

        if not inst in self.current_namespace.fanouts:
            fanout = VoterOrFanout()
            fanout.u = netID
            fanout.a = netID+self.EXT[0]
            fanout.b = netID+self.EXT[1]
            fanout.c = netID+self.EXT[2]
            fanout.base_signal = self.current_namespace.nets[netID]
            fanout.add_wires = addWires

            self.current_namespace.fanouts[inst] = fanout
            self.__fanoutPresent = True

            logging.debug("Adding fanout %s" % inst)
            logging.debug("    %s -> %s %s %s" % (fanout.u, fanout.a, fanout.b, fanout.c))

    def _addFanoutsIfTmr(self, idList, addWires=""):
        for netId in idList:
            netname, _ = self.split_name(netId)
            if not self.current_namespace.nets[netname].triplicate and not self.current_namespace.nets[netname].dont_touch:
                logging.debug("Net %s needs a Fanout!" % netname)
                self._addFanout(netname, addWires=addWires)
            else:
                logging.debug("Net %s doesn't need a Fanout." % netname)

    def _addTmrErrorWire(self, post, netName):
        if not post in self.current_namespace.tmrErrNets:
            self.current_namespace.tmrErrNets[post] = set()
        self.current_namespace.tmrErrNets[post].add(netName)

    def exc(self):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logging.error("")
        logging.error("TMR exception:")
        for l in traceback.format_exception(exc_type, exc_value,
                                            exc_traceback):
            for ll in l.split("\n"):
                logging.error(ll)
        logging.error(ll)

    def __getLenStr(self, toks):
        rangeLen = "1"
        if len(toks) < 2:
            return rangeLen
        left = toks[-2]
        right = toks[-1]
        rangeLen = "%s - %s + 1" % (self.vf.format(left), self.vf.format(right))
        try:
            rangeInt = eval(rangeLen)
            rangeLen = "%d" % rangeInt
        except:
            pass
        return rangeLen

    def elaborate(self, allowMissingModules=False):
        """ Elaborate the design
        :return:
        """
        # allowMissingModules = False # FIXME shoudl not be here like that (added for wrappers)
        VerilogElaborator.elaborate(self, allowMissingModules=allowMissingModules)
        # apply constrains
        logging.info("")
        logging.info("Applying constrains")
        for module in sorted(self.global_namespace.modules):
            logging.info("Module %s" % module)

            # tmr error output
            # global settings
            tmrErrOut = self.config.getboolean("global", "tmr_error")
            s = "configGlobal:%s" % (str(tmrErrOut))
            # from source code
            if "tmr_error" in self.global_namespace.modules[module].constraints:
                tmrErrOut = self.global_namespace.modules[module].constraints["tmr_error"]
                s += " -> srcModule:%s" % (str(tmrErrOut))
            # from module configuration
            if self.config.has_section(module) and self.config.has_option(module, "tmr_error"):
                tmrErrOut = self.config.getboolean(module, "tmr_error")
                s += " -> configModule:%s" % (str(tmrErrOut))
            # from command line arguments
            if module in self.cmdLineConstrains and "tmr_error" in self.cmdLineConstrains[module]:
                tmrErrOut = self.cmdLineConstrains[module]["tmr_error"]
                s += " -> cmdModule:%s" % (str(tmrErrOut))

            logging.info(" | tmrErrOut : %s (%s)" % (str(tmrErrOut), s))
            if tmrErrOut:
                err = Signal()
                err.triplicate = True
                self.global_namespace.modules[module].nets["tmrError"] = err
            self.global_namespace.modules[module].constraints["tmrErrorOut"] = tmrErrOut

            s = "false"
            do_not_touch = False
            if self.global_namespace.modules[module].constraints["dnt"]:
                do_not_touch = self.global_namespace.modules[module].constraints["dnt"]
                s += " -> srcModule:%s" % (str(do_not_touch))

            if self.config.has_section(module) and self.config.has_option(module, "do_not_touch"):
                do_not_touch = self.config.getboolean(module, "do_not_touch")
                s += " -> configModule:%s" % (str(do_not_touch))
            if module in self.cmdLineConstrains and "constraints" in self.cmdLineConstrains[module] and "dnt" in self.cmdLineConstrains[module]["constraints"]:
                do_not_touch = True
                s += " -> cmdModule:%s" % (str(do_not_touch))
            self.global_namespace.modules[module].constraints["dnt"] = do_not_touch
            logging.info(" | do_not_touch : %s (%s)" % (str(tmrErrOut), s))

            for net in self.global_namespace.modules[module].nets:
                tmr = False

                # default from global configuration
                globalTmr = self.config.get("global", "default")
                if globalTmr.lower() == "triplicate":
                    tmr = True

                s = "configGlobalDefault:%s" % (str(tmr))
                # default from source code
                if "default" in self.global_namespace.modules[module].constraints:
                    tmr = self.global_namespace.modules[module].constraints["default"]
                    s += " -> srcModuleDefault:%s" % (str(tmr))

                # default from module configuration
                if self.config.has_section(module) and self.config.has_option(module, "default"):
                    modDefault = self.config.get(module, "default")
                    if modDefault.lower() == "triplicate":
                        tmr = True
                    else:
                        tmr = False
                    s += " -> configModuleDefault:%s" % (str(tmr))

                # default from command line arguments
                if module in self.cmdLineConstrains and "default" in self.cmdLineConstrains[module]:
                    tmr = self.cmdLineConstrains[module]["default"]
                    s += " -> cmdModuleDefault:%s" % (str(tmr))

                # net specific from source code
                if net in self.global_namespace.modules[module].constraints:
                    tmr = self.global_namespace.modules[module].constraints[net]
                    s += " -> src:%s" % (str(tmr))

                # net specific from configuration
                if self.config.has_section(module) and self.config.has_option(module, net):
                    conf = self.config.get(module, net)
                    if conf.lower() == "triplicate":
                        tmr = True
                    else:
                        tmr = False
                    s += " -> config:%s" % (str(tmr))

                # net specific from command line
                if module in self.cmdLineConstrains and net in self.cmdLineConstrains[module]:
                    tmr = self.cmdLineConstrains[module][net]
                    s += " -> cmd:%s" % (str(tmr))

                if "slicing" in self.global_namespace.modules[module].constraints:
                    tmr = True
                    s += " -> slicing:%s" % (str(tmr))

                if "dnt" in self.global_namespace.modules[module].constraints and self.global_namespace.modules[module].constraints["dnt"]:
                    tmr = False
                    s += " -> module do_not_touch:%s" % (str(tmr))

                if self.global_namespace.modules[module].nets[net].dont_touch:
                    tmr = False
                    s += " -> net do_not_touch:%s" % (str(tmr))

                logging.info(" | module %s net %s : %s (%s)" % (module, net, str(tmr), s))
                self.global_namespace.modules[module].nets[net].triplicate = tmr

            for (instName, inst) in self.global_namespace.modules[module].instances:
                tmr = False
                # default from global configuration
                globalTmr = self.config.get("global", "default")
                if globalTmr.lower() == "triplicate":
                    tmr = True
                s = "configGlobalDefault:%s" % (str(tmr))
                # default from module configuration
                if self.config.has_section(module) and self.config.has_option(module, "default"):
                    modDefault = self.config.get(module, "default")
                    if modDefault.lower() == "triplicate":
                        tmr = True
                    else:
                        tmr = False
                    s += " -> configModuleDefault:%s" % (str(tmr))
                # default from source code
                if "default" in self.global_namespace.modules[module].constraints:
                    tmr = self.global_namespace.modules[module].constraints["default"]
                    s += " -> srcModuleDefault:%s" % (str(tmr))

                # default from command line arguments
                if module in self.cmdLineConstrains and "default" in self.cmdLineConstrains[module]:
                    tmr = self.cmdLineConstrains[module]["default"]
                    s += " -> cmdModuleDefault:%s" % (str(tmr))
                # instName specific from configuration
                if self.config.has_section(module) and self.config.has_option(module, instName):
                    conf = self.config.get(module, instName)
                    if conf.lower() == "triplicate":
                        tmr = True
                    else:
                        tmr = False
                    s += " -> config:%s" % (str(tmr))
                # instance specific from source code
                if instName in self.global_namespace.modules[module].constraints:
                    tmr = self.global_namespace.modules[module].constraints[instName]
                    s += " -> src:%s" % (str(tmr))
                # instance specific from command line
                if module in self.cmdLineConstrains and instName in self.cmdLineConstrains[module]:
                    tmr = self.cmdLineConstrains[module][instName]
                    s += " -> cmd:%s" % (str(tmr))

                logging.info(" | instName %s : %s (%s)" % (instName, str(tmr), s))
                inst.triplicate = tmr

        # apply special constrains by name conventions
        logging.info("")
        logging.info("Applying constrains by name")
        self.voting_nets = []
        for module in sorted(self.global_namespace.modules):
            logging.info("Module %s" % module)
            for net in self.global_namespace.modules[module].nets:
                # Look for voting nets...
                netVoted = net+"Voted"
                netUnvoted = net+"Unvoted"

                if netVoted in self.global_namespace.modules[module].nets:
                    netUnvoted = net
                elif netUnvoted in self.global_namespace.modules[module].nets:
                    netVoted = net
                else:
                    continue

                self.voting_nets.append((netUnvoted, netVoted))
                logging.info("Full voting detected for nets %s -> %s" % (netUnvoted, netVoted))
                if not self.global_namespace.modules[module].nets[netUnvoted].triplicate or not self.global_namespace.modules[module].nets[netVoted].triplicate:
                    logging.warning("Nets for full voting should be triplicated! (%s:%s, %s:%s)" % (
                        netUnvoted, self.global_namespace.modules[module].nets[netUnvoted].triplicate, netVoted, self.global_namespace.modules[module].nets[netVoted].triplicate))

        if len(self.voting_nets):
            logging.info("Voting present (%d nets)" % (len(self.voting_nets)))

    def _addCommonModules(self, fname, voter=False, fanout=False):
        if not self.__fanoutPresent and not self.__voterPresent:
            return
        if not self.config.getboolean("tmrg", "add_common_definitions"):
            return
        if not self.options.common_definitions:
            return
        logging.info("Declarations of voters and fanouts are being added to %s" % fname)
        f = open(fname, "a")

        if self.__voterPresent:
            vfile = os.path.join(self.scriptDir,  self.config.get("tmrg", "voter_definition"))
            logging.info("Taking voter declaration from %s" % vfile)
            f.write("\n\n// %s\n" % vfile)
            fileContent = readFile(vfile)
            fileContent = fileContent.replace("majorityVoter", "majorityVoter"+self.options.common_cells_postfix)
            f.write(fileContent)

        if self.__fanoutPresent:
            ffile = os.path.join(self.scriptDir,  self.config.get("tmrg", "fanout_definition"))
            logging.info("Taking fanout declaration from %s" % ffile)
            f.write("\n\n// %s\n" % ffile)
            fileContent = readFile(ffile)
            fileContent = fileContent.replace("fanout", "fanout"+self.options.common_cells_postfix)
            f.write(fileContent)
        f.close()

    def getHeader(self, fname, fout):
        HLEN = 100
        header = "/"+("*"*HLEN)+"\n"

        def getRevInfo(fname):
            git_version, errors = runCommand('git rev-parse HEAD')
            if git_version:
                oStr = "Git SHA           : %s" % git_version.rstrip().decode("utf-8")
                git_status, errors = runCommand('git status --short %s'%fname)
                git_status = git_status.rstrip().decode("utf-8")
                if git_status:
                    oStr += " (%s)\n" % git_status
                else:
                    oStr += "\n"
            else:
                oStr = "Git SHA           : File not in git repository!\n"

            t = os.path.getmtime(fname)
            oStr += "Modification time : %s\n" % datetime.datetime.fromtimestamp(t)
            oStr += "File Size         : %s\n" % os.path.getsize(fname)
            oStr += "MD5 hash          : %s" % hashlib.md5(open(fname, 'rb').read()).hexdigest()

            return oStr

        def addLine(header, s, align="left"):
            if align == "left":
                line = s+" "*(HLEN-4-len(s))
                header += " * %s *\n" % line
            if align == "center":
                slen = HLEN-4-len(s)
                sleft = int(slen/2)
                sright = slen-sleft
                line = (" "*sleft) + s + (" "*sright)
                header += " * %s *\n" % line
            return header

        def getTmrgRev():
            ver = tmrg_version()
            if ver == "trunk":
                out, err = runCommand("svnversion %s" % sys.argv[0])
                ver += " (svnversion: %s)" % out.rstrip()
            return ver
        header = addLine(header, "! THIS FILE WAS AUTO-GENERATED BY TMRG TOOL !", align="center")
        header = addLine(header, "! DO NOT EDIT IT MANUALLY !", align="center")
        header = addLine(header, "")
        header = addLine(header, "file    : %s" % (fout))
        header = addLine(header, "")
        header = addLine(header, "user    : %s" % (getpass.getuser()))
        header = addLine(header, "host    : %s" % (socket.gethostname()))
        header = addLine(header, "date    : %s" % (time.strftime("%d/%m/%Y %H:%M:%S")))
        header = addLine(header, "")
        header = addLine(header, "workdir : %s" % os.getcwd())
        for l in textwrap.wrap("cmd     : %s" % (" ".join(sys.argv)), 100, subsequent_indent="          "):
            header = addLine(header, l)

        header = addLine(header, "tmrg rev: %s" % (getTmrgRev()))

        header = addLine(header, "")
        header = addLine(header, "src file: %s" % fname)
        for l in getRevInfo(fname).split("\n"):
            header = addLine(header, "          "+l)
        header = addLine(header, "")
        header += " "+("*"*HLEN)+"/\n\n"
        return header

    def triplicate(self):
        """ Triplicate the design
        :return:
        """
        tmrSuffix = "TMR"
        spaces = self.config.getint("tmrg", "spaces")

        logging.debug("")
        logging.info("Triplication starts here")
        tmr_start_time = time.time()
        self.tmrLinesTotal = 0
        self.statsLogs = []
        if not os.path.isdir(self.config.get("tmrg", "tmr_dir")):
            raise ErrorMessage("Specified output directory does not exists (%s)" % self.config.get("tmrg", "tmr_dir"))

        for file in self.files:
            logging.info("")
            logging.debug("#"*100)
            logging.info("Triplicating file %s" % (file.filename))
            logging.debug("#"*100)
            tmrTokens = self.__triplicate(file.tokens)

            tmp_out_file = file.get_tmp_filename()
            logging.debug("Saving result of triplication to %s" % tmp_out_file)

            f = open(tmp_out_file, "w")
            if self.options.header and self.config.getboolean("tmrg", "add_header"):
                header = self.getHeader(file.filename, file.get_out_filename())
                f.write(header)
            f.write(self.vf.format(tmrTokens).replace("\t", " "*self.config.getint("tmrg", "spaces")))
            f.close()

            if self.options.stats:
                lines = self.lineCount(tmp_out_file)
                self.statsLogs.append("File '%s' has %d lines " % (tmp_out_file, lines))
                self.tmrLinesTotal += lines

        topFile, ext = os.path.splitext(os.path.basename(self.topFile))
        ftop = os.path.join(self.config.get("tmrg", "tmr_dir"), topFile+tmrSuffix+ext+'.new')
        self._addCommonModules(ftop)

        if self.options.simplify_verilog:
            try:
                from pyosys import libyosys as ys
                mode = "-o"
                for file in self.files:
                    tmp_file = file.get_tmp_filename()
                    design = ys.Design()
                    ys.run_pass("tee -q %s yosys.log read_verilog %s" % (mode, tmp_file) , design)
                    ys.run_pass("tee -q -a yosys.log proc", design)
                    ys.run_pass("tee -q -a yosys.log write_verilog  %s" % tmp_file, design)
                    mode = "-a"
                    del design
            except ModuleNotFoundError:
                raise ErrorMessage("Option '--simplify-verilog' requires pyosys.")

        for file in self.files:
            tmp_out_file = file.get_tmp_filename()
            out_file     = file.get_out_filename()

            if self.options.generateBugReport:
                fcopy = os.path.join(self.options.bugReportDir, out_file)
                logging.debug("Coping output file from '%s' to '%s'" % (tmp_out_file, fcopy))
                shutil.copyfile(tmp_out_file, fcopy)

            if os.path.exists(out_file):
                if filecmp.cmp(out_file, tmp_out_file):
                    logging.info("File '%s' exists. Its content is up to date." % out_file)
                    logging.debug("Removing temporary file %s." % tmp_out_file)
                    os.remove(tmp_out_file)
                else:
                    if self.config.getboolean("tmrg", "overwrite_files"):
                        logging.debug("Overwriting %s by %s" % (out_file, tmp_out_file))
                        os.rename(tmp_out_file, out_file)
                    else:
                        logging.warning("File '%s' exists. Saving output to '%s'" % (out_file, tmp_out_file))
            else:
                logging.info("Saving output to '%s'" % (out_file))
                logging.debug("Rename %s to %s" % (tmp_out_file, out_file))
                os.rename(tmp_out_file, out_file)

        self.genSDC()
        if self.options.stats:
            tmr_time = time.time()-tmr_start_time
            for line in self.statsLogs:
                print(line)
            print("Total number of triplicated lines: %d " % self.tmrLinesTotal)
            print("Triplication time : %.3f s " % tmr_time)
            print("-"*80)

    def genSDC(self):
        tmrSuffix = "TMR"

        def _findVotersAndFanouts(module, i="", ret=[]):
            for fanoutInst in self.global_namespace.modules[module].fanouts:
                fanout = self.global_namespace.modules[module].fanouts[fanoutInst]
                postfix = "[*]" if len(fanout.base_signal.unpacked + fanout.base_signal.packed) > 0 else ""

                ret.append(i + fanout.a + postfix)
                ret.append(i + fanout.b + postfix)
                ret.append(i + fanout.c + postfix)
                ret.append(i + fanout.u + postfix)

            for group in self.global_namespace.modules[module].voters:
                for voterInst in self.global_namespace.modules[module].voters[group]:
                    voter = self.global_namespace.modules[module].voters[group][voterInst]
                    postfix = "[*]" if len(voter.base_signal.unpacked) + len(voter.base_signal.packed) > 0 else ""

                    ret.append(i + voter.a + postfix)
                    ret.append(i + voter.b + postfix)
                    ret.append(i + voter.c + postfix)
                    ret.append(i + voter.u + postfix)

            for (instName, inst) in self.global_namespace.modules[module].instances:
                ni = i+"%s/" % instName
                if inst.module_name in self.global_namespace.modules:
                    _findVotersAndFanouts(inst.module_name, ni, ret)
                else:
                    pass
            return ret

        if self.config.getboolean("tmrg", "sdc_generate") or self.options.sdc_generate:
            topFile, ext = os.path.splitext(os.path.basename(self.topFile))
            if self.options.sdc_fileName != "":
                fsdc = self.options.sdc_fileName
            elif self.config.get("tmrg", "sdc_file_name") != "":
                fsdc = self.config.get("tmrg", "sdc_file_name")
            else:
                fsdc = os.path.join(self.config.get("tmrg", "tmr_dir"), topFile+tmrSuffix+".sdc")
            logging.info("Generating SDC constraints file %s" % fsdc)

            header = ""
            if self.config.getboolean("tmrg", "sdc_headers") or self.options.sdc_headers:
                header = "set sdc_version 1.3\n"
            # generate sdf file
            ret = _findVotersAndFanouts(self.topModule, i="/")
            f = open(fsdc, "w")
            f.write(header)
            f.write("""
set tmrgSucces 0
set tmrgFailed 0
proc constrainNet netName {
  global tmrgSucces
  global tmrgFailed
  # find nets matching netName pattern
  set nets [dc::get_net $netName]
  if {[llength $nets] != 0} {
    set_dont_touch $nets
    incr tmrgSucces
  } else {
    puts "\[TMRG\] Warning! Net(s) '$netName' not found"
    incr tmrgFailed
  }
}

""")
            if self.__voterPresent:
                # f.write("set_dont_touch majorityVoter\n")
                pass
            if self.__fanoutPresent:
                # f.write("set_dont_touch fanout\n")
                pass
            retset = set(ret)  # we can have some duplicates because of voters
            for l in sorted(retset):
                f.write("constrainNet %s\n" % l)

            f.write('\n\n    puts "TMRG successful  $tmrgSucces failed $tmrgFailed"\n')

            f.close()

            if self.options.generateBugReport:
                fcopy = os.path.join(self.options.bugReportDir, os.path.basename(fsdc))
                logging.debug("Coping output file from '%s' to '%s'" % (fsdc, fcopy))
                shutil.copyfile(fsdc, fcopy)

def main():
    OptionParser.format_epilog = lambda self, formatter: self.epilog
    parser = OptionParser(version="TMRG %s" % tmrg_version(),
                          usage="%prog [options] fileName [fileName2 fileName3]", epilog=epilog)

    parser.add_option("-v",  "--verbose",          dest="verbose",      action="count",
                      default=0, help="More verbose output (use: -v, -vv, -vvv..)")
    parser.add_option("",  "--doc",               dest="doc",  action="store_true",
                      default=False, help="Open documentation in web browser")

    actionGroup = OptionGroup(parser, "Actions")
    actionGroup.add_option("-p", "--parse-only",        action="store_true",
                           dest="parse",     default=False, help="Parse")
    actionGroup.add_option("-e", "--elaborate",         action="store_true",
                           dest="elaborate", default=False, help="Elaborate")
    actionGroup.add_option("", "--log",                 dest="log",
                           default="",             help="Store detailed log to file")
    parser.add_option_group(actionGroup)

    dirGroup = OptionGroup(parser, "Directories")
    dirGroup.add_option("",   "--rtl-dir",           dest="rtl_dir",      action="store", default="",
                        help="All files from this directory are taken as input files (only if no input files are specified as arguments)")
    dirGroup.add_option("",   "--inc-dir",           dest="inc_dir",      action="append", default=[],
                        help="Directory where to look for include files (use option --include to actualy include the files during preprocessing)")
    dirGroup.add_option("",   "--tmr-dir",           dest="tmr_dir",      action="store", default="",
                        help="Directory for output files (where all the *TMR.v files are placed)")
    dirGroup.add_option("-l",  "--lib",              dest="libs",       action="append",    default=[],
                        help="Verilog file to be included as a library (modules from this file are not triplicated)")
    parser.add_option_group(dirGroup)

    tmrGroup = OptionGroup(parser, "Triplication")
    tmrGroup.add_option("",    "--tmr-suffix",       dest="tmr_suffix",   action="store", default="")
    tmrGroup.add_option("-c",  "--config",           dest="config",
                        action="append",   default=[], help="Load config file")
    tmrGroup.add_option("-w",  "--constrain",        dest="constrain",
                        action="append",   default=[], help="Load config file")
    tmrGroup.add_option("",  "--common-cells-postfix",  dest="common_cells_postfix",  action="store",
                        default="",    help="String to be appended to common cell names")
    tmrGroup.add_option("",  "--no-common-definitions", dest="common_definitions", action="store_false",
                        default=True, help="Do not add definitions of common modules (majorityVoter and fanout)")
    tmrGroup.add_option("-b","--bare-minimum", dest="bare_minimum", action="store_true",
                        default=False, help="Alias for --no-common-definitions --no-header")
    tmrGroup.add_option("-P","--pragmas", dest="pragmas", action="store",
                        default="", help="Comma-separated list of comment pragmas to be preserved.")
    tmrGroup.add_option("-C","--comments", dest="comments", action="store_true",
                        default=False, help="Tries to preserve single line comments. Inline comments will still be removed. If failing, try to run `tmrg` without this option.")
    tmrGroup.add_option("",  "--no-header",          dest="header",       action="store_false",
                        default=True, help="Do not append  information header to triplicated file.")
    tmrGroup.add_option("",  "--sdc-generate",       dest="sdc_generate",   action="store_true",
                        default=False, help="Generate SDC file for Design Compiler")
    tmrGroup.add_option("",  "--sdc-headers",        dest="sdc_headers",
                        action="store_true",   default=False, help="Append SDC headers")
    tmrGroup.add_option("",  "--sdc-file-name",      dest="sdc_fileName",    default="",   help="Specify SDC filename")
    tmrGroup.add_option("",  "--generate-report",    dest="generateBugReport",
                        action="store_true",   default=False, help="Generate bug report")
    tmrGroup.add_option("",  "--stats",              dest="stats",    action="store_true",   help="Print statistics")
    tmrGroup.add_option("",  "--include",            dest="include",    action="store_true",
                        default=False,   help="Include include files")
    tmrGroup.add_option("",  "--profile",            dest="profile",    default="", help="Dump profiling info to file")
    dirGroup.add_option("",   "--top-module",        dest="top_module",
                        action="store", default="",  help="Specify top module name")
    dirGroup.add_option("",   "--simplify-verilog",   dest="simplify_verilog",
                        action="store_true", default=False,  help="Simplifies generated verilog code to enable SET injection (requires pyosys)")
    parser.add_option_group(tmrGroup)
    logFormatter = logging.Formatter('[%(levelname)-7s] %(message)s')
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)
    exit_code = 0
    try:
        (options, args) = parser.parse_args()

        if options.verbose == 0:
            consoleHandler.setLevel(logging.WARNING)
        if options.verbose == 1:
            consoleHandler.setLevel(logging.INFO)
        elif options.verbose == 2:
            consoleHandler.setLevel(logging.DEBUG)

        if options.log != "":
            logging.debug("Creating log file '%s'" % options.log)
            fileHandler = logging.FileHandler(options.log, mode='w')
            fileHandler.setFormatter(logFormatter)
            fileHandler.setLevel(logging.DEBUG)
            rootLogger.addHandler(fileHandler)

        if options.generateBugReport:
            bugReportDir = "bugReport_%s_%s" % (getpass.getuser(), time.strftime("%d%m%Y_%H%M%S"))
            options.bugReportDir = bugReportDir
            makeSureDirExists(bugReportDir)
            fileHandlerBug = logging.FileHandler(os.path.join(bugReportDir, "log.txt"))
            fileHandlerBug.setFormatter(logFormatter)
            fileHandlerBug.setLevel(logging.DEBUG)
            rootLogger.addHandler(fileHandlerBug)
            logging.info("Creating debug report in location '%s'" % bugReportDir)
            logging.debug("Creating log file '%s'" % options.log)
            logging.debug("Run cmd '%s'" % " ".join(sys.argv))

        if options.profile != "":
            sys.setprofile(tracefunc)

        if options.bare_minimum:
            options.header = False
            options.common_definitions = False

        tmrg = TMR(options, args)

        if options.doc:
            startDocumentation()
            return

        tmrg.parse()
        if options.parse:
            return

        tmrg.elaborate()
        tmrg.showSummary()
        if options.elaborate:
            return

        tmrg.triplicate()

        if options.generateBugReport:
            options.bugReportDir = bugReportDir
            zipFile = options.bugReportDir+".zip"

            def zipdir(path, zipf):
                for root, dirs, files in os.walk(path):
                    for file in files:
                        zipf.write(os.path.join(root, file))
            fileHandlerBug.close()
            zipf = zipfile.ZipFile(zipFile, 'w')
            zipdir(options.bugReportDir, zipf)
            zipf.close()
            consoleHandler.setLevel(logging.INFO)
            logging.info("Creating zip archive with bug report '%s'" % zipFile)
            try:
                shutil.rmtree(options.bugReportDir)
                os.rmdir(options.bugReportDir)
            except:
                pass
    except ErrorMessage as e:
        for line in str(e).split("\n"):
            logging.error(line)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logging.debug("The exception was raised from:")
        for l in traceback.format_tb(exc_traceback):
            for ll in l.split("\n"):
                logging.debug(ll)
        logging.debug(ll)
        rootLogger.handlers = []
        exit_code = 1

    rootLogger.handlers = []

    if options.profile != "":
        with open(options.profile, "w") as f:
            times = dict(sorted(tracefunc.stats.items(), key= lambda item: item[1]))

            for function in times:
                f.write("%s : %s\n" % (datetime.timedelta(seconds=times[function]), function))

    sys.exit(exit_code)

def tracefunc(frame, event, arg, indent=[0]):
    try:
        current_time = time.time()
        elapsed_time = current_time - tracefunc.last

        try:
            class_name = frame.f_locals['self'].__class__.__name__
        except KeyError:
            return

        if class_name not in ["VerilogParser", "VerilogElaborator", "TMR", "VerilogFormatter"]:
            return

        function = class_name + "." + frame.f_code.co_name

        # Calling a new function
        if event == "call":
            parent = tracefunc.stack[-1]
            tracefunc.stats[parent] += elapsed_time

            tracefunc.stats[function] = 0

            tracefunc.stack.append(function)

        # Closing a call
        elif event == "return":
            if function not in tracefunc.stats:
                return

            tracefunc.stats[function] += elapsed_time

            tracefunc.stack.pop()

        tracefunc.last = current_time
    except:
        pass

tracefunc.stack = ["main"]
tracefunc.stats = {"main" : 0}
tracefunc.last = time.time()

if __name__ == "__main__":
    main()
