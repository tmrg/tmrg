#!/usr/bin/env python2
# -*- coding: utf-8 -*-

try:
    from cPyparsing import ParseResults
except:
    from pyparsing import ParseResults

from .verilog_parser import VerilogParser

class VerilogFormatter:
    formater = {}

    def setTrace(self, t):
        self.trace = t

    def _format_Top(self, tokens, i=""):
        oStr = ""
        for i in tokens:
            oStr += self.format(i)
        return oStr

    def _format_Default(self, tokens, i=""):
        return "default (%s : %s)\n" % (tokens.getName(), tokens)

    def _format_Id(self, tokens, i=""):
        oStr = str(tokens[0])
        return oStr

    def _format_None(self, tokens, i=""):
        oStr = ""
        for t in tokens:
            oStr += self.format(t)+" "
        return oStr

    def _format_assign(self, tokens, i=""):
        oStr = tokens[0]
        oStr += " %s" % self.format(tokens[1])
        oStr += "="
        for t in tokens[2:]:
            oStr += self.format(t)+" "
        return oStr

    def _format_array_size(self, tokens, i=""):
        return "[%s]" % self.format(tokens[0])

    def _formatIoHdr(self, tokens, i="", sep="", end=""):
        oStr = ""
        spec = []
        dir = str(tokens.get("dir"))
        atributes = self.format(tokens.get("attributes")) if tokens.get("attributes") else ""
        key = "untyped"
        if "typed" in tokens:
            typed = tokens.get("typed")

            if "net_type" in typed:
                spec.append(self.format(typed.get("net_type")[0]))

            if "standard_type" in typed:
                spec.append(self.format(typed.get("standard_type")[0]))

                if "modifiers" in typed:
                    spec.append(self.format(typed.get("modifiers")))
            elif "custom_type" in typed:
                spec.append(self.format("".join(typed.get("custom_type"))))

            key = "typed"

        for r in tokens.get(key).get("packed_ranges"):
            spec.append(self.format(r))

        spec = " ".join(spec)
        if spec:
            spec += " "

        _identifiers = tokens.get(key).get("identifiers")

        oStr = []
        for port in _identifiers:
            name = port.get("name")[0]
            array = ""
            for r in port.get("unpacked_ranges"):
                array += " "+self.format(r)

            oStr.append("%s%s %s%s%s" % (atributes, dir, spec, name, array))

        oStr = sep.join(oStr) + end

        return oStr

    def _format_porthdr(self, tokens, i=""):
        return self._formatIoHdr(tokens, i=i, sep=",\n  ", end="")

    def _format_portbody(self, tokens, i=""):
        return self._formatIoHdr(tokens, i=i, sep=";\n", end=";\n")

    def _format_port(self, tokens, i=""):
        if tokens[0].getName() != "container":
            return self.format(tokens[0])

        defs = []
        for t in tokens[0]:
            defs.append(self.format(t))

        return (",\n"+i).join(defs)

    def _format_portnotansi(self, tokens, i=""):
        packed = ""
        for r in tokens.get("packed_ranges"):
            packed += " "+self.format(r)
        if packed:
            packed += " "

        name = tokens.get("identifier").get("name")[0]

        unpacked = ""
        for r in tokens.get("identifier").get("unpacked_ranges"):
            unpacked += " "+self.format(r)
        if unpacked:
            unpacked += " "

        return "%s%s%s" % (packed, name, unpacked)

    def _format_force(self, tokens, i=""):
        force = str(tokens[0])
        assignment = self._format_assignment(tokens[1])
        return "%s%s %s;\n" % (i, force, assignment)

    def _format_release(self, tokens, i=""):
        release = str(tokens[0])
        return "%s%s %s;\n" % (i, release, self.format(tokens.get("lvalue")[0]))

    def _format_netdecl(self, tokens, i=""):
        label = []
        if "net_type" in tokens:
            label.append(tokens.get("net_type")[0])

        if "standard_type" in tokens:
            label.append(tokens.get("standard_type")[0])

            if "modifiers" in tokens:
                label.append(tokens.get("modifiers"))
        elif "custom_type" in tokens:
            label.append("".join(tokens.get("custom_type")))

        for r in tokens.get("packed_ranges"):
            label.append(self.format(r))

        label = " ".join(label)
        if label:
            label += " "

        atributes = self.format(tokens.get("attributes")) if tokens.get("attributes") else ""

        oStr = ""
        for port in tokens.get("identifiers"):
            unpacked = ""
            for r in port.get("unpacked_ranges"):
                unpacked += " " + self.format(r)

            oStr += "%s%s%s%s;\n" % (atributes, label, port.get("name")[0], unpacked)
        return oStr

    def _format_netdeclwassign(self, tokens, i="", ending=";\n"):
        label = []
        if "lifetime" in tokens:
            label.append(tokens.get("lifetime")[0])

        if "net_type" in tokens:
            label.append(tokens.get("net_type")[0])

        if "standard_type" in tokens:
            label.append(tokens.get("standard_type")[0])

            if "modifiers" in tokens:
                label.append(tokens.get("modifiers"))
        elif "custom_type" in tokens:
            label.append("".join(tokens.get("custom_type")))

        for r in tokens.get("packed_ranges"):
            label.append(self.format(r))

        label = " ".join(label)
        if label:
            label += " "

        atributes = self.format(tokens.get("attributes")) if tokens.get("attributes") else ""

        oStr = ""

        for assignment in tokens.get("assignments"):
            assignment_str = self._format_assignment(assignment)
            oStr += "%s%s%s%s%s" % (i, atributes, label, assignment_str, ending)
        return oStr

    def _format_realDecl(self, tokens, i=""):
        oStr = i
        real = str(tokens[0])
        ports = tokens[1:]
        for port in ports:
            port_name = port[0]
            port_str = self.format(port[1:])
            if port_str != "":
                port_str = " "+port_str
            oStr += "%s %s%s;\n" % (real, port_name, port_str)
        return oStr

    def _format_genVarDecl(self, tokens, i=""):
        oStr = i + str(tokens[0]) + " "

        names = [t[0] for t in tokens.get("names")]
        oStr += ", ".join(names) + ";\n"

        return oStr

    def _format_genVarDeclWAssign(self, tokens, i=""):
        oStr = i+"genvar "+tokens.get("name")[0]

        oStr += " = " + self.format(tokens.get("expr@rvalue"))

        return oStr

    def _format_namedPortConnection(self, tokens, i=""):
        oStr = ""
        for i in tokens:
            oStr += self.format(i)
        return oStr

    def _format_generate_module_named_block(self, tokens, i=""):
        oStr = i + "begin" + (" : " + tokens.get("id@begin")[0] if "id@begin" in tokens else "") + "\n"
        for stmt in tokens.get("members"):
            oStr += self.format(stmt, i+"\t")
        oStr += i+"end" + (" : " + tokens.get("id@end")[0] if "id@end" in tokens else "") + "\n"
        return oStr

    def _format_container(self, tokens, i=""):
        return "".join([self.format(token, i=i) for token in tokens])

    # This is a utility
    def __format_beginend_if_container(self, tokens, i=""):
        if tokens.getName() != "container":
            return i+self.format(tokens, i+"\t")

        oStr = "\n\t" + i + "begin\n"
        oStr += i+self.format(tokens, i+"\t\t")
        oStr += i+"\tend\n"
        return oStr

    def _format_generate_for_statement(self, tokens, i=""):
        genvar_decl_assignment = self.format(tokens.get("for1")[0]).replace("\n", " ")
        genvar_cond = self.format(tokens.get("expr@for2"))
        genvar_assignment = self.format(tokens.get("for3")[0])

        oStr = i+"for(%s; %s; %s)\n" % (genvar_decl_assignment, genvar_cond, genvar_assignment)
        oStr += self.__format_beginend_if_container(tokens.get("generateStmt")[0], i)

        return oStr

    def _format_generate_if_else_statement(self, tokens, i=""):
        oStr = i + "if %s\n" % self.format(tokens[0])

        oStr += self.__format_beginend_if_container(tokens.get("if_stmt")[0], i)

        if "else_stmt" not in tokens:
            return oStr

        oStr += "\n" + i + "else\n"
        oStr += self.__format_beginend_if_container(tokens.get("else_stmt")[0], i)

        return oStr

    def _format_generate_case_item(self, tokens, i=""):
        oStr = self.format(tokens.get("case_item_expr")[0]) + " : "
        oStr += self.__format_beginend_if_container(tokens.get("case_item_body")[0], i)

        return oStr

    def _format_generate_case_statement(self, tokens, i=""):
        oStr = "%s (%s)\n" % (tokens.get("case_flavour")[0], self.format(tokens.get("expr@case")))
        for t in tokens.get("case_items"):
            oStr += i+"\t"+self.format(t, i+"\t")+"\n"
        oStr += i + "endcase"

        return oStr

    def _format_generate(self, tokens, i=""):
        oStr = ""
        oStr = "\n"+i

        oStr += "generate\n"
        for sub_tokens in tokens:
            oStr += self.format(sub_tokens, i+"\t")+"\n"
        oStr += i+"endgenerate\n"
        return oStr

    def _format_Range(self, tokens, i=""):
        oStr = "["
        oStr += self.format(tokens.get("expr@from"))
        oStr += tokens.get("dir")
        oStr += self.format(tokens.get("expr@to"))
        oStr += "]"
        return oStr

    def _format_Size(self, tokens, i=""):
        oStr = "["
        oStr += self.format(tokens[0])
        oStr += "]"
        return oStr

    def _format_Field(self, tokens, i=""):
        return "." + tokens[0]

    def _format_Always(self, tokens, i=""):
        oStr = "\n"+i + "%s" % self.format(tokens.get("type"))
        if "eventCtrl" in tokens:
            oStr += " %s" % self.format(tokens.get("eventCtrl"))

        oStr += "\n" + i + "\t" + self.format(tokens.get("stmt")[0], i+"\t") + "\n"

        return oStr

    def _format_reg_reference(self, tokens, i=""):
        oStr = ""

        if "attributes" in tokens:
            oStr += self.format(tokens.get("attributes")) + " "

        if "namespace" in tokens:
            oStr += tokens.get("namespace")[0] + "::"

        oStr += tokens.get("name")[0]

        for sid in tokens.get("range_or_field"):
            oStr += self.format(sid)

        return oStr

    def _format_field_ref(self, tokens, i=""):
        return "."+tokens[0]

    def _format_BeginEnd(self, tokens, i=""):
        oStr = "begin"
        if "id@blockName" in tokens:
            oStr += " : " + self.format(tokens.get("id@blockName"))

        oStr += "\n"

        for stmt in tokens.get("declarations"):
            oStr += i+"\t"+self.format(stmt, i+"\t")+"\n"
        for stmt in tokens.get("statements"):
            oStr += i+"\t"+self.format(stmt, i+"\t")+"\n"

        oStr += i+"end"

        if "id@blockNameEnd" in tokens:
            oStr += " : " + self.format(tokens.get("id@blockName"))

        return oStr

    def _format_delayStm(self, tokens, i=""):
        oStr = ""
        delay = self.format(tokens[0])
        stm = self.format(tokens[1])
        if stm != ";":
            stm = " "+stm

        oStr += "%s%s%s" % (i, delay, stm)
        return oStr

    def _format_timeunitsdecl(self, tokens, i=""):
        oStr = i + tokens.get("keyword")[0] + " " + self.format(tokens.get("time0")[0])

        if "time1" in tokens:
            oStr += " / " + self.format(tokens.get("time1")[0])

        oStr += ";\n"

        return oStr

    def _format_EventCtrl(self, tokens, i=""):
        oStr = ""
        for el in tokens:
            oStr += self.format(el)
        return oStr

    def _format_DelimitedList(self, tokens, i=""):
        oStr = ""
        sep = ""
        for el in tokens:
            oStr += sep+self.format(el)
            sep = ", "
        return oStr

    def _format_DelimitedOrList(self, tokens, i=""):
        oStr = ""
        sep = ""
        for el in tokens:
            oStr += sep+self.format(el)
            sep = " or "
        return oStr

    def _format_EventTerm(self, tokens, i=""):
        oStr = ""
        sep = ""
        for el in tokens:
            oStr += sep+self.format(el)
            sep = " "
        return oStr

    def _format_ifelse(self, tokens, i=""):
        oStr = (tokens.get("modifier")[0] + " ") if "modifier" in tokens else ""
        oStr += "if %s\n" % self.format(tokens.get("condition"))
        oStr += i+"\t%s\n" % self.format(tokens.get("stmt_true")[0], i+"\t")
        if "stmt_false" in tokens:
            oStr += i+"else\n"
            oStr += i+"\t%s" % self.format(tokens.get("stmt_false")[0], i+"\t")
        return oStr

    def _format_streaming_expression(self, tokens, i=""):
        slice_size = self.format(tokens.get("slice_size")[0]) if "slice_size" in tokens else ""
        concat = self._format_concat(tokens.get("streaming_concat")[0])
        return "{" + tokens.get("streaming_op")[0] + slice_size + " " + concat + "}"

    def _format_concat(self, tokens, i=""):
        elements = []
        for t in tokens.get("elements"):
            s = ""
            if "field_name" in t:
                s += t.get("field_name")[0] + ": "

            elements.append(s + self.format(t.get("expr@field_value")))

        return "{" + ", ".join(elements) + "}"

    def _format_caseItem(self, tokens, i=""):
        expr = self.format(tokens[0])
        stm = self.format(tokens[1], i+"\t")
        if stm.find("\n") >= 0:
            stm = "\n%s%s" % (i+"\t", stm)
        oStr = "%s : %s" % (expr, stm)
        return oStr

    def _format_case(self, tokens, i=""):
        oStr = (tokens.get("modifier")[0] + " ") if "modifier" in tokens else ""

        oStr += "%s (%s)" % (tokens.get("case_flavour")[0], self.format(tokens.get("expr@case")))
        oStr += " inside\n" if "inside" in tokens else "\n"

        for t in tokens.get("case_items"):
            oStr += i+"\t"+self.format(t, i+"\t")+"\n"
        oStr += i + "endcase"

        return oStr

    def _format_standaloneFuncOrTask(self, tokens, i=""):
        return self._format_funcOrTaskCall(tokens[0]) + ";"

    def _format_funcOrTaskCall(self, tokens, i=""):
        oStr = ""
        if "namespace" in tokens:
            oStr += tokens.get("namespace")[0] + "::"

        oStr += tokens.get("name")[0]

        if "arguments" in tokens:
            oStr += "(" + ", ".join([self.format(expr) for expr in tokens.get("arguments")]) + ")"

        if "semi" in tokens:
            oStr += ";"

        return oStr

    def _format_parameterValueAssignment(self, tokens, i=""):
        if len(tokens[0]) > 1:
            oStr = "#(\n"+i
            sep = ""
            for param in tokens[0]:
                oStr += sep+self.format(param)
                sep = ",\n"+i
            oStr += "\n)\n"
            return oStr
        else:
            oStr = "#("
            oStr += self.format(tokens[0][0])
            oStr += ") "
            return oStr

    def _format_primary(self, tokens, i=""):
        oStr = ""
        for t in tokens:
            oStr += self.format(t, i="")
        return oStr

    def _format_moduleInstance(self, tokens, i=""):
        id    = self.format(tokens[0][0])
        range = ""
        if len(tokens[0]) == 2:
            range = self.format(tokens[0][1]) + " "

        args = self.format(tokens[1], i=i)

        return "%s %s%s" % (id, range, args)

    def _format_moduleArgs(self, tokens, i=""):
        defs = []
        indexes = []
        for t in tokens:
            if t.getName() != "comment":
                indexes.append(len(defs))
            
            defs.append(self.format(t, i+"\t"))
        
        # Comma separate all ports but the last
        for idx in indexes[0:-1]:
            defs[idx] += ","

        # Add a new line only after ports, comments already have them
        for idx in indexes:
            defs[idx] += "\n"

        return "(\n" + i + "\t" + (i+"\t").join(defs) + i + ")"

    def _format_modulePortConnection(self, tokens, i=""):
        return self.format(tokens[0], i)

    def _format_moduleInstantiation(self, tokens, i=""):
        attributes = self.format(tokens.get("attributes")) if tokens.get("attributes") else ""

        identifier = tokens.get("moduleName")[0]
        if len(tokens) > 2:
            parameterValueAssignment = self.format(tokens[2], i=i+"\t")
        else:
            parameterValueAssignment = ""

        oStr = "\n%s%s%s %s" % (i, attributes, identifier, parameterValueAssignment)
        oStr += ", ".join([self.format(modIns, i=i+"\t") for modIns in tokens.get("moduleInstances")])
        oStr += ";\n"

        return oStr
    
    def __format_ports(self, tokens, i):
        defs = []
        indexes = []
        for port in tokens:
            if port.getName() == "comment":
                defs.append("\t"+self.format(port, i+"\t"))
                continue

            if port.getName() == "container":
                for subport in port:
                    indexes.append(len(defs))
                    defs.append("\t"+self.format(subport, i+"\t"))
                continue

            indexes.append(len(defs))
            defs.append("\t"+self.format(port, i+"\t"))

        # Comma separate all ports but the last
        for idx in indexes[0:-1]:
            defs[idx] += ","

        # Add a new line only after ports, comments already have them
        for idx in indexes:
            defs[idx] += "\n"

        return " (\n" + "".join(defs) + ")" if defs else ""

    def _format_functionDecl(self, tokens, i=""):
        header = []
        if "lifetime" in tokens:
            header.append(tokens.get("lifetime")[0])

        if "return_type" in tokens:
            header.append(tokens.get("return_type")[0])

        if "return_range" in tokens:
            header.append(self._format_Range(tokens.get("return_range")))

        header = " ".join(header)
        if header:
            header += " "

        oStr = "function " + header + tokens.get("name")[0]
        
        if "ports" in tokens:
            oStr += self.__format_ports(tokens.get("ports"), i)
                                       
        oStr += ";\n"

        for item in tokens.get("tfDecl"):
            oStr += ("\n").join([i+"\t"+x for x in self.format(item).splitlines()])+"\n"

        for item in tokens.get("statements"):
            oStr += ("\n").join([i+"\t"+x for x in self.format(item).splitlines()])+"\n"

        oStr += i + "endfunction\n\n"
        return oStr

    def _format_task(self, tokens, i=""):
        lifetime = tokens.get("lifetime")[0] + " " if "lifetime" in tokens else ""
        oStr = "task " + lifetime + tokens.get("name")[0]
        
        if "ports" in tokens:
            oStr += self.__format_ports(tokens.get("ports"), i)
                                       
        oStr += ";\n"

        for item in tokens.get("tfDecl"):
            oStr += ("\n").join([i+"\t"+x for x in self.format(item).splitlines()])+"\n"

        for item in tokens.get("statements"):
            oStr += ("\n").join([i+"\t"+x for x in self.format(item).splitlines()])+"\n"

        oStr += "endtask\n"
        return oStr

    def _format_module(self, tokens, i=""):
        atributes = self.format(tokens.get("attributes")) if tokens.get("attributes") else ""
        header = tokens[1]
        modname = header[1]
        oStr = atributes + "module %s" % modname
        if "package_import" in header:
            oStr += " " + self._format_package_import(header.get("package_import"))[:-1]

        if "parameterList" in header:
            defs = []
            indexes = []
            for p in header.get("parameterList"):
                if p.getName() != "comment":
                    indexes.append(len(defs))
                defs.append("  " + self._format_paramdecl(p, end_with_colon=False))

            # Comma separate all parameters but the last
            for idx in indexes[0:-1]:
                defs[idx] += ","

            # Add a new line only after parameters, comments already have them
            for idx in indexes:
                defs[idx] += "\n"

            if defs:
                oStr += " #(\n" + "".join(defs) + ")"

        if "ports" in header:
            oStr += self.__format_ports(header.get("ports"), i)

        oStr += ";\n"

        moduleBody = tokens[2]
        for moduleItem in moduleBody:
            oStr += self.format(moduleItem)

        endmodule_label = ""
        if len(tokens[4]):
            endmodule_label = " : %s" % modname
        oStr += "endmodule%s\n\n" % (endmodule_label)
        return oStr

    def _format_nbassignment(self, tokens, i=""):
        lval = self.format(tokens.get("lvalue")[0])
        doec = ""
        if "delayOrEventControl" in tokens:
            doec = self.format(tokens.get("delayOrEventControl")[0]) + " "

        expr = self.format(tokens.get("expr@rvalue"))
        oStr = "%s <= %s%s;" % (lval, doec, expr)

        return oStr

    def _format_inlineIfExpr(self, tokens, i=""):
        if len(tokens) == 1:
            return self.format(tokens[0])
        primary = self.format(tokens[0])
        expr1 = self.format(tokens[1])
        expr2 = self.format(tokens[2])

        return "%s ? %s : %s" % (primary, expr1, expr2)

    def _format_inside(self, tokens, i=""):
        return self.format(tokens[0]) + " inside " + self.format(tokens[2])

    def _format_Expr(self, tokens, i=""):
        oStr = ""
        for t in tokens:
            ft = self.format(t)

            if ft == "(":
                ft = " " + ft
            elif ft == ")":
                ft = ft + " "
            elif ft in ["+", "-", "/", "*"]:
                ft = " " + ft + " "

            oStr += ft

        return oStr

    def _format_return(self, tokens, i=""):
        return "return " + self.format(tokens.get("expr")) + ";"

    def _format_standaloneDelay(self, tokens, i=""):
        return i + "#" + tokens[1][0] + " " + tokens[2] + ";"

    def _format_delay(self, tokens, i=""):
        oStr = "#"
        for t in tokens[1]:
            oStr += self.format(t)

        if len(tokens) == 3:
            if oStr[-1].isalpha():
                oStr += " "

            oStr += tokens[2]

        return oStr

    def _format_Condition(self, tokens, i=""):
        oStr = ""
        for t in tokens:
            oStr += self.format(t)
        return oStr

    def _format_defparam(self, tokens, i=""):
        oStr = "defparam "

        assigns = []
        for param in tokens.get("assignments"):
            assigns.append(self.format(param))

        oStr += ", ".join(assigns) + ";\n"
        return oStr

    def _format_paramdecl(self, tokens, i="", end_with_colon=True):
        decls = []
        pre = (tokens.get("param_type")[0] if "param_type" in tokens else "parameter") + " "

        if "standard_type" in tokens:
            pre += tokens.get("standard_type")[0] + " "

            if "modifiers" in tokens:
                pre += self.format(tokens.get("modifiers")) + " "

        elif "custom_type" in tokens:
            pre += "".join(tokens.get("custom_type")) + " "

        for r in tokens.get("packed_ranges"):
            pre += self(self.format(r)) + " "

        for p in tokens.get("params"):
            decls.append(self.format(p))

        oStr = pre + ", ".join(decls) + (";\n" if end_with_colon else "")

        return oStr

    def _format_modport(self, tokens, i=""):
        res = i + "modport " + tokens.get("name")[0] + " (\n"

        l = []
        for port in tokens.get("ports"):
            l.append(i + "  %s %s" % ( port.get("dir"), ", ".join([s[0] for s in port.get("signals")]) ))

        res += ",\n".join(l) + "\n"
        res += i + ");\n"

        return res

    def _format_interfacedecl(self, tokens, i=""):
        res = i + "interface " + tokens.get("id")[0] + ";\n"
        for elem in tokens.get("elems"):
            if elem.getName() == "signal":
                res += self._format_netdecl(elem[0], i + "  ")
            elif elem.getName() == "modport":
                res += "\n" + i + self._format_modport(elem, i + "  ")

        res += i + "endinterface\n\n"

        return res

    def _format_blockName(self, tokens, i=""):
        oStr = tokens[0]
        return oStr

    def _format_integerDecl(self, tokens, i=""):
        oStr = ""
        label = tokens[0]
        for var in tokens[1]:
            oStr += "%s %s;\n" % (label, self.format(var))
        return oStr

    def _format_from(self, tokens, i=""):
        return self._format_Expr(tokens)

    def _format_to(self, tokens, i=""):
        return self._format_Expr(tokens)

    def _format_typedefDecl(self, tokens, i=""):
        oStr = i + "typedef "

        label = []
        _type = tokens.get("type")

        if "standard_type" in _type:
            label.append(_type.get("standard_type")[0])

            if "modifiers" in _type:
                label.append(_type.get("modifiers"))
        else:
            label.append("".join(_type.get("custom_type")))

        for r in _type.get("packed_ranges"):
            label.append(self.format(r))

        oStr += " ".join(label) + " " + tokens.get("id")[0] + ";\n"

        return oStr

    def _format_enumDecl(self, tokens, i=""):
        oStr = i + "typedef enum "

        label = []
        _type = tokens.get("type")
        if "net_type" in _type:
            label.append(_type.get("net_type")[0])

        if "standard_type" in _type:
            label.append(_type.get("standard_type")[0])

            if "modifiers" in _type:
                label.append(_type.get("modifiers"))
        else:
            label.append("".join(_type.get("custom_type")))

        for r in _type.get("packed_ranges"):
            label.append(self.format(r))

        oStr += " ".join(label) + " {"

        fields = []
        for f in tokens.get("fields"):
            field = f.get("name")[0]

            if "expr@rvalue" in f:
                field += " = " + self._format_Expr(f.get("expr@rvalue"))

            fields.append(field)

        oStr += ", ".join(fields)

        oStr += "} " + tokens.get("id")[0] + ";\n"

        return oStr

    def _format_structDecl(self, tokens, i=""):
        oStr = tokens[0] + " { \n"

        for field in tokens[1]:
            label = []
            if "net_type" in field:
                label.append(field.get("net_type")[0])

            if "standard_type" in field:
                label.append(field.get("standard_type")[0])

                if "modifiers" in field:
                    label.append(field.get("modifiers"))
            else:
                label.append("".join(field.get("custom_type")))

            for r in field.get("packed_ranges"):
                label.append(self.format(r))

            label = " ".join(label)
            if label:
                label += " "

            for port in field.get("identifiers"):
                r = self.format(port.get("name")[0])
                if "unpacked_ranges" in port.keys():
                    r += self.format(port.get("unpacked_ranges"))

                oStr += i + "  %s%s;\n" % (label, r)

        oStr += i + "} " + tokens.get("id")[0] + ";\n"

        return oStr

    def _format_unionDecl(self, tokens, i=""):
        return self._format_structDecl(tokens, i)

    def _format_integerDeclAssgn(self, tokens, i=""):
        oStr = ""
        for assigment in tokens[1]:
            label = tokens[0]
            oStr += "%s %s = %s;\n" % (label, self.format(assigment[0]), self.format(assigment[-1]))
        return oStr

    def _format_assignmentStm(self, tokens, i=""):
        oStr = self.format(tokens[0])+";"
        return oStr

    def _format_nbassignmentStm(self, tokens, i=""):
        return self.format(tokens[0])

    def _format_assignment(self, tokens, i=""):
        lvalue = self.format(tokens.get("lvalue")[0])
        assign = tokens.get("op")

        doec = ""
        if "delayOrEventControl" in tokens:
            doec = " " + self.format(tokens.get("delayOrEventControl")[0])

        expr = self.format(tokens.get("expr@rvalue"))
        oStr = "%s %s%s %s" % (lvalue, assign, doec, expr)
        return oStr

    def _format_assignment_with_declaration(self, tokens, i=""):
        return tokens.get("type") + " " + tokens.get("name")[0] + " = " + self.format(tokens.get("expr@rvalue"))

    def _format_incr_decr(self, tokens, i=""):
        return self.format(tokens[0]) + tokens[1]

    def _format_forstmt(self, tokens, i=""):
        for1 = tokens.get("for1")[0]

        # If netDeclWAssign, remove newline and ;
        if for1.getName() == "netDeclWAssign":
            decl_assignment = self._format_netdeclwassign(for1, ending="")
        else:
            decl_assignment = self.format(for1)

        cond = self.format(tokens.get("expr@for2"))
        assignment = self.format(tokens.get("for3")[0])
        stmt = tokens.get("stmt")[0]

        oStr = "for(%s; %s; %s)\n" % (decl_assignment, cond, assignment)
        if stmt.getName().startswith("beginend"):
            oStr += i+self.format(stmt, i)
        else:
            oStr += i+"\t"+self.format(stmt, i=i+"\t")
        return oStr

    def _format_driveStrength(self, tokens, i=""):
        oStr = ""
        for t in tokens:
            oStr += self.format(t)
        return oStr

    def _format_attr_spec(self, tokens, i=""):
        return " ".join(tokens)

    def _format_attribute(self, tokens, i=""):
        oStr = tokens[0] + " "
        attr_list = []
        for attr in tokens[1]:
          attr_list.append(self.format(attr))
        oStr += ", ".join(attr_list)
        oStr += " " + tokens[2]
        return oStr

    def _format_attributes(self, tokens, i=""):
        oStr = ""
        for a in tokens:
            oStr += self.format(a)
        oStr += " "
        return oStr

    def _format_continuousAssign(self, tokens, i=""):
        pre = i + "assign "

        if "driveStrength" in tokens:
            pre += self.format(tokens.get("driveStrength")) + " "

        if "delay" in tokens:
            pre += self.format(tokens.get("delay")) + " "

        oStr = ""
        for asg in tokens.get("assignments"):
            asg_str = self.format(asg)
            oStr += "%s%s;\n" % (pre, asg_str)

        return oStr

    def _format_net3(self, tokens, i=""):
        oStr = ""
        return oStr

    def _format_initialStmt(self, tokens, i=""):
        oStr = "initial\n\t%s\n" % self.format(tokens[1], i+"\t")
        return oStr

    def _format_forever(self, tokens, i=""):
        oStr = "forever\n\t%s\n" % self.format(tokens[1], i+"\t")
        return oStr

    def _format_comment(self, tokens, i=""):
        words = tokens.get("content").lower().split()

        # If tmrg as_is_index, copy values from the buffer
        if len(words) >= 3 and (words[0], words[1]) == ("tmrg", "as_is_index"):
            return VerilogParser.as_is_buffers[int(words[2])] + "\n"

        # If special pragmas or comments are enabled, format them
        if VerilogParser.comments or (len(words) and words[0] in VerilogParser.pragmas + VerilogParser.extra_pragmas):
            return "//" + tokens.get("content") + "\n"

        return ""

    def _format_comp_directive(self, tokens, i=""):
        return tokens[0] + "\n"

    def _format_wait(self, tokens, i=""):
        action = ';' if tokens.get("action")[0] == ';' else " " + self.format(tokens.get("action")[0])
        return i+ "wait (" + self.format(tokens.get("expr@condition")) + ")" + action

    def _format_gateDecl(self, tokens, i=""):
        ostr = ""
        base_str = self.format(tokens[0]) + " "

        if "driveStrength" in tokens:
            base_str += self.format(tokens.get("driveStrength")) + " "

        if "delay" in tokens:
            base_str += self._format_delay(tokens.get("delay")) + " "

        for instance in tokens.get("instances"):
            gate_inst_str = base_str
            if "name_and_range" in instance:
                name_and_range = instance.get("name_and_range")
                gate_inst_str += name_and_range[0] + " "

                if len(name_and_range) == 2:
                    gate_inst_str += self.format(name_and_range[1]) + " "

            ports_str = ", ".join([self.format(p) for p in instance.get("ports")])

            ostr += "%s%s(%s);\n" % (i, gate_inst_str, ports_str)
        return ostr + "\n"

    def _format_packagedecl(self, tokens, i=""):
        res = "package " + tokens.get("id@packageName")[0] + ";\n"

        for elem in tokens.get("packageBody"):
            res += i + "  " + self.format(elem, i+"  ")

        res += i + "endpackage\n\n"

        return res

    def _format_package_import_item(self, tokens, i=""):
        return tokens[0][0] + "::" + tokens[2][0]

    def _format_package_import(self, tokens, i=""):
        oStr = tokens[0] + " " + ", ".join([self.format(x) for x in tokens[1]]) + ";\n"
        return oStr

    def __init__(self):
        for member in dir(self):
            if member.find("_format_") == 0:
                token = member[len("_format_"):].lower()
                self.formater[token] = getattr(self, member)
        self.trace = False

    def format(self, tokens, i=""):
        outStr = ""
        if tokens == None:
            return ""

        if isinstance(tokens, ParseResults):
            name = str(tokens.getName()).lower()
            offset = name.find("@")
            if offset != -1:
                name = name[0:offset]
            if self.trace:
                print("[%-20s] len:%2d  str:'%s' >" % (name, len(tokens), str(tokens)[:80]))
            if name != "moduleargs" and len(tokens) == 0:
                return ""
            if name in self.formater:
                outStr = self.formater[name](tokens, i)
            else:
                outStr = self.formater["default"](tokens, i)
        elif isinstance(tokens, list):
            outStr = "".join(map(self.format, tokens))
        else:
            outStr = tokens
        return outStr

    def __call__(self, tokens, i=""):
        return self.format(tokens, i)
