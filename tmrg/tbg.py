#!/usr/bin/env python3

# Copyright (c) CERN and the TMRG authors.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import logging
import os
import logging
import sys
import re
from optparse import OptionParser, OptParseError
from .tmrg import TMR
from .verilog_elaborator import readFile, ErrorMessage, LinearExpression
from .toolset import tmrg_version, epilog, startDocumentation


class TBG(TMR):
    def __init__(self, options, args):
        TMR.__init__(self, options, args)

    def loadVoterDefinition(self, voterName="majorityVoterTB"):
        vfile = os.path.join(self.scriptDir,  self.config.get("tmrg", "voter_definition"))
        logging.info("Taking voter declaration from %s" % vfile)
        return readFile(vfile).replace("majorityVoter", voterName)

    def generate(self):
        rename_tb = self.options.tbName != "" and len(self.global_namespace.modules)

        logging.debug("")
        tmrerrors = [] # store for formal analysis
        oStr = "`timescale 1 ps / 1 ps\n"
        voterCell = "majorityVoterTB"
        oStr += self.loadVoterDefinition(voterCell)+"\n"
        for module in self.global_namespace.modules:
            clocks = []
            resets = []
            if (rename_tb):
                oStr += "module %s;\n" % self.options.tbName
            else:
                oStr += "module %s_test;\n" % module

            oStr += "\n// - - - - - - - - - - - - - - Parameters section  - - - - - - - - - - - - - -\n"

            parameters = ""
            psep = "\n    "
            for k in self.global_namespace.modules[module].params:
                param = self.global_namespace.modules[module].params[k]
                if param.param_type == 'localparam':
                    continue

                oStr += "  " + str(param)
                parameters += "%s.%s(%s)" % (psep, k, k)
                psep = ",\n    "

            if len(parameters):
                parameters = "\n`ifndef NETLIST\n  #("+parameters+"\n  )\n`endif\n "

            oStr += "\n// - - - - - - - - - - - - - - Input/Output section  - - - - - - - - - - - - -\n"
            # initial declaration
            for ioName in sorted(self.global_namespace.modules[module].io):
                io = self.global_namespace.modules[module].io[ioName]
                packed_ranges_str   = " ".join([str(k) for k in io.packed])
                unpacked_ranges_str = " ".join([str(k) for k in io.unpacked])

                if io.direction == "input":
                    oStr += "  reg %s %s %s;\n" % (packed_ranges_str, ioName, unpacked_ranges_str)

                    if ioName.lower().find("clk") >= 0 or ioName.lower().find("clock") >= 0:
                        clocks.append(ioName)
                    if ioName.lower().find("rst") >= 0 or ioName.lower().find("reset") >= 0:
                        resets.append(ioName)

                elif io.direction == "output":
                    oStr += "  wire %s %s %s;\n" % (packed_ranges_str, ioName, unpacked_ranges_str)

                else:
                    logging.warning("Unsupported IO type: %s" % io.direction)

            oStr += "\n// - - - - - - - - - - - - - Device Under Test section - - - - - - - - - - - -\n"
            oStr += "\n`ifdef TMR\n"
            # initial declaration
            for ioName in sorted(self.global_namespace.modules[module].io):
                if not self.global_namespace.modules[module].nets[ioName].triplicate:
                    continue

                io = self.global_namespace.modules[module].io[ioName]
                packed_ranges_str   = " ".join([str(k) for k in io.packed])
                unpacked_ranges_str = " ".join([str(k) for k in io.unpacked])

                if io.direction == "input":
                    oStr += "  // fanout for %s\n" % (ioName)
                    for ext in self.EXT:
                        oStr += "  wire %s %s%s %s=%s;\n" % (packed_ranges_str, ioName, ext, unpacked_ranges_str, ioName)
                elif io.direction == "output":
                    oStr += "  // voter for %s\n" % (ioName)
                    for ext in self.EXT:
                        oStr += "  wire %s %s%s %s;\n" % (packed_ranges_str, ioName, ext, unpacked_ranges_str)
                    oStr += "  wire %stmrErr;\n" % (ioName)
                    tmrerrors.append("%stmrErr" % (ioName))

                    width = ""
                    _len = self.global_namespace.modules[module].io[ioName].voter_size()

                    if _len != "1":
                        width += "#(.WIDTH(%s)) " % _len

                    indent = "  "
                    for i, unpacked_range in enumerate(io.unpacked):
                        varname = "gen_%s%d" % (ioName, i)
                        oStr += indent + "genvar %s;\n" % varname

                        _start = "((%s>%s) ? %s : %s )" % (unpacked_range.left, unpacked_range.right, unpacked_range.right, unpacked_range.left)
                        _stop  = "((%s>%s) ? %s : %s )" % (unpacked_range.left, unpacked_range.right, unpacked_range.left, unpacked_range.right)
                        _for   = "%sfor(%s=%s; %s <= %s; %s=%s+1) begin : %s_fanout\n" % (indent, varname, _start, varname, _stop, varname, varname, varname)

                        oStr += _for + "\n"
                        indent += "  "

                    if len(io.unpacked):
                        for i, unpacked_range in enumerate(io.unpacked):
                            varname = "gen_%s%d" % (ioName, i)
                            oStr += indent+"%s %s%s (\n" % (voterCell, width, ioName+"Voter")
                            oStr += indent+"  .inA(%sA[%s]),\n" % (ioName, varname)
                            oStr += indent+"  .inB(%sB[%s]),\n" % (ioName, varname)
                            oStr += indent+"  .inC(%sC[%s]),\n" % (ioName, varname)
                            oStr += indent+"  .out(%s [%s]),\n" % (ioName, varname)
                            oStr += indent+"  .tmrErr(%stmrErr)\n" % (ioName)
                            oStr += indent+");\n"
                    else:
                        oStr += indent+"%s %s%s (\n" % (voterCell, width, ioName+"Voter")
                        oStr += indent+"  .inA(%sA),\n" % (ioName)
                        oStr += indent+"  .inB(%sB),\n" % (ioName)
                        oStr += indent+"  .inC(%sC),\n" % (ioName)
                        oStr += indent+"  .out(%s),\n" % (ioName)
                        oStr += indent+"  .tmrErr(%stmrErr)\n" % (ioName)
                        oStr += indent+");\n"

                    for i in range(len(io.unpacked)):
                        indent = indent[:-2]
                        oStr += "%send\n" % indent

                else:
                    logging.warning("Unsupported IO type: %s" % io.direction)

            # dut tmr instantiation
            oStr += "  %sTMR%s %s (\n" % (module, parameters, self.options.dutName)
            sep = "    "
            for ioName in sorted(self.global_namespace.modules[module].io):
                if self.global_namespace.modules[module].nets[ioName].triplicate:
                    for ext in self.EXT:
                        oStr += "%s.%s%s(%s%s)" % (sep, ioName, ext, ioName, ext)
                        sep = ",\n    "
                else:
                    oStr += "%s.%s(%s)" % (sep, ioName, ioName)
                    sep = ",\n    "
            oStr += "\n  );\n"
            oStr += "`else\n"
            # dut instantiation
            oStr += "  %s%s %s (\n" % (module, parameters, self.options.dutName)
            sep = "    "
            for ioName in sorted(self.global_namespace.modules[module].io):
                oStr += "%s.%s(%s)" % (sep, ioName, ioName)
                sep = ",\n    "
            oStr += "\n  );\n"
            oStr += "`endif\n"

            tmrerr = 'tmr_error'
            oStr += "\n// - - - - - - - - - - - - Optional formal SEU injection section - - - - - - - - - - - - -\n"
            oStr += "`ifdef FRMSVA\n"
            oStr += "`ifdef TMR\n"
            oStr += "`ifdef NETLIST\n"
            for terr in tmrerrors:
                oStr += "  ASSERT_ERROUT_%s : assert property ( @(posedge Clk) %s == 1'b0);\n" % (terr.upper().replace("TMRERR",""), terr)
            oStr += "`include \"seu_assert.sv\"\n"
            oStr += "`endif\n"
            oStr += "`endif\n"
            oStr += "`endif\n"
            oStr += "\n// - - - - - - - - - - - - Timing annotation section - - - - - - - - - - - - -\n"
            oStr += "`ifdef SDF\n"
            oStr += "`ifndef FRMSVA\n"
            oStr += "  initial\n"
            oStr += "    $sdf_annotate(\"r2g.sdf\", DUT, ,\"sdf.log\");\n"
            oStr += "`endif\n"
            oStr += "`endif\n"
            oStr += "\n// - - - - - - - - - - - - Single Event Effect section - - - - - - - - - - - -\n"
            oStr += "`ifdef SEE\n"
            oStr += "`ifndef FRMSVA\n"
            oStr += "  reg     SEEEnable=0;          // enables SEE generator\n"
            oStr += "  reg     SEEActive=0;          // high during any SEE event\n"
            oStr += "  integer SEEnextTime=0;        // time until the next SEE event\n"
            oStr += "  integer SEEduration=0;        // duration of the next SEE event\n"
            oStr += "  integer SEEwireId=0;          // wire to be affected by the next SEE event\n"
            oStr += "  integer SEEmaxWireId=0;       // number of wires in the design which can be affected by SEE event\n"
            oStr += "  integer SEEmaxUpaseTime=1000; // 1 ns  (change if you are using different timescale)\n"
            oStr += "  integer SEEDel=100_000;       // 100 ns (change if you are using different timescale)\n"
            oStr += "  integer SEECounter=0;         // number of simulated SEE events\n"
            oStr += "\n"
            oStr += "  `include \"see.v\"\n"
            oStr += "\n"
            oStr += "  // get number of wires which can be affected by see\n"
            oStr += "  initial\n"
            oStr += "    see_max_net (SEEmaxWireId);\n"
            oStr += "\n"
            oStr += "\n"
            oStr += "  always\n"
            oStr += "    begin\n"
            oStr += "      if (SEEEnable)\n"
            oStr += "        begin\n"
            oStr += "          // randomize time, duration, and wire of the next SEE\n"
            oStr += "          SEEnextTime = #(SEEDel/2) {$random} % SEEDel;\n"
            oStr += "          SEEduration = {$random} % (SEEmaxUpaseTime-1) + 1;  // SEE time is from 1 - MAX_UPSET_TIME ns\n"
            oStr += "          SEEwireId   = {$random} % SEEmaxWireId;\n"
            oStr += "\n"
            oStr += "          // wait for SEE\n"
            oStr += "          #(SEEnextTime);\n"
            oStr += "\n"
            oStr += "          // SEE happens here! Toggle the selected wire.\n"
            oStr += "          SEECounter=SEECounter+1;\n"
            oStr += "          SEEActive=1;\n"
            oStr += "          see_force_net(SEEwireId);\n"
            oStr += "          see_display_net(SEEwireId); // probably you want to comment this line ?\n"
            oStr += "          #(SEEduration);\n"
            oStr += "          see_release_net(SEEwireId);\n"
            oStr += "          SEEActive=0;\n"
            oStr += "        end\n"
            oStr += "      else\n"
            oStr += "        #10;\n"
            oStr += "    end\n"
            oStr += "\n"
            oStr += "`endif\n"
            oStr += "`endif\n"

            # initial
            oStr += "\n// - - - - - - - - - - - - - Actual testbench section  - - - - - - - - - - - -\n"
            oStr += "`ifndef FRMSVA\n"
            oStr += "  initial\n"
            oStr += "    begin\n"
            resetsRelease = ""
            for ioName in sorted(self.global_namespace.modules[module].io):
                io = self.global_namespace.modules[module].io[ioName]
                if io.direction == "input":
                    if ioName in resets:
                        if ioName.find("n") >= 0 or ioName.find("b") >= 0:
                            oStr += "      %s=0;\n" % (ioName)
                            resetsRelease = "      %s=1;\n" % (ioName)
                        else:
                            oStr += "      %s=1;\n" % (ioName)
                            resetsRelease = "      %s=0;\n" % (ioName)
                    else:
                        if not io.unpacked:
                            oStr += "      %s=0;\n" % (ioName)
            if len(resetsRelease):
                oStr += "      #10_000\n"+resetsRelease
            oStr += "`ifdef SEE\n"
            oStr += "      // enable SEE after 1ms\n"
            oStr += "      #1000_000 SEEEnable=1;\n"
            oStr += "      $display(\"Enabling SEE generation\");\n"
            oStr += "`endif\n"
            oStr += "      // finish simulation after 1ms\n"
            oStr += "      #1000_000 $finish;\n"
            oStr += "    end\n"

            oStr += "\n"
            for clock in clocks:
                def guessClkFreq(clkName):
                    try:
                        return int(1e6/float(re.search(r'\d+', clock).group()))
                    except:
                        return 1e5
                oStr += "  localparam %s_PERIOD = %d;\n" % (clock.upper(), guessClkFreq(clock))
                oStr += "  initial\n"
                oStr += "    begin\n"
                oStr += "      #1000;\n"
                oStr += "      forever\n"
                oStr += "        begin\n"
                oStr += "          %s = !%s;\n" % (clock, clock)
                oStr += "          #(%s_PERIOD/2);\n" % (clock.upper())
                oStr += "        end\n"
                oStr += "    end\n"

            oStr += "`endif\n"
            oStr += "endmodule\n"
        if self.options.outputFname != "":
            logging.info("Writing testbench to %s" % self.options.outputFname)
            f = open(self.options.outputFname, "w")
            f.write(oStr)
            f.close()
        else:
            print(oStr)


def main():
    OptionParser.format_epilog = lambda self, formatter: self.epilog
    parser = OptionParser(version="TBG %s" % tmrg_version(), usage="%prog [options] fileName", epilog=epilog)
    parser.add_option("-v", "--verbose",       dest="verbose",   action="count",
                      default=0, help="More verbose output (use: -v, -vv, -vvv..)")
    parser.add_option("",   "--doc",           dest="doc",       action="store_true",
                      default=False, help="Open documentation in web browser")
    parser.add_option("-l", "--lib",           dest="libs",      action="append",   default=[], help="Library")
    parser.add_option("-c",  "--config",           dest="config",
                      action="append",   default=[], help="Load config file")
    parser.add_option("-w",  "--constrain",        dest="constrain",
                      action="append",   default=[], help="Load config file")
    parser.add_option("-o",  "--output-file",      dest="outputFname",    default="", help="Output file name")
    parser.add_option("",  "--generate-report",    dest="generateBugReport",
                      action="store_true",   default=False, help="Generate bug report")
    parser.add_option("", "--stats", dest="stats", action="store_true", help="Print statistics")
    parser.add_option("", "--include", dest="include", action="store_true", default="false",
                      help="Include include files")
    parser.add_option("",   "--inc-dir",           dest="inc_dir",
                      action="append", default=[], help="Include directories")
    parser.add_option("",   "--top-module",        dest="top_module",
                      action="store", default="",  help="Specify top module name")
    parser.add_option("",   "--tb-name",           dest="tbName", default="",
                      help="Testbench name. If not provided the the name will be <module>_test")
    parser.add_option("",   "--dut-name",          dest="dutName", default="DUT", help="DUT name in the testbench")


    logFormatter = logging.Formatter('[%(levelname)-7s] %(message)s')
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.WARNING)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)
    exit_code = 0
    try:
        (options, args) = parser.parse_args()

        if options.verbose == 0:
            logging.getLogger().setLevel(logging.WARNING)
        if options.verbose == 1:
            logging.getLogger().setLevel(logging.INFO)
        elif options.verbose == 2:
            logging.getLogger().setLevel(logging.DEBUG)

        if options.doc:
            startDocumentation()
            return

        if len(args) != 1:
            raise OptParseError("You have to specify verilog file name. ")

        tbg = TBG(options, args)
        tbg.parse()
        tbg.elaborate(allowMissingModules=True)
        tbg.generate()
    except ErrorMessage as er:
        logging.error(er)
        exit_code = 1
    except OptParseError as er:
        logging.error(er)
        exit_code = 2
    rootLogger.handlers = []
    sys.exit(exit_code)

if __name__ == "__main__":
    main()
