#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Copyright (c) CERN and the TMRG authors.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import logging
import traceback
import os
import glob
import logging
import copy
import shutil
import time
import pickle
import ast
import sys
import operator as op
try:
    import ConfigParser as cp
except:
    import configparser as cp
from .verilog_parser import VerilogParser
from .verilog_formatter import VerilogFormatter
from .prettytable import PrettyTable
try:
    from cPyparsing import ParseResults, Literal, ParseException
except:
    from pyparsing import ParseResults, Literal, ParseException

try:
	from functools import lru_cache
except:
	from functools32 import lru_cache

def custom_deepcopy(self):
    @lru_cache(1024)
    def naiveCopy(tokens):
        if isinstance(tokens, ParseResults):
            newtokens=ParseResults(toklist=list(),name=tokens.getName())

            for tok in tokens:
                if isinstance(tok, ParseResults):
                    copied = naiveCopy(tok)
                    newtokens.append(naiveCopy(tok))
                else:
                    newtokens.append(tok)

            for tok_key in list(tokens.keys()):
                tok = tokens[tok_key]
                if isinstance(tok, ParseResults):
                    copied = naiveCopy(tok)
                    newtokens[tok_key] = copied
                else:
                    newtokens[tok_key] = tok

            return newtokens
        else:
            return copy.deepcopy(tokens)

    return naiveCopy(self)

ParseResults.deepcopy = custom_deepcopy

class LinearExpression:
    def __init__(self, value):
        self.to_add = 0
        self.is_int = True

        if value == "" or value is None:
            self.value = 0
            return

        try:
            value_int = int(value)
        except (TypeError, ValueError):
            try:
                self.value = self.reduce(value)
            except (TypeError, SyntaxError):
                try:
                    self.value = str(value)
                except TypeError:
                    raise TypeError("Unable to convert value of type `%s` to string")

                self.is_int = False
        else:
            self.value = value_int

    def __str__(self):
        if self.is_int:
            return str(self.value)

        if self.to_add != 0:
            return "(%s %s %d)" % (self.value, "+" if self.to_add > 0 else "-", abs(self.to_add))

        # Only alpha, can safely return without parentheses
        if self.value.isalpha():
            return self.value

        if self.value[0] == "(" and self.value[-1] == ")" and self.value.count("(", 1, -1) == self.value.count(")", 1, -1):
            return self.value[1:-1]

        return "(" + self.value + ")"

    def __repr__(self):
        return str(self)

    def __int__(self):
        if self.is_int:
            return self.value
        else:
            raise TypeError("Unable to convert `%s` to int!" % self.value)

    def reduce(self, value):
        # supported operators
        operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
                ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor,
                ast.USub: op.neg}

        def eval_(node):
            if isinstance(node, ast.Num): # <number>
                return node.n
            elif isinstance(node, ast.BinOp): # <left> <operator> <right>
                return operators[type(node.op)](eval_(node.left), eval_(node.right))
            elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
                return operators[type(node.op)](eval_(node.operand))
            else:
                raise TypeError(node)

        return eval_(ast.parse(value, mode='eval').body)

    def __iadd__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            self.value += other.value
            return self

        if self.is_int:
            self.to_add = other.to_add + self.value
            self.value = other.value
            self.is_int = False
            return self

        if other.is_int:
            self.to_add += other.value
            return self

        self.value += " + " + other.value
        self.to_add += other.to_add
        return self

    def __add__(self, other):
        copy = LinearExpression(self.value)
        copy.to_add = self.to_add
        copy += other
        return copy

    def __radd__(self, other):
        return self.__add__(other)

    def __isub__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            self.value -= other.value
            return self

        if self.is_int:
            self.to_add = other.to_add - self.value
            self.value = other.value
            self.is_int = False
            return self

        if other.is_int:
            self.to_add -= other.value
            return self

        self.value += " - " + other.value
        self.to_add += other.to_add
        return self

    def __sub__(self, other):
        copy = LinearExpression(self.value)
        copy.to_add = self.to_add
        copy -= other
        return copy

    def __rsub__(self, other):
        return self.__sub__(other)

    def __imul__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int:
            if other.is_int:
                self.value *= other.value
                return self

            if self.value == 1:
                self.value = other.value
                self.to_add = other.to_add
                self.is_int = other.is_int
                return self

        elif other.is_int and other.value == 1:
            return self

        self.value = "( " + str(self) + " * " + str(other) + " )"
        self.to_add = 0
        self.is_int = False

        return self

    def __mul__(self, other):
        copy = LinearExpression(self.value)
        copy.to_add = self.to_add
        copy *= other
        return copy

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            return LinearExpression( self.value / other.value )

        return LinearExpression( "( " + str(self) + " / " + str(other) + " )" )

    def __floordiv__(self, other):
        return self.__truediv__(other)

    def __pow__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            return LinearExpression( self.value ** other.value )

        return LinearExpression( "( " + str(self) + " ** " + str(other) + " )" )

    def __abs__(self):
        if self.is_int:
            return LinearExpression( abs(self.value) )

        self_str = str(self)

        return LinearExpression( "( (" + self_str + ">0) ? " + self_str + " : -" + self_str + " )" )

    def __le__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            return self.value <= other.value

        raise TypeError("Cannot compare non-integer types.")

    def __lt__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            return self.value < other.value

        raise TypeError("Cannot compare non-integer types.")

    def __eq__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        eq_value = self.value == other.value

        if self.is_int and other.is_int:
            return eq_value

        return eq_value and (self.to_add == other.to_add)

    def __ne__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        ne_value = self.value != other.value

        if self.is_int and other.is_int:
            return ne_value

        return ne_value or (self.to_add != other.to_add)

    def __ge__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            return self.value >= other.value

        raise TypeError("Cannot compare non-integer types.")

    def __gt__(self, other):
        if not isinstance(other, LinearExpression):
            other = LinearExpression(other)

        if self.is_int and other.is_int:
            return self.value > other.value

        raise TypeError("Cannot compare non-integer types.")

class File:
    def __init__(self, name, config, tokens=None):
        self.filename = name
        self.config   = config
        self.tokens   = tokens

    def get_out_filename(self):
        file, ext = os.path.splitext(os.path.basename(self.filename))
        fout = os.path.join(self.config.get("tmrg", "tmr_dir"), file + "TMR" + ext)
        return fout

    def get_tmp_filename(self):
        fout = self.get_out_filename()
        return fout + ".new"

class Range:
    OPERATORS = [":", "-:", "+:"]

    def __init__(self):
        self.left = LinearExpression(0)
        self.right = LinearExpression(0)
        self.operator = ":"
        self.range_not_size = True

    def size(self):
        if self.operator != ":":
            return self.right + 1

        return abs(self.left - self.right) + 1

    def __str__(self):
        if self.range_not_size:
            return "[" + str(self.left) + " " + self.operator + " " + str(self.right) + "]"

        return "[" + str(self.size()) + "]"

class Parameter:
    def __init__(self, base_signal=None):
        self.param_type = None
        self.value = None
        self.name = None

        self.modifiers = ""
        self.data_type = None
        self.packed = []
        self.unpacked = []

        if base_signal:
            if not isinstance(base_signal, Signal):
                raise Exception("Base signal provided is not of `Signal` type!")

            self.modifiers = base_signal.modifiers
            self.data_type = base_signal.data_type
            self.packed = base_signal.packed

    def __str__(self):
        if not self.param_type:
            raise Exception("Cannot produce parameter declaration: this instance has no parameter type set!")

        if not self.name:
            raise Exception("Cannot produce parameter declaration: this instance has no name set!")

        t = ""

        if self.data_type:
            t += self.data_type + " "

        if self.modifiers:
            t += self.modifiers + " "

        if self.packed:
            t += "".join([str(p) for p in self.packed]) + " "

        u = ""
        if self.unpacked:
            u = "".join([str(p) for p in self.unpacked])

        if self.value:
            u += " = %s"% self.value

        return "%s %s%s%s;\n" % (self.param_type, t, self.name, u)

class Signal:
    """Class holding properties for a signal

    Attributes:
        packed     : Packed array information. List of `Range` objects.
        unpacked   : Unpacked array information. List of `Range` objects.
        data_type  : Data type (i.e. int, logic, time, etc or custom defined types). String.
        net_type   : Net type (i.e. wire, wor, tri0, etc). String.
        modifiers  : Modifier keywords (i.e. signed, scalared, vectored, etc.). String.
        dont_touch : `True` if the signal is not to be touched, `None` if attribute is to be inherited. Boolean.
        triplicate : `True` if the signal is to be triplicated, `None` if attribute is to be inherited. Boolean.
        direction  : Port direction if the signal is in the module interface (i.e. input, output, inout). String.
    """

    __cache = {}

    def __init__(self, elaborator=None, namespace=None):
        self.name = None

        self.packed = []
        self.unpacked = []
        self.data_type = None
        self.net_type = None
        self.modifiers = None
        self.dont_touch = False
        self.triplicate = None
        self.direction = ""
        self.__voter_size = None

        self.elaborator = elaborator
        self.namespace  = namespace

    def copy(self):
        # Temporarily store references
        elaborator = self.elaborator
        namespace  = self.namespace

        # Remove them from the instance
        self.elaborator = None
        self.namespace  = None

        # Deepcopy without elaborator
        cpy = copy.deepcopy(self)

        # Copy it back into self and the copy
        self.elaborator = elaborator
        self.namespace  = namespace
        cpy.elaborator  = elaborator
        cpy.namespace   = namespace

        return cpy

    def packed_size(self):
        res = LinearExpression(1)
        for i in self.packed:
            res *= i.size()
        return res

    def get_type(self, cache=False):
        return Type.resolve(self.data_type, self.namespace, cache)

    def voter_size(self, cache=False):
        type_size = None

        if cache:
            if self.__voter_size is not None:
                return self.__voter_size

            if (self.data_type, self.namespace) in Signal.__cache:
                type_size = Signal.__cache[(self.data_type, self.namespace)]

        if type_size is None:
            type_ref = self.get_type(cache)

            type_size = LinearExpression(1)
            if self.data_type:
                if type_ref is None:
                    logging.warning("Unable to evaluate the size of type `%s`. Using the `$size` system task." % self.data_type)
                    type_size = LinearExpression("$size(%s)" % self.name)
                elif type_ref.size is False:
                    raise ErrorMessage("Unable to evaluate the size of type `%s`. Cannot proceed." % self.data_type)
                else:
                    type_size = type_ref.size

            if cache:
                Signal.__cache[(self.data_type, self.namespace)] = type_size

        self.__voter_size = type_size * self.packed_size()

        return self.__voter_size

    def full_type(self):
        if self.net_type:
            if self.data_type and self.data_type not in ["reg", "logic"]:
                if self.modifiers:
                    return self.net_type + " " + self.data_type + " " + self.modifiers
                else:
                    return self.net_type + " " + self.data_type
            else:
                if self.modifiers:
                    return self.net_type + " " + self.modifiers
                else:
                    return self.net_type
        else:
            if self.data_type:
                if self.modifiers:
                    return self.data_type + " " + self.modifiers
                else:
                    return self.data_type
            else:
                if self.modifiers:
                    return "wire " + self.modifiers
                else:
                    return "wire"

class Namespace(object):
    __slots__ = 'name', 'types', 'params', 'functions_tasks'

    def __init__(self):
        self.types = {}
        self.params = {}
        self.functions_tasks = {}

    def __getattr__(self, name):
        if name not in self.__slots__:
            raise ErrorMessage('Namespace of type %s cannot contain %s elements.' % (self.__class__.__name__, name))

    def full_name(self):
        return self.__class__.__name__.lower() + " " + self.name

    def import_from_package(self, pkg, item_name="*"):
        if item_name == "*":
            self.types.update(pkg.types)
            self.params.update(pkg.params)

        else:
            if item_name in pkg.types:
                self.types[item_name] = pkg.types[item_name]

            elif item_name in pkg.params:
                self.params[item_name] = pkg.params[item_name]

            else:
                raise ErrorMessage("%s tried to import non-existent item %s from package %s." % (self.full_name().capitalize(), item_name, pkg.name))

class GlobalNamespace(Namespace):
    __slots__ = 'packages', 'modules'
    __self = None

    def __init__(self):
        Namespace.__init__(self)

        self.packages = {}
        self.modules = {}

        GlobalNamespace.__self = self

    def full_name(self):
        return 'global'

    @staticmethod
    def get():
        if GlobalNamespace.__self:
            return GlobalNamespace.__self
        
        return GlobalNamespace()

class Package(Namespace):
    __slots__ = 'name'

    def __init__(self, name):
        Namespace.__init__(self)

        self.name = name

class Instance(object):
    __slots__ = 'module_name', 'packed', 'size', 'triplicate', 'dont_touch'

    def __init__(self):
        self.module_name = ""
        self.packed = []
        self.size = 0
        self.triplicate = None
        self.dont_touch = None

class Modport(object):
    __slots__ = 'signals', 'interface'

    def __init__(self, interface = None):
        self.signals = {}
        self.interface = interface

    def get_inputs(self):
        return [i for i in self.signals if i.direction == "input"]

    def get_outputs(self):
        return [i for i in self.signals if i.direction == "output"]

    def get_inouts(self):
        return [i for i in self.signals if i.direction == "inout"]

class Type(object):
    __builtin_sizes = {
        "bit"       : 1,
        "byte"      : 8,
        "shortint"  : 16,
        "int"       : 32,
        "longint"   : 64,
        "reg"       : 1,
        "logic"     : 1,
        "integer"   : 32,
        "time"      : 64,
        "shortreal" : False,
        "real"      : False,
        "realtime"  : False
    }

    __builtin = {}
    __cache = {}

    def __init__(self, size=None, builtin=False):
        self.size = LinearExpression(0) if size is None else size
        self.__is_builtin = builtin
        self.is_builtin = self._instance_is_builtin

    @staticmethod
    def is_builtin(name): # pylint: disable=method-hidden
        return name in Type.__builtin_sizes

    def _instance_is_builtin(self):
        return self.__is_builtin

    @staticmethod
    def resolve(name, namespace, cache=False):
        if not name:
            return None

        if Type.is_builtin(name):
            if name not in Type.__builtin:
                Type.__builtin[name] = Type(Type.__builtin_sizes[name], True)

            return Type.__builtin[name]

        if Type.__cache and (name, namespace) in Type.__cache:
            return Type.__cache[(name, namespace)]

        try:
            (type_package, type_name) = name.split("::")

        # There is no namespace reference in the type name
        except ValueError:
            if not name in namespace.types:
                logging.warning(
                    "Referencing type `%s`, which has not been declared in `%s`'s namespace. Available types: `%s`." % (
                    name, namespace.full_name(),  ", ".join(sorted(list(namespace.types.keys()))))
                )
                Type.__cache[(name, namespace)] = None
            else:
                Type.__cache[(name, namespace)] = namespace.types[name]

        # We're referencing a type from a package
        else:
            packages = GlobalNamespace.get().packages
            if not type_package in packages:
                logging.warning("Referencing type `%s` from package `%s`, which has not been declared. Available packages: `%s`." % (
                    type_name, type_package, ", ".join(sorted(list(packages.keys()))))
                )
                Type.__cache[(name, namespace)] = None
            else:
                pkg = packages[type_package]
                if not type_name in pkg.types:
                    raise ErrorMessage("Referencing type `%s` which has not been declared in package `%s`. Available types in the package: `%s`." % (name, pkg.name, ", ".join(sorted(list(pkg.types.keys())))))

                Type.__cache[(name, namespace)] = pkg.types[type_name]

        return Type.__cache[(name, namespace)]

class Interface(Namespace, Type):
    __slots__ = 'modports', 'signals', 'default_tmr', 'size'

    def __init__(self, name):
        Type.__init__(self)
        self.modports = {}
        self.signals = {}
        self.default_tmr = False
        self.name = name

class Structure(Type):
    def __init__(self):
        Type.__init__(self)
        self.fields = {}

class Union(Type):
    def __init__(self):
        Type.__init__(self)
        self.fields = {}

class Enumeration(Type):
    def __init__(self):
        Type.__init__(self)
        self.fields = []
        self.size = LinearExpression(32)

class FunctionOrTask:
    __slots__ = 'name', 'inputs', 'outputs', 'arguments'

    def __init__(self, name = None):
        self.name = name
        self.arguments = []
        self.inputs = []
        self.outputs = []
    
    def add_port(self, name, direction):
        self.arguments.append(name)

        if direction == "input":
            self.inputs.append(name)
        elif direction == "output":
            self.outputs.append(name)
        else:
            logging.error("Function and task declarations currently not supporting inouts")

    @staticmethod
    def resolve(name, namespace):
        packages = GlobalNamespace.get().packages
        if not namespace in packages:
            logging.warning("Referencing function or task `%s` from package `%s`, which has not been declared. Available packages: `%s`." % (
                name, namespace, ", ".join(sorted(list(packages.keys()))))
            )
            return False

        pkg = packages[namespace]
        if not name in pkg.functions_tasks:
            raise ErrorMessage("Referencing function or task `%s` which has not been declared in package `%s`. Available ones in the package: `%s`." % (name, pkg.name, ", ".join(sorted(list(pkg.functions_tasks.keys())))))

        return pkg.functions_tasks[name]

class Module(Namespace):
    __slots__ = 'name', 'file', 'is_lib', 'instances', 'nets', 'io', \
                'portMode', 'constraints', 'instantiated', 'fanouts', 'voters', \
                'tmrErrNets', 'genvars', 'local_variables'

    def __init__(self, name = None, file = None):
        Namespace.__init__(self)

        self.name = name
        self.file = file
        self.is_lib  = False

        self.instances = []
        self.nets = {}
        self.genvars = []
        self.local_variables = []

        self.io = {}
        self.portMode = "non-ANSI"

        self.constraints = {"dnt" : False}
        self.instantiated = 0

        self.fanouts = {}
        self.voters = {}
        self.tmrErrNets = {}

class ErrorMessage(BaseException):
    def __init__(self, s):
        BaseException.__init__(self, s)

def readFile(fname):
    if os.path.isfile(fname):
        f = open(fname, "r")
        body = f.read()
        f.close()
        return body
    else:
        logging.error("File '%s' does not exists" % fname)
        return ""

def resultLine(tokens, sep=""):
    s = ""
    if isinstance(tokens, ParseResults):
        for i in tokens:
            s += resultLine(i)+sep
    else:
        s += tokens
    return s

class VerilogElaborator():
    NET_TYPES = ["wire", "tri", "tri0", "tri1", "supply0", "supply1", "wand", "wor", "triand", "trior", "trireg"]
    DATA_TYPES = ["bit", "byte", "shortint", "int", "longint", "reg", "logic", "integer", "time", "shortreal", "real", "realtime"]

    def __init__(self, options, args, cnfgName):
        self.options = options
        self.args = args

        if hasattr(options, "comments"):
            VerilogParser.comments = options.comments

        if hasattr(options, "pragmas"):
            VerilogParser.extra_pragmas = [x.strip() for x in options.pragmas.replace('"', '').split(",") if x]
            logging.info("Custom pragmas defines: " + ", ".join(VerilogParser.extra_pragmas))

        self.vp = VerilogParser()
        self.statsLogs = []
        self.statsFilesParsed = 0
        self.vp.include = options.include
        self.vp.inc_dir = options.inc_dir

        self.vf = VerilogFormatter()
        self.libFiles = []

        self.EXT = ('A', 'B', 'C')

        self.global_namespace = GlobalNamespace()
        self.current_namespace = self.global_namespace

        self.files = []
        self.libs = []
        self.__init_elaborate_callbacks()

        self.trace = True

        self.config = cp.ConfigParser()
        self.scriptDir = os.path.abspath(os.path.dirname(__file__))
        logging.debug("Script path : %s" % self.scriptDir)

        # master clonfig file
        masterCnfg = os.path.join(self.scriptDir, "../etc/%s.cfg" % cnfgName)
        if os.path.exists(masterCnfg):
            logging.debug("Loading master config file from %s" % masterCnfg)
            self.config.read(masterCnfg)
            if self.options.generateBugReport:
                fcopy = os.path.join(self.options.bugReportDir, "master.cfg")
                logging.debug("Coping master config file from '%s' to '%s'" % (masterCnfg, fcopy))
                shutil.copyfile(masterCnfg, fcopy)

        else:
            logging.warning("Master config file does not exists at '%s'" % masterCnfg)

        # user config file
        userCnfg = os.path.expanduser('~/.%s.cfg' % cnfgName)
        if os.path.exists(userCnfg):
            logging.debug("Loading user config file from %s" % userCnfg)
            self.config.read(userCnfg)
            if self.options.generateBugReport:
                fcopy = os.path.join(self.options.bugReportDir, "user.cfg")
                logging.debug("Coping user config file from '%s' to '%s'" % (userCnfg, fcopy))
                shutil.copyfile(userCnfg, fcopy)
        else:
            logging.info("User config file does not exists at '%s'" % userCnfg)
        self.translate = True
        self.linesTotal = 0

        # Populating std namespace
        std = Package("std")
        randomize = FunctionOrTask("randomize")
        randomize.add_port("net", "output")
        std.functions_tasks["randomize"] = randomize
        self.global_namespace.packages["std"] = std

    def __init_elaborate_callbacks(self):
        # scan class looking for elaborator functions
        self.elaborator = {}
        for member in dir(self):
            if member.find("_elaborate_") == 0:
                token = member[len("_elaborate_"):].lower()
                self.elaborator[token] = getattr(self, member)
                logging.debug("Found elaborator for %s" % token)

    def getLeftRightHandSide(self, t, res=None):
        def _extractID(t, res=None):
            if res == None:
                res = set()
            if isinstance(t, ParseResults):
                if t.getName() == "reg_reference":
                    # Perform some basic checks
                    self.__check_reg_reference(t)

                    fullname = t.get("name")[0]
                    netname, netfields = self.split_name(fullname)

                    if not netname in self.current_namespace.nets:
                        if netname[0] == '`':
                            logging.debug("%s is a macro. Leaving it untouched." % fullname)

                        elif fullname in self.current_namespace.params:
                            logging.debug("%s is a parameter. Leaving it untouched." % fullname)

                        elif netname in self.current_namespace.genvars:
                            logging.debug("%s is a genvar. Leaving it untouched." % fullname)

                        elif netname in self.current_namespace.local_variables:
                            logging.debug("%s is a local variable. Leaving it untouched." % fullname)

                        elif self._is_enum_element(fullname):
                            logging.debug("%s is an enumeration element. Leaving it untouched." % fullname)

                        elif netname in [name for (name, _) in self.current_namespace.instances]:
                            logging.debug("%s is an instance name. Assuming %s is a hierarchical reference. Leaving it untouched." % (netname, fullname))

                        else:
                            logging.info("%s cannot be recognized as neither macro, parameter, enumeration element, nor hierarchical reference. Leaving it untouched." % fullname)

                        return res

                    if not self.current_namespace.nets[netname].dont_touch:
                        res.add(netname)
                    _extractID(t.get("range_or_field"), res=res)

                else:
                    for i in range(len(t)):
                        res = _extractID(t[i], res=res)
            return res

        # Initialize if needed
        if res == None:
            res = {"left": set(), "right": set()}

        # Return empty results if not a ParseResults instance
        if not isinstance(t, ParseResults):
            return res

        # Return empty results if there are no tokens
        if len(t) == 0:
            return res

        name = str(t.getName()).lower()

        # Assignments
        if name in ("assignment", "nbassignment"):
            lvalue = t.get("lvalue")[0]
            if lvalue.getName() == "reg_reference":
                res["left"].add(lvalue.get("name")[0])
                res["right"].update(_extractID(t.get("expr@rvalue")))
            elif lvalue.getName() == "concat":
                for i in lvalue:
                    res["left"].update(_extractID(i))
                res["right"].update(_extractID(t.get("expr@rvalue")))
            else:
                logging.error("Unsupported syntax : %s on left hand side of the assignment." % lvalue.getName())
                logging.error("Output may be incorrect.")

        # Register declaration
        elif name in ("netdecl"):
            for tt in t.get("identifiers"):
                res["left"].add(tt.get("name")[0])

        # Register declaration w/ assignment
        elif name in ("netdeclwassign"):
            for tt in t.get("assignments"):
                lvalue = tt.get("lvalue")[0]
                if lvalue.getName() == "reg_reference":
                    res["left"].add(lvalue.get("name")[0])
                    res["right"].update(_extractID(tt.get("expr@rvalue")))

        # Register reference
        elif name == "reg_reference":
            if t.get("name")[0] in self.current_namespace.nets:
                res["right"].add(t.get("name")[0])
                res = self.getLeftRightHandSide(t.get("range_or_field"), res=res)
            else:
                pass

        elif name in ["genvardeclwassign"]:
            res["left"].add(t[1][0])

        elif name == "funcortaskcall":
            func = None
            if "namespace" in t:
                func = FunctionOrTask.resolve(t.get("name")[0], t.get("namespace")[0])
            else:
                try:
                    func = self.current_namespace.functions_tasks[t.get("name")[0]]
                except KeyError:
                    logging.debug("Cannot find function or task %s. Treating all arguments as inputs.", t.get("name")[0])

            if "arguments" in t:
                for idx, arg in enumerate(t.get("arguments")):
                    arg = arg[0]
                    if not isinstance(arg, ParseResults) or arg.getName() != "reg_reference":
                        logging.debug("Argument %s to function %s is not a reg_reference. Ignoring it.", self.vf.format(arg), t.get("name")[0])
                        continue

                    # Inherently treating inouts as inputs
                    dir = "right"
                    if func is not None and func.arguments[idx] in func.outputs:
                        dir = "left"

                    if arg.get("name")[0] in self.current_namespace.nets:
                        res[dir].add(arg.get("name")[0])

        elif name == "genvardecl":
            for tt in t.get("names"):
                res["left"].add(tt[0][0])

        # All else
        else:
            for i in range(len(t)):
                res = self.getLeftRightHandSide(t[i], res=res)
        return res

    def __packagedecl(self, tokens):
        res = Package(tokens.get("id@packageName")[0])

        prev_namespace = self.current_namespace
        self.current_namespace = res

        # Importing global packages and types
        self.current_namespace.types = self.global_namespace.types.copy()
        for pkg in self.global_namespace.packages:
            for p in self.global_namespace.packages[pkg].params:
                self.current_namespace.params[pkg + "::" + p] = self.global_namespace.packages[pkg].params[p]

        for item in tokens.get("packageBody"):
            if item.getName() == "structDecl":
                res.types[item.get("id")[0]] = self.__structdecl(item)

            elif item.getName() == "enumDecl":
                res.types[item.get("id")[0]] = self.__enumdecl(item)

            elif item.getName() == "typedefDecl":
                res.types[item.get("id")[0]] = self.__typedefdecl(item)

            elif item.getName() == "unionDecl":
                res.types[item.get("id")[0]] = self.__uniondecl(item)

            elif item.getName() == "paramDecl":
                res.params.update(self.__paramdecl(item))

            elif item.getName() == "package_import":
                self._elaborate_package_import(item)

        self.current_namespace = prev_namespace

        return res

    def __enumdecl(self, tokens):
        fields = tokens.get("fields")
        res = Enumeration()

        # Size
        res.size = LinearExpression(1)
        _type = tokens.get("type")

        packed_ranges = self.__elaborate_ranges(_type.get("packed_ranges"))
        for prange in packed_ranges:
            res.size *= prange.size()

        for field in fields:
            res.fields.append(field.get("name")[0])

        return res

    def __typedefdecl(self, tokens):
        res = Type()
        _type = tokens.get("type")

        if "standard_type" in _type:
            res.size = LinearExpression(1)
        else:
            type_name = "".join(_type.get("custom_type"))
            if type_name in self.current_namespace.types:
                res.size = self.current_namespace.types[type_name].size

        packed_ranges = self.__elaborate_ranges(_type.get("packed_ranges"))
        for prange in packed_ranges:
            res.size *= prange.size()

        return res

    def __modport(self, tokens, interface):
        name = tokens.get("name")[0]
        m = Modport(name)
        for p in tokens.get("ports"):
            for s in p.get("signals"):
                s = s[0]
                if s not in interface.signals:
                    raise ErrorMessage("Signal %s, found in modport %s, is not present in the interface" % (s, name))

                interface.signals[s].direction = p.get("dir")
                m.signals[name] = interface.signals[s]

        return m

    def __interfacedecl(self, tokens):
        elems = tokens.get("elems")

        res = Interface(tokens.get("id")[0])
        tmr = {}
        for elem in elems:
            if elem.getName() == "signal":
                elem = elem.get("netDecl")
                base_sig = self.__elaborate_signal_declaration(elem)

                for reg in elem.get("identifiers"):
                    sig = base_sig.copy()
                    sig.name = reg.get("name")[0]
                    sig.unpacked = self.__elaborate_ranges(reg.get("unpacked_ranges"))

                    res.signals[reg.get("name")[0]] = sig
                    res.size += sig.voter_size()

            elif elem.getName() == "modport":
                res.modports[elem.get("name")[0]] = self.__modport(elem, res)

            elif elem.getName() == "directive_default":
                res.default_tmr = elem[0].lower() == 'triplicate'

            elif elem.getName() == "directive_triplicate":
                for net in elem:
                    tmr[net] = True

            elif elem.getName() == "directive_do_not_triplicate":
                for net in elem:
                    tmr[net] = False

            else:
                raise ErrorMessage("Found unsupported `%s` in interface declaration." % elem.getName())

        for net in tmr:
            if net not in res.signals:
                continue

            res.signals[net].triplicate = tmr[net]
        return res

    def __elaborate_signal_declaration(self, tokens):
        sig = Signal(self, self.current_namespace)

        sig.modifiers = tokens.get("modifiers") or None
        sig.packed = self.__elaborate_ranges(tokens.get("packed_ranges"))

        if "net_type" in tokens:
            sig.net_type = tokens.get("net_type")[0]

        if "standard_type" in tokens:
            sig.data_type = tokens.get("standard_type")[0]
        elif "custom_type" in tokens:
            sig.data_type = "".join(tokens.get("custom_type"))

        return sig

    def __structdecl(self, tokens):
        fields = tokens.get("fields")

        res = Structure()

        res.fields = {}
        res.size = LinearExpression(0)
        for field in fields:
            base_sig = self.__elaborate_signal_declaration(field)

            for reg in field.get("identifiers"):
                sig = base_sig.copy()

                name = reg.get("name")[0]
                res.fields[name] = sig
                sig.name = name
                sig.unpacked = self.__elaborate_ranges(reg.get("unpacked_ranges"))
                res.size += sig.voter_size()

        return res

    def __uniondecl(self, tokens):
        fields = tokens.get("fields")

        res = Union()

        res.fields = {}
        res.size = None
        for field in fields:
            base_sig = self.__elaborate_signal_declaration(field)

            for reg in field.get("identifiers"):
                sig = base_sig.copy()

                name = reg.get("name")[0]
                res.fields[name] = sig
                sig.name = name
                sig.unpacked = self.__elaborate_ranges(reg.get("unpacked_ranges"))

                if res.size is None:
                    res.size = sig.voter_size()
                elif not res.size.is_int or not sig.voter_size().is_int:
                    logging.info("Skipping union size check for union %s field %s: one of the fields of the union is not an integer", tokens.get("id")[0], name)
                elif res.size != sig.voter_size():
                    raise ErrorMessage(
                        "Field %s (type: %s) of union %s has size %s. Expecting it to be %s." %
                        (name, base_sig.full_type() + "".join(sig.unpacked), tokens.get("id")[0], sig.voter_size(), res.size)
                    )

        return res

    def _elaborate_functiondecl(self, tokens):
        func = FunctionOrTask(tokens.get("name")[0])

        if "ports" in tokens:
            for item in tokens.get("ports"):
                key = "typed" if "typed" in item else "untyped"
                direction   = item.get("dir") or None
                identifiers = item.get(key).get("identifiers")

                for identifier in identifiers:
                    func.add_port(identifier.get("name")[0], direction)

        for item in tokens.get("tfDecl"):
            if item.getName() != "portBody":
                continue

            sub = "typed" if "typed" in item else "untyped"
            names = item.get(sub).get("identifiers")
            names = [n.get("name")[0] for n in names]

            for n in names:
                func.add_port(n, item.get("dir"))

        self.current_namespace.functions_tasks[func.name] = func

    def _elaborate_task(self, tokens):
        self._elaborate_functiondecl(tokens)

    def _elaborate_standalonedelay(self, tokens):
        self._elaborate_delay(tokens)

    def _elaborate_delay(self, tokens):
        tocheck = []
        if "alphanum" in tokens[1]:
            if tokens[1].get("alphanum").startswith("`"):
                return

            tocheck.append(tokens[1].get("alphanum"))
        elif "expr" in tokens[1]:
            res = self.getLeftRightHandSide(tokens[1])
            tocheck = res["right"]
        else:
            return

        for symbol in tocheck:
            if symbol in self.current_namespace.params:
                continue

            if symbol in self.current_namespace.nets:
                if not symbol in self.current_namespace.constraints:
                    logging.warning("Variable %s is in a delay expression. Marking it as do_not_triplicate." % symbol)
                    self.current_namespace.constraints[symbol] = False
                elif self.current_namespace.constraints[symbol]:
                    logging.warning("Variable %s is in a delay expression, but is to be triplicated! This is not supported." % symbol)
            else:
                logging.warning("Delay expression %s references unknown symbol %s." % (tokens[1], symbol))

    def _elaborate_enumdecl(self, tokens):
        self.current_namespace.types[tokens.get("id")[0]] = self.__enumdecl(tokens)

    def _elaborate_structdecl(self, tokens):
        self.current_namespace.types[tokens.get("id")[0]] = self.__structdecl(tokens)

    def _elaborate_typedefdecl(self, tokens):
        self.current_namespace.types[tokens.get("id")[0]] = self.__typedefdecl(tokens)

    def _elaborate_uniondecl(self, tokens):
        self.current_namespace.types[tokens.get("id")[0]] = self.__uniondecl(tokens)

    def _elaborate_netdecl(self, tokens):
        base_sig = self.__elaborate_signal_declaration(tokens)

        for identifier in tokens.get("identifiers"):
            name = identifier.get("name")[0]
            sig = base_sig.copy()
            sig.name = name
            sig.unpacked = self.__elaborate_ranges(identifier.get("unpacked_ranges"))
            sig.dont_touch = False

            if name in self.current_namespace.genvars:
                raise ErrorMessage("Found the net declaration of `%s`, but there is already a genvar with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            if name in self.current_namespace.local_variables:
                raise ErrorMessage("Found the net declaration of `%s`, but there is already a local variable with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            # Is redefinition?
            if name in self.current_namespace.nets:
                logging.debug("Detected redefinition of net: %s" % name)
                logging.debug("Please note that TMRG doesn't distinguish redefinitions in `ifdef constructs, nor treats inline net assignments as different namespaces. Thus, this might be legal.")
                continue

            # Is enum element?
            enum_name = self._is_enum_element(name)
            if enum_name:
                logging.error("Symbol %s is already an element of enumeration %s." % (name, enum_name))

            # Phew! We can add it safely to the list of nets
            self.current_namespace.nets[name] = sig

    def _elaborate_moduleinstantiation(self, tokens):
        identifier = tokens.get("moduleName")[0]
        instances = [i[0][0] for i in tokens.get("moduleInstances")]
        _range = ""

        for inst in instances:
            i = Instance()
            i.module_name = identifier
            i.packed = _range
            self.current_namespace.instances.append( (inst, i) )

    def __check_reg_reference(self, tokens):
        # Check for errors
        netname = tokens.get("name")[0]

        if netname not in self.current_namespace.nets:
            return

        current_sig = self.current_namespace.nets[netname]
        chain = netname
        for field in tokens.get("range_or_field"):
            # We're only checking that fields exist here
            if field.getName() != "field_ref":
                chain += self.vf.format(field)
                continue

            type_ref = current_sig.get_type()
            if not type_ref:
                continue

            if type_ref.is_builtin():
                raise ErrorMessage("Referencing field `%s` of signal `%s`, whose type `%s` is neither a structure nor a union." % (field[0], chain, current_sig.data_type))

            if current_sig.data_type not in current_sig.namespace.types:
                logging.info("Referencing field `%s` of signal `%s`, whose type `%s` has not been declared. Skipping checks.", field[0], chain, current_sig.data_type)
                break

            if not isinstance(current_sig.namespace.types[current_sig.data_type], Structure) and not isinstance(current_sig.namespace.types[current_sig.data_type], Union):
                raise ErrorMessage("Referencing field `%s` of signal `%s`, whose type `%s` is neither a structure nor a union." % (field[0], chain, current_sig.data_type))

            available_fields = current_sig.namespace.types[current_sig.data_type].fields
            if field[0] not in available_fields:
                raise ErrorMessage("Referencing field `%s` of signal `%s`, whose structure/union `%s` doesn't contain a field with that name. Available fields are: %s." % (field[0], chain, current_sig.data_type, ", ".join(list(available_fields.keys()))))
            
            current_sig = available_fields[field[0]]
            chain += self.vf.format(field)

    def _elaborate_reg_reference(self, tokens):
        self.__check_reg_reference(tokens)

    def _elaborate_declarations(self, tokens):
        pass

    def _elaborate_genvardecl(self, tokens):
        ids = self.getLeftRightHandSide(tokens)
        for name in ids["left"]:
            if name in self.current_namespace.nets:
                raise ErrorMessage("Found a genvar called `%s`, but there is already a net with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            self.current_namespace.genvars.append(name)

    def _elaborate_generate_for_statement(self, tokens):
        genvar = tokens.get("for1")[0]
        ids = self.getLeftRightHandSide(genvar)

        for name in ids["left"]:
            if name in self.current_namespace.nets:
                raise ErrorMessage("Found a genvar called `%s`, but there is already a net with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            self.current_namespace.genvars.append(name)

        self._elaborate(tokens.get("generateStmt"))

    def _elaborate_generate_if_else_statement(self, tokens):
        self._elaborate(tokens.get("if_stmt"))

        if "else_stmt" in tokens:
            self._elaborate(tokens.get("else_stmt"))
    
    def _elaborate_generate_case_statement(self, tokens):
        for case_item in tokens.get("case_items"):
            self._elaborate(case_item.get("case_item_body")[0])

    def _elaborate_always(self, tokens):
        self._elaborate(tokens.get("stmt")[0])

    def _elaborate_beginend(self, tokens):
        ids = self.getLeftRightHandSide(tokens.get("declarations"))
        for name in ids["left"]:
            if name in self.current_namespace.nets:
                raise ErrorMessage("Found a local variable called `%s`, but there is already a net with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            self.current_namespace.local_variables.append(name)

        for k in tokens.get("statements"):
            self._elaborate(k)

    def _elaborate_forstmt(self, tokens):
        decl = tokens.get("for1")[0]

        if decl.getName() != "netDeclWAssign":
            return

        for a in decl.get("assignments"):
            name = a.get("lvalue")[0][0][0]
            if name in self.current_namespace.nets:
                raise ErrorMessage("Found a local variable called `%s`, but there is already a net with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            self.current_namespace.local_variables.append(name)

        self._elaborate(tokens.get("stmt")[0])

    def __elaborate_ranges(self, tokens):
        ranges = []

        if not tokens:
            return []

        for r in tokens:
            if r.getName() == "range":
                range = Range()
                range.operator  = r.get("dir")[0] ;# First character out of ": -: +:" is ": - +"
                range.left = LinearExpression(self.vf.format(r.get("expr@from")))
                range.right   = LinearExpression(self.vf.format(r.get("expr@to")))

                ranges.append(range)
            elif r.getName() == "size":
                _len = LinearExpression(self.vf.format(r[0]))

                range = Range()
                range.left = 0
                range.right = _len - 1
                range.operator = ":"
                range.range_not_size = False

                ranges.append(range)

            else:
                raise ErrorMessage("Unknown range type: %s" % r.getName())

        return ranges

    def __elaborate_generic_port(self, tokens):
        key = "typed" if "typed" in tokens else "untyped"

        sig_base = self.__elaborate_signal_declaration(tokens.get(key))
        sig_base.direction = tokens.get("dir") or None

        identifiers = tokens.get(key).get("identifiers")

        for identifier in identifiers:
            name = identifier.get("name")[0]
            sig = sig_base.copy()
            sig.name = name
            sig.unpacked = self.__elaborate_ranges(identifier.get("unpacked_ranges"))

            self.lastANSIPort = sig

            if not name in self.current_namespace.io:
                self.current_namespace.io[name] = sig

            if not name in self.current_namespace.nets:
                self.current_namespace.nets[name] = sig
                logging.debug("generic_port: Adding net %s: %s" % (name, self.current_namespace.nets[name]))

    def _elaborate_porthdr(self, tokens):
        if self.current_namespace.portMode == "non-ANSI":
            self.current_namespace.portMode = "ANSI"
            logging.info("Port mode : ANSI")

        self.__elaborate_generic_port(tokens)

    def _elaborate_portbody(self, tokens):
        self.__elaborate_generic_port(tokens)

    def _elaborate_portnotansi(self, tokens):
        if not self.current_namespace.portMode == "ANSI":
            return

        # Net declaration is only performed when ports are in ANSI mode.
        # Otherwise they are declared later in the module mody.

        name = tokens.get("identifier").get("name")[0]

        if not name in self.current_namespace.nets:
            self.current_namespace.io[name] = self.lastANSIPort["io"].copy()

        if not name in self.current_namespace.nets:
            self.current_namespace.nets[name] = self.lastANSIPort["net"].copy()
            logging.debug("port: Adding net: %s" % self.current_namespace.nets[name])

    def _elaborate_portnotansilist(self, tokens):
        if not self.current_namespace.portMode == "ANSI":
            return

        for i in tokens:
            self._elaborate(i)

    def __paramdecl(self, tokens):
        param_type = tokens.get("param_type")[0] if "param_type" in tokens else "parameter"

        base_sig = self.__elaborate_signal_declaration(tokens)
        res = {}

        for param in tokens.get("params"):
            p = Parameter(base_sig)
            p.param_type = param_type
            p.unpacked = self.__elaborate_ranges(param.get("range_or_field"))
            p.value = self.vf.format(param.get("expr@rvalue"))
            p.name = param.get("lvalue")[0].get("name")[0]

            res[p.name] = p

        return res

    def _elaborate_paramdecl(self, tokens):
        self.current_namespace.params.update(self.__paramdecl(tokens))

    def _is_enum_element(self, name):
        _types = self.current_namespace.types

        for t in _types:
            if not isinstance(_types[t], Enumeration):
                continue

            if name in _types[t].fields:
                return t

        return False

    def _elaborate_netDeclWAssign(self, tokens):
        sig_base = self.__elaborate_signal_declaration(tokens)

        for assignment in tokens.get("assignments"):
            lvalue = assignment.get("lvalue")[0]
            if lvalue.getName() != "reg_reference":
                raise ErrorMessage("Net declarations with assignment don't support tokens of type %s on the LHS." % lvalue.getName())

            name = lvalue.get("name")[0]
            sig = sig_base.copy()
            sig.name = name
            sig.dont_touch = False

            if name in self.current_namespace.genvars:
                raise ErrorMessage("Found the net declaration of `%s`, but there is already a genvar with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            if name in self.current_namespace.local_variables:
                raise ErrorMessage("Found the net declaration of `%s`, but there is already a local variable with the same name in module `%s`!. This is currently not supported." % (name, self.current_namespace.name))

            # Is redefinition?
            if name in self.current_namespace.nets:
                logging.debug("Detected redefinition of net: %s" % name)
                logging.debug("Please note that TMRG doesn't distinguish redefinitions in `ifdef constructs, nor treats inline net assignments as different namespaces. Thus, this might be legal.")
                continue

            # Is enum element?
            enum_name = self._is_enum_element(name)
            if enum_name:
                logging.error("Symbol %s is already an element of enumeration %s." % (name, enum_name))

            # Are we doing an illegal unpacked array assignment?
            unpacked = []
            if assignment.get("lvalue")[0].getName() == "reg_reference":
                range_or_field = assignment.get("lvalue")[0].get("range_or_field")
                if range_or_field:
                    logging.warning("Unpacked net declaration with assignment is not SV-compliant. TMR will still work but non-triplicated simulations may not.")
                    sig.unpacked = self.__elaborate_ranges(range_or_field)

            rhs = assignment.get("expr@rvalue")
            rhs_reg = len(rhs) == 1 and isinstance(rhs[0], ParseResults) and rhs[0].getName() == "reg_reference"
            rhs_val = rhs[0].get("name")[0] if rhs_reg else self.vf.format(rhs)

            # Is this a fanin net?
            if name.endswith(self.EXT):
                if not rhs_reg:
                    logging.info("Detected fanin assignment `%s` doesn't have a net reference on the right hand side: `%s`. Not going to be treated as fanin." % (name, rhs_val))

                elif name[0:-1] != rhs_val:
                    logging.info("Detected fanin assignment `%s` doesn't match the right hand side net `%s`! Not going to be treated as fanin." % (name, rhs_val))

                logging.debug("Net %s is a fanin of %s. Marking it as don't touch." % (name, rhs_val))
                sig.dont_touch = True

            # Is this a Voted net?
            elif name.endswith("Voted"):
                if not rhs_reg:
                    logging.warning("Detected voted assignment `%s` doesn't have a net reference on the right hand side: `%s`" % (name, rhs_val))

                elif name[0:-len("Voted")] != rhs_val:
                    logging.warning("Detected voted assignment `%s` doesn't match the right hand side net `%s`!" % (name, rhs_val))

            # Phew! All is well. Continue!
            self.current_namespace.nets[name] = sig

    def _elaborate_package_import(self, tokens):
        for pkg in tokens.get("packages"):
            pkg_name = pkg.get("id@packageName")[0]
            item_name = pkg.get("id@itemName")[0]

            # If the package has not been declared, log the info
            if pkg_name not in self.global_namespace.packages:
                logging.info("Module %s tried to import %s from package %s, which has not been declared before." % (self.current_namespace.name, item_name, pkg_name))
                return

            # If the package has been declared, proceed with the import
            self.current_namespace.import_from_package(self.global_namespace.packages[pkg_name], item_name)

    def _elaborate_comment(self, tokens):
        words = tokens.get("content").split()

        if len(words) < 2 or words[0].lower() != "tmrg":
            logging.debug("Comment is not tmrg directive: %s" % words)
            return tokens
        
        logging.debug("Found a tmrg directive: %s" % tokens[0])

        directive = words[1].lower()
        arguments = words[2:] if len(words) > 2 else []

        def directive_default(directive, arguments):
            triplicate = arguments[0] == 'triplicate'
            logging.debug("Setting %s's triplicate default to: %s" % (self.current_namespace.full_name(), triplicate))
            self.current_namespace.constraints["default"] = triplicate

        def directive_do_not_triplicate(directive, arguments):
            logging.debug("Setting do_not_triplicate attribute to %s's nets: %s" % (self.current_namespace.full_name(), ", ".join(arguments)))
            for net in arguments:
                self.current_namespace.constraints[net] = False

        def directive_triplicate(directive, arguments):
            logging.debug("Setting triplicate attribute to %s's nets: %s" % (self.current_namespace.full_name(), ", ".join(arguments)))
            for net in arguments:
                self.current_namespace.constraints[net] = True

        def directive_do_not_touch(directive, arguments):
            if len(arguments):
                logging.debug("Setting do_not_touch attribute to %s's instances: %s" % (self.current_namespace.full_name(), ", ".join(arguments)))
                self.current_namespace.constraints["dntinst"] = arguments
            else:
                logging.debug("Setting do_not_touch attribute to %s" % (self.current_namespace.full_name()))
                self.current_namespace.constraints["dnt"] = True

        def directive_slicing(directive, arguments):
            logging.debug("Setting %s's Slicing to True" % (self.current_namespace.full_name()))
            self.current_namespace.constraints["slicing"] = True

        def directive_tmr_error(directive, arguments):
            enable = arguments[0] in ('true', 'enable')
            logging.debug("Setting %s's TMR Error : %s" % (self.current_namespace.full_name(), enable))
            self.current_namespace.constraints["tmr_error"] = enable

        def directive_tmr_error_exclude(directive, arguments):
            logging.debug("Appending to %s's TMR Error list: %s" % (self.current_namespace.full_name(), arguments[0]))
            if not "tmr_error_exclude" in self.current_namespace.constraints:
                self.current_namespace.constraints["tmr_error_exclude"] = []
            self.current_namespace.constraints["tmr_error_exclude"].append(arguments[0])

        def directive_seu_set(directive, arguments):
            logging.debug("Setting %s's SEU set to: %s" % (self.current_namespace.full_name(), arguments[0]))
            self.current_namespace.constraints["seu_set"] = arguments[0]

        def directive_seu_reset(directive, arguments):
            logging.debug("Setting %s's SEU reset to: %s" % (self.current_namespace.full_name(), arguments[0]))
            self.current_namespace.constraints["seu_reset"] = arguments[0]

        def directive_majority_voter_cell(directive, arguments):
            logging.debug("Setting %s's majority voter cell to: %s" % (self.current_namespace.full_name(), arguments[0]))
            self.current_namespace.constraints["majority_voter_cell"] = arguments[0]

        def directive_fanout_cell(directive, arguments):
            logging.debug("Setting %s's fanout cell to: %s" % (self.current_namespace.full_name(), arguments[0]))
            self.current_namespace.constraints["fanout_cell"] = arguments[0]
        
        def directive_as_is_index(directive, arguments):
            pass

        def warning_leftover(directive, arguments):
            logging.warning("Found an unprocessed tmrg `%s` directive." % directive)
            logging.warning("This can happen if it is unmatched or if it is interleaved with other translate/copy/ignore directives.")
            logging.warning("Please review your code in order to make sure the directives are interpreted correctly.")

        directive_lookup = {
            "do_not_triplicate"   : directive_do_not_triplicate,
            "triplicate"          : directive_triplicate,
            "default"             : directive_default,
            "tmr_error"           : directive_tmr_error,
            "tmr_error_exclude"   : directive_tmr_error_exclude,
            "do_not_touch"        : directive_do_not_touch,
            "seu_set"             : directive_seu_set,
            "seu_reset"           : directive_seu_reset,
            "slicing"             : directive_slicing,
            "majority_voter_cell" : directive_majority_voter_cell,
            "fanout_cell"         : directive_fanout_cell,
            "as_is_index"         : directive_as_is_index,
            "translate"           : warning_leftover,
            "copy"                : warning_leftover,
            "ignore"              : warning_leftover,
        }

        try:
            directive_lookup[directive](directive, arguments)
        except KeyError:
            logging.warning("Could not process tmrg directive: `%s` (in %s)" % (" ".join(words), self.current_namespace.full_name()))
            logging.warning("Directive of type %s is not supported." % directive)
        except Exception as e:
            logging.warning("Could not process tmrg directive: `%s` (in %s)" % (" ".join(words), self.current_namespace.full_name()))
            logging.warning("Unexpected error: %s" % e)
        else:
            logging.debug("Elaborated %s directive." % directive)

    def _elaborate(self, tokens):
        """ Elaborates tokens
        :param tokens: tokens to be parsed
        :return:
        """
        if not isinstance(tokens, ParseResults):
            if not isinstance(tokens, str):
                logging.debug("Unable to elaborate tokens of type %s: %s" % (str(type(tokens)), tokens))
            return

        name = str(tokens.getName()).lower()
        offset = name.find("@")
        if offset != -1:
            name = name[0:offset]
        logging.debug("[%-20s] len:%2d  str:'%s' >" % (name, len(tokens), str(tokens)[:80]))
        if name in self.elaborator:
            self.elaborator[name](tokens)
        else:
            logging.debug("No elaborator for %s" % name)
            if len(tokens):
                for t in tokens:
                    self._elaborate(t)

    def exc(self):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logging.error("")
        logging.error("TMR exception:")
        for l in traceback.format_exception(exc_type, exc_value,
                                            exc_traceback):
            for ll in l.split("\n"):
                logging.error(ll)
        logging.error(ll)

    def lineCount(self, fname):
        f = open(fname)
        lines = 0
        buf_size = 1024 * 1024
        read_f = f.read  # loop optimization

        buf = read_f(buf_size)
        while buf:
            lines += buf.count('\n')
            buf = read_f(buf_size)
        return lines

    def addFile(self, fname):
        already_exists = any(x for x in self.files if x.filename == fname)
        if already_exists:
            logging.debug("Omitting file %s, as it has already been elaborated." % fname)
            return

        if self.options.generateBugReport:
            bn = os.path.basename(fname)
            fcopy = os.path.join(self.options.bugReportDir, bn)
            logging.debug("Copying source file from '%s' to '%s'" % (fname, fcopy))
            shutil.copyfile(fname, fcopy)

        tokens = self.vp.parseFile(fname)
        if self.options.stats:
            lines = self.lineCount(fname)
            self.statsLogs.append("File '%s' has %d lines " % (fname, lines))
            self.statsFilesParsed += 1
            self.linesTotal += lines

        file = File(fname, self.config, tokens)
        self.files.append(file)

    def addLibFile(self, fname):
        already_exists = any(x for x in self.libs if x.filename == fname)
        if already_exists:
            logging.debug("Omitting file %s, as it has already been elaborated." % fname)
            return

        if self.options.generateBugReport:
            bn = os.path.basename(fname)
            fcopy = os.path.join(self.options.bugReportDir, bn)
            logging.debug("Copying library file from '%s' to '%s'" % (fname, fcopy))
            shutil.copyfile(fname, fcopy)
        tokens = self.vp.parseFile(fname)
        if self.options.stats:
            lines = self.lineCount(fname)
            self.statsLogs.append("File '%s' has %d lines " % (fname, lines))
            self.linesTotal += lines
            self.statsFilesParsed += 1

        file = File(fname, self.config, tokens)
        self.libs.append(file)

    def __getLenStr(self, toks):
        if not toks or len(toks) < 2:
            return "1"

        _from = LinearExpression(self.vf.format(toks[-2]))
        _to   = LinearExpression(self.vf.format(toks[-1]))

        return abs(_from - _to) + 1

    def __getFromStr(self, toks):
        fromStr = ""
        if len(toks) < 2:
            return fromStr
        fromStr = "%s" % (self.vf.format(toks[-2]))
        try:
            fromInt = eval(fromStr)
            fromStr = "%d" % fromInt
        except:
            pass
        return fromStr

    def __getToStr(self, toks):
        toStr = ""
        if len(toks) < 2:
            return toStr
        toStr = "%s" % (self.vf.format(toks[-1]))
        try:
            toInt = eval(toStr)
            toStr = "%d" % toInt
        except:
            pass
        return toStr

    def __getArrayLenStr(self, toks):
        rangeLen = "%s - %s + 1" % (self.vf.format(toks[3]), self.vf.format(toks[1]))
        try:
            rangeInt = eval(rangeLen)
            rangeLen = "%d" % rangeInt
        except:
            pass
        return rangeLen

    def __getArrayFrom(self, toks):
        return "%s" % (self.vf.format(toks[3]))

    def __getArrayTo(self, toks):
        return "%s" % (self.vf.format(toks[1]))

    def moduleSummary(self, module):
        def printDict(d, dname=""):
            if len(d) == 0:
                return

            tab = PrettyTable([dname,  "type", "packed range", "unpacked_range", "tmr"])
            tab.min_width[dname] = 30
            tab.min_width["type"] = 10
            tab.min_width["packed range"] = 20
            tab.min_width["unpacked range"] = 20
            tab.min_width["modifiers"] = 10
            tab.min_width["tmr"] = 10
            tab.align[dname] = "l"  # Left align city names

            for (k, item) in d:
                packed_ranges = "N/A"
                unpacked_ranges = "N/A"
                type = "N/A"
                tmr = "N/A"

                if isinstance(item, Signal):
                    packed_ranges = " ".join([str(i) for i in item.packed])
                    unpacked_ranges = " ".join([str(i) for i in item.unpacked])
                    type = item.full_type()
                    tmr = (str(item.triplicate) if item.triplicate is not None else "(inherit)") + (" (DNT)" if item.dont_touch else "")
                elif isinstance(item, Instance):
                    type = item.module_name
                    tmr = (str(item.triplicate) if item.triplicate is not None else "(inherit)") + (" (DNT)" if item.dont_touch else "")

                tab.add_row([k, type, packed_ranges, unpacked_ranges, tmr])
            tab.padding_width = 1  # One space between column edges and contents (default)
            for l in str(tab).split("\n"):
                logging.info(l)

        logging.info("")
        logging.info("Module:%s (dnt:%s)" % (module.name, module.constraints["dnt"]))
        logging.info("Constraints:")
        for k in module.constraints:
            logging.info("    %s: %s" % (k, module.constraints[k]))
        printDict(list(module.nets.items()),                         "Nets")
        printDict([(name, None) for name in module.local_variables], "Local variables")
        printDict([(name, None) for name in module.genvars],         "Genvars")
        printDict(module.instances,                                  "Instances")
        if module.params:
            printDict(list(zip(module.params.keys(), module.params)), "Params")

    def parse(self):
        """ Parse files
        :return:
        """
        def args2files(args):
            files = []
            for name in args:
                if len(name) == 0:
                    continue
                if os.path.isfile(name):
                    files.append(name)
                elif os.path.isdir(name):
                    for fname in glob.glob("%s/*.v" % name):
                        files.append(fname)
                else:
                    logging.error("File or directory does not exists '%s'" % name)

            return files
        parse_start_time = time.time()
        for fname in args2files(self.args):
            try:
                logging.info("Loading file '%s'" % fname)
                self.addFile(fname)
            except ParseException as err:
                logging.error("Error in file '%s' around line '%d'." % (fname, err.lineno))
                if err.line.find("tmrg ") == 0:
                    logging.error("")
                    logging.error("  Wrong tmrg directive")
                    logging.error("  //%s" % err.line[:-1])
                    logging.error("")
                else:
                    logging.error("")
                    logging.error(err.line)
                    logging.error(" "*(err.column-1) + "^")
                    logging.error(err)
                for l in traceback.format_exc().split("\n"):
                    logging.debug(l)
                raise ErrorMessage("Error during parsing")

        for fname in self.options.libs:
            self.libFiles.append(fname)

        for fname in self.libFiles:
            try:
                logging.info("Loading lib file '%s'" % fname)
                self.addLibFile(fname)
            except ParseException as err:
                logging.error("Error in file '%s' around line '%d'." % (fname, err.lineno))
                logging.error("")
                logging.error(err.line)
                logging.error(" "*(err.column-1) + "^")
                logging.error(err)
                for l in traceback.format_exc().split("\n"):
                    logging.error(l)
                raise ErrorMessage("Error during parsing")
        if self.options.stats:
            parse_time = time.time()-parse_start_time
            print("-"*80)
            for line in self.statsLogs:
                print(line)
            print("-"*80)
            print("Total number of files parsed: %d " % self.statsFilesParsed)
            print("Total number of lines parsed: %d " % self.linesTotal)
            print("Total parse time: %.3f s " % parse_time)
            print("-"*80)

    def elaborate_file(self, file, is_lib=False):
        lib_or_source_str = "library file" if is_lib else "source file"

        logging.info("")
        logging.info("Elaborating %s %s" % (lib_or_source_str, file.filename))

        for item in file.tokens:
            if item.getName() == "interfaceDecl":
                name = item.get("id")[0]
                self.global_namespace.types[name] = self.__interfacedecl(item)
                continue

            if item.getName() == "structDecl":
                name = item.get("id")[0]
                self.global_namespace.types[name] = self.__structdecl(item)
                continue

            if item.getName() == "enumDecl":
                name = item.get("id")[0]
                self.global_namespace.types[name] = self.__enumdecl(item)
                continue

            if item.getName() == "typedefDecl":
                name = item.get("id")[0]
                self.global_namespace.types[name] = self.__typedefdecl(item)
                continue

            if item.getName() == "unionDecl":
                name = item.get("id")[0]
                self.global_namespace.types[name] = self.__uniondecl(item)
                continue

            if item.getName() == "packageDecl":
                name = item.get("id@packageName")[0]
                self.global_namespace.packages[name] = self.__packagedecl(item)
                continue

            if item.getName() == "comment":
                self._elaborate_comment(item)

            if item.getName() == "module":
                moduleHdr = item[1]
                moduleName = moduleHdr[1]
                logging.debug("")
                logging.debug("= "*50)
                logging.info("Module %s (%s)" % (moduleName, file.filename))
                logging.debug("= "*50)

                # Libraries can contain already-triplicated modules
                auto_inferred = False
                if is_lib and moduleName.endswith("TMR"):
                    logging.info("Module %s has been already triplicated (%s)" % (moduleName, file.filename))
                    moduleName = moduleName[:-len("TMR")]
                    logging.info("Inferred non-triplicated module name: %s" % moduleName)
                    auto_inferred = True

                self.global_namespace.modules[moduleName] = Module(moduleName, file)
                self.current_namespace = self.global_namespace.modules[moduleName]
                self.current_namespace.types = self.global_namespace.types.copy()
                self.current_namespace.is_lib = is_lib

                # Copying other namespaces' types and params
                # so that they can be referenced.
                for pkg in self.global_namespace.packages:
                    for p in self.global_namespace.packages[pkg].params:
                        self.current_namespace.params[pkg + "::" + p] = self.global_namespace.packages[pkg].params[p]

                if "package_import" in moduleHdr:
                    self._elaborate_package_import(moduleHdr.get("package_import"))

                if "parameterList" in moduleHdr:
                    for param in moduleHdr.get("parameterList"):
                        if param.getName() == "comment":
                            continue

                        self.current_namespace.params.update(self.__paramdecl(param))

                if "ports" in moduleHdr:
                    for port in moduleHdr.get("ports"):
                        self._elaborate(port)

                for moduleItem in item[2]:
                    self._elaborate(moduleItem)

                # Regular file, nothing else to do.
                if not is_lib:
                    self.current_namespace = self.global_namespace
                    continue

                # If this is a lib, try some inferring here
                if auto_inferred:
                    # Look for triplicated ports...
                    for io in list(self.current_namespace.io):
                        # Check ports ending with A first, then see if there are the B and C versions
                        if not io.endswith("A"):
                            continue

                        port = io[:-1]
                        aport = io
                        bport = io[:-1] + "B"
                        cport = io[:-1] + "C"

                        # If B or C are not there, then it's not a triplicated port.
                        if bport not in self.current_namespace.io or cport not in self.current_namespace.io:
                            continue

                        logging.info("Inferring triplicated port: %s" % port)

                        # create new port and net
                        self.current_namespace.io[port] = self.current_namespace.io[io]
                        self.current_namespace.nets[port] = self.current_namespace.nets[io]

                        # remove triplicated ports and nets
                        for name in (aport, bport, cport):
                            del self.current_namespace.io[name]
                            del self.current_namespace.nets[name]

                        # add constraints
                        self.current_namespace.constraints[port] = True

                if "tmrError" in self.current_namespace.io:
                    self.current_namespace.constraints["tmr_error"] = True
                    del self.current_namespace.io["tmrError"]

                self.current_namespace.constraints["dnt"] = True
                if auto_inferred:
                    self.current_namespace.constraints["dnt"] = False
                    self.current_namespace.constraints["default"] = False

                self.current_namespace = self.global_namespace

    def elaborate(self, allowMissingModules=False):
        """ Elaborate the design
        :return:
        """
        elaborate_start_time = time.time()

        # elaborate all modules
        for file in self.files:
            self.elaborate_file(file, is_lib=False)

        for file in self.libs:
            self.elaborate_file(file, is_lib=True)

        # display summary
        if len(self.global_namespace.modules) > 1:
            logging.info("")
            logging.info("Modules found %d" % len(self.global_namespace.modules))
            libDetails = {}
            for module in sorted(self.global_namespace.modules):
                if not self.global_namespace.modules[module].is_lib:
                    logging.info(" - %s (%s)" % (module, self.global_namespace.modules[module].file.filename))
                else:
                    lib = self.global_namespace.modules[module].file.filename
                    if not lib in libDetails:
                        libDetails[lib] = []
                    libDetails[lib].append(module)

            for lib in libDetails:
                s = "Lib %s : " % lib
                infoed = 0
                for m in libDetails[lib]:
                    s += m+" "
                    if len(s) > 100:
                        if infoed < 5:
                            logging.info(s)
                        else:
                            logging.debug(s)
                        s = ""
                        infoed += 1
                if infoed < 5:
                    logging.info(s)
                else:
                    logging.debug(s)

        # check if all modules are known
        logging.info("")
        logging.info("Checking the design hierarchy")
        elaborationError = False
        for module in self.global_namespace.modules:
            for (instName, instance) in self.global_namespace.modules[module].instances:
                if instance.module_name in self.global_namespace.modules:
                    self.global_namespace.modules[instance.module_name].instantiated += 1
                else:
                    if "dnt" in self.global_namespace.modules[module].constraints and self.global_namespace.modules[module].constraints["dnt"]:
                        logging.warning("Unknown module instantiation! In module '%s', instance.module_name name '%s' instance.module_name type '%s'." % (
                            module, instName, instance.module_name))
                    else:
                        msg = "Unknown module instantiation! In module '%s', instance name '%s' instance type '%s'." % (module, instName, instance.module_name)
                        if allowMissingModules:
                            logging.info(msg)
                        else:
                            logging.error(msg)
                            elaborationError = True

        tops = 0
        self.topFile = ""
        self.topModule = ""
        if len(self.global_namespace.modules) == 0:
            raise ErrorMessage(
                "No modules found. Please refer to the documentation using 'tmrg --help' or 'tmrg --doc'")

        for module in self.global_namespace.modules:
            if self.global_namespace.modules[module].is_lib:
                continue

            if self.global_namespace.modules[module].instantiated == 0:
                self._printHierarchy(module)
                self.topModule = module
                self.topFile = self.global_namespace.modules[module].file.filename
                tops += 1

        top_module = self.config.get("global", "top_module")
        if self.options.top_module:
            top_module = self.options.top_module
        if top_module:
            if not top_module in self.global_namespace.modules:
                logging.error("Specified top module (%s) not found.", self.options.top_module)
                elaborationError = True
            else:
                self.topModule = top_module
                logging.info("Top module found (%s)!", self.topModule)
        elif tops != 1:
            logging.warning("The design has multiple top cells! Output may not be correct!")

        if not allowMissingModules and elaborationError:
            raise ErrorMessage("Serious error during elaboration.")

        if self.options.stats:
            elaborate_time = time.time()-elaborate_start_time
            print("Elaboration time : %.3f s " % elaborate_time)
            print("-"*80)

    def _printHierarchy(self, topModule):
        def _printH(module, i="", done=[]):
            i += "  |"
            for instName, inst in self.global_namespace.modules[module].instances:
                if inst.module_name in self.global_namespace.modules:
                    logging.info(i+"- "+instName+":"+inst.module_name)
                    if id(inst) in done:
                        continue
                    else:
                        done.append(id(inst))
                        _printH(inst.module_name, i, done)
                else:
                    logging.info(i+"- [!] "+instName+":"+inst.module_name)

        logging.info("[%s]" % topModule)
        done = []
        _printH(topModule, "", done)

    @staticmethod
    def split_name(name):
        if not isinstance(name, str):
            raise ErrorMessage("%s is not a string!" % name)

        splitted = name.split(".")

        return (splitted[0], ".".join(splitted[1:]) if len(splitted) > 1 else None)

    def showSummary(self):
        for module in sorted(self.global_namespace.modules):
            self.moduleSummary(self.global_namespace.modules[module])

    def getAllInstances(self, module, prefix=""):
        # we want store instances from the bottom of the hierarhy
        if len(self.global_namespace.modules[module].instances) == 0:
            return [(prefix, module)]

        res = []
        # in other case we loop over hierarchy
        for (instId, inst) in self.global_namespace.modules[module].instances:
            inst_module = inst.module_name

            if "[" in instId:
                instId = "\\"+instId+" "

            if inst_module in self.global_namespace.modules:
                res += self.getAllInstances(inst_module, prefix=prefix+"/"+instId)

        return res
