#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# verilogParse.py
#
# an example of using the pyparsing module to be able to process Verilog files
# uses BNF defined at http://www.verilog.com/VerilogBNF.html
#
#    Copyright (c) 2004-2011 Paul T. McGuire.  All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# If you find this software to be useful, please make a donation to one
# of the following charities:
# - the Red Cross (http://www.redcross.org)
# - Hospice Austin (http://www.hospiceaustin.org)
#
#    DISCLAIMER:
#    THIS SOFTWARE IS PROVIDED BY PAUL T. McGUIRE ``AS IS'' AND ANY EXPRESS OR
#    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#    EVENT SHALL PAUL T. McGUIRE OR CO-CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OFUSE,
#    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
#    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#    For questions or inquiries regarding this license, or commercial use of
#    this software, contact the author via e-mail: ptmcg@users.sourceforge.net
#
# Todo:
#  - add pre-process pass to implement compilerDirectives (ifdef, include, etc.)
#
# Revision History:
#
#   1.0   - Initial release
#   1.0.1 - Fixed grammar errors:
#           . real declaration was incorrect
#           . tolerant of '=>' for '*>' operator
#           . tolerant of '?' as hex character
#           . proper handling of mintypmax_expr within path delays
#   1.0.2 - Performance tuning (requires pyparsing 1.3)
#   1.0.3 - Performance updates, using Regex (requires pyparsing 1.4)
#   1.0.4 - Performance updates, enable packrat parsing (requires pyparsing 1.4.2)
#   1.0.5 - Converted keyword Literals to Keywords, added more use of Group to
#           group parsed results tokens
#   1.0.6 - Added support for module header with no ports list (thanks, Thomas Dejanovic!)
#   1.0.7 - Fixed erroneous '<<' Forward definition in timCheckCond, omitting ()'s
#   1.0.8 - Re-released under MIT license
#   1.0.9 - Enhanced udpInstance to handle identifiers with leading '\' and subscripting
#   1.0.10 - Fixed change added in 1.0.9 to work for all identifiers, not just those used
#           for udpInstance.
#   1.1.0 - Adding named results and support for a variety of SV constructs
#
#   Szymon Kulis, CERN, 2015-2020
#   Renamed to verilog_parser.py, integreated to TMRG, adjusted to the TMRG needs
#

__version__ = "1.0.10"

import re
import sys
import logging
import os
try:
    from cPyparsing import (
        CharsNotIn,
        Combine,
        FollowedBy,
        Forward,
        Group,
        Keyword,
        Literal,
        MatchFirst,
        OneOrMore,
        Optional,
        ParserElement,
        Regex,
        StringEnd,
        Suppress,
        Word,
        ZeroOrMore,
        alphanums,
        alphas,
        dblQuotedString,
        empty,
        nums,
        oneOf,
        restOfLine,
        cStyleComment
    )
except:
    from pyparsing import (
        CharsNotIn,
        Combine,
        FollowedBy,
        Forward,
        Group,
        Keyword,
        Literal,
        MatchFirst,
        OneOrMore,
        Optional,
        ParserElement,
        Regex,
        StringEnd,
        Suppress,
        Word,
        ZeroOrMore,
        alphanums,
        alphas,
        dblQuotedString,
        empty,
        nums,
        oneOf,
        restOfLine,
        cStyleComment
    )

# see: https://gitlab.cern.ch/tmrg/tmrg/-/merge_requests/174#note_7509617
# pylint: disable=too-many-function-args unsupported-binary-operation

def delimitedList( expr, delim=","):
    """Helper to define a delimited list of expressions - the delimiter defaults to ','.
    """
    return ( (expr) + ZeroOrMore( Suppress(delim) + (expr)) ).setResultsName("delimitedList")

class VerilogParser:
    data_2states_types = "bit byte shortint int longint".split()
    data_4states_types = "reg logic integer".split()
    data_nonint_types  = "string time real shortreal realtime event".split()

    as_is_buffers = []
    pragmas = ["cadence", "synopsys", "lint_checking"] 
    extra_pragmas = []
    comments = False

    def __init__(self):
        sys.setrecursionlimit(4000)
        ParserElement.enablePackrat()
        self.include=False
        self.inc_dir=[]

        # Special till-newline expressions
        self.compDirective = Group(Combine( "`" + \
            oneOf("define undef ifdef ifndef else elsif endif default_nettype "
                  "include resetall timescale unconnected_drive "
                  "nounconnected_drive celldefine endcelldefine") + \
            restOfLine)).setResultsName("comp_directive")

        self.comment = Group(Regex(r"//(?P<content>(?:\\\n|[^\n])*)")).setResultsName("comment")

        # keywords
        if_        = Keyword("if")
        else_      = Keyword("else")
        edge       = Keyword("edge")
        posedge    = Keyword("posedge")
        negedge    = Keyword("negedge")
        or_        = Keyword("or")
        specify    = Keyword("specify")
        endspecify = Keyword("endspecify")
        fork       = Keyword("fork")
        join       = Keyword("join")
        begin      = Keyword("begin")
        end        = Keyword("end")
        default    = Keyword("default")
        forever    = Keyword("forever")
        repeat     = Keyword("repeat")
        while_     = Keyword("while")
        for_       = Keyword("for")
        case       = oneOf( "case casez casex" )
        endcase    = Keyword("endcase")
        wait       = Keyword("wait")
        disable    = Keyword("disable")
        deassign   = Keyword("deassign")
        force      = Keyword("force")
        release    = Keyword("release")
        assign     = Keyword("assign").setResultsName("keyword")
        wire       = Keyword("wire")
        case_if_modifiers = oneOf("unique unique0 priority")

        # Keywords
        directions         = MatchFirst(map(Keyword, "input output inout".split()))
        self.data_types    = MatchFirst(map(Keyword, self.data_2states_types + self.data_4states_types + self.data_nonint_types))
        self.net_types     = MatchFirst(map(Keyword, "wire supply0 supply1 tri triand trior tri0 tri1 wand wor".split()))
        self.gate_types    = MatchFirst(map(Keyword, "and nand or nor xor xnor buf bufif0 bufif1 not notif0 notif1 pulldown pullup nmos rnmos pmos rpmos cmos rcmos tran rtran tranif0 rtranif0 tranif1 rtranif1"))
        modifiers          = MatchFirst(map(Keyword, "unsigned signed vectored scalared const".split()))
        strength           = MatchFirst(map(Keyword, "supply0 supply1 strong strong0 strong1 pull0 pull1 weak weak0 weak1 highz0 highz1 small medium large".split()))
        unsupported_log95  = MatchFirst(map(Keyword, "ifnone".split()))
        unsupported_log21  = MatchFirst(map(Keyword, "incdir pulsestyle_ondetect cell pulsestyle_onevent config instance endconfig liblist showcancelled library use noshowcancelled".split()))
        unsupported_svlog  = MatchFirst(map(Keyword, """
accept_on export ref alias extends restrict extern final s_always first_match s_eventually assert foreach s_nexttime assume forkjoin s_until before globals_until_with bind iff sequence bins ignore_bins binsof illegal_bins implies solve break import inside chandle checker interface class intersect super clocking join_any sync_accept_on join_none sync_reject_on constraint let tagged context local this throughout cover timeprecision covergroup matches timeunit coverpoint modport type cross new dist nexttime union do null unique endchecker package unique0 endclass packed until endclocking priority until_with endgroup program untypted endinterface property var endpackage protected virtual endprogram pure void endproperty rand wait_order endsequence randc enum randcase wildcard eventually randsequence with expect reject_on within
        """.split()))

        non_type = MatchFirst(map(Keyword, "return force".split()))

        valid_keywords = directions | self.data_types | self.net_types | modifiers | strength | unsupported_log95 | unsupported_log21 | unsupported_svlog | non_type
        custom_type = Group(~valid_keywords + Word(alphas + '_', alphanums + '_') + Optional("::" + Word(alphas + '_', alphanums + '_')))

        # primitives
        self.semi = Literal(";")
        self.lpar = Literal("(")
        self.rpar = Literal(")")
        self.equals = Literal("=")
        self.constraints = {"triplicate":set(),"do_not_triplicate":set(), "default":True, "tmr_error":True}

        identLead = alphas+"$_"
        identBody = alphanums+"$_"
        identifier1 = Regex( r"`?["+identLead+"]["+identBody+"]*").setName("baseIdent")
        identifier2 = Regex(r"\\\S+").setParseAction(lambda t:t[0][1:]).setName("escapedIdent")
        identifier = ~(directions | self.data_types | self.net_types) + (identifier1 | identifier2)
        identifier_unsafe = identifier1 | identifier2
        literal_macro = Regex( r"`["+identLead+"]["+identBody+"]*")

        hexnums = nums + "abcdefABCDEF" + "_?"
        base = Regex("[bBoOdDhH]{0,1}").setName("base")
        signed = Regex("'[sS]?").setName("signed")
        basedNumber = Combine( Optional( Word(nums + "_") | literal_macro ) + signed + base + Word(hexnums+"xXzZ"),
                               joinString="", adjacent=False ).setResultsName("basedNumber")
        number = ( basedNumber | \
                   Regex(r"[+-]?([0-9][0-9_]?)+(\.[0-9_]*)?([0-9_]*)?([Ee][+-]?[0-9_]+)?") \
                  ).setName("numeric")

        # Time
        time_unit = oneOf("fs ps ns us ms s")
        time_literal = Regex(r'[0-9]+(\.[0-9_]*)?([fpnum]?s)')

        # Expressions
        self.expr = Forward()

        attr_spec = Group(identifier + Optional("=" + self.expr)).setResultsName("attr_spec")
        attribute = Group( "(*" + Group(delimitedList(attr_spec)) + "*)").setResultsName("attribute")
        attributes = OneOrMore(attribute).setResultsName("attributes")

        # References to registers or nets
        self.range = Group( Suppress("[") + Group(self.expr).setResultsName("expr@from") + oneOf(": +: -:").setResultsName("dir") + Group(self.expr).setResultsName("expr@to") + Suppress("]")).setResultsName("range")
        self.size = Group( Suppress("[") + Group(self.expr) + Suppress("]")).setResultsName("size")
        self.field = Group(Suppress(".") + identifier).setResultsName("field_ref")
        regReference  = Group( Optional(attributes) + Optional(Group(identifier).setResultsName("namespace") + Suppress("::")) + Group(identifier).setResultsName("name") + Group(ZeroOrMore(self.range | self.size | self.field)).setResultsName("range_or_field") ).setResultsName("reg_reference")

        # Concatenations
        concat = Optional("'") + Group(
            Suppress("{") +
            Group(delimitedList(
                Group(
                    Optional(Group(identifier).setResultsName("field_name") + ":") +
                    Group(self.expr).setResultsName("expr@field_value")
                )
            )).setResultsName("elements") +
            Suppress("}")
        ).setResultsName("concat")

        multiConcat = Group("{" + self.expr + concat + "}").setName("multiConcat")

        streaming = Group("{" +
            Group(oneOf("<< >>")).setResultsName("streaming_op") +
            Optional(Group(number | identifier).setResultsName("slice_size")) +
            Group(concat).setResultsName("streaming_concat") +
            "}").setResultsName("streaming_expression")

        funcOrTaskCall = Group(
            Optional(Group(identifier).setResultsName("namespace") + Suppress("::")) +
            Group(Combine( Optional("$") + identifier )).setResultsName("name") +
            Suppress("(") +
            Optional(
                Group(
                    delimitedList( Group(self.expr) )
                ).setResultsName("arguments")
            ) +
            Suppress(")")
        ).setResultsName("funcOrTaskCall")

        funcOrTaskCallStandalone = Group(
            Optional(Group(identifier).setResultsName("namespace") + Suppress("::")) +
            Group(Combine( Optional("$") + identifier )).setResultsName("name") +
            Optional(
                Suppress("(") +
                Group(
                    Optional(delimitedList( Group(self.expr) ))
                ).setResultsName("arguments") +
                Suppress(")")
            ) +
            self.semi.setResultsName("semi")
        ).setResultsName("funcOrTaskCall")

        mintypmaxExpr = Group( self.expr + ":" + self.expr + ":" + self.expr ).setName("mintypmax")
        primary = ( time_literal | number |
                    ("(" + mintypmaxExpr + ")" ) |
                    ( "(" + Group(self.expr) + ")" ).setName("nestedExpr") |
                    multiConcat |
                    concat |
                    streaming |
                    dblQuotedString |
                    funcOrTaskCall |
                    regReference
                  ).setResultsName("primary")

        unop  = oneOf( "+  -  !  ~  &  ~&  |  ^|  ^  ~^" ).setName("unop")
        binop = oneOf( "+  -  *  /  %  ==  !=  ===  !==  &&  "
                       "||  <  <=  >  >=  &  |  ^  ^~  ~^  >>  << ** <<< >>>" ).setName("binop")
        inlineIfExpr= Group( Group(primary) + Suppress(Literal("?")) + Group(self.expr) + Suppress(Literal(":")) + Group(self.expr) ).setResultsName("inlineIfExpr")
        inside = Group(primary + Keyword("inside") + self.expr).setResultsName("inside")

        cast = (identifier_unsafe | "(" + self.expr + ")" | number) + "'" + "(" + self.expr + ")"
        self.expr << (
                       ( unop + self.expr ) |  # must be first!
                       inlineIfExpr | inside | 
                       ( (cast | primary) + Optional(binop + self.expr) )
                     )

        self.expr=self.expr.setName("expr").setResultsName("expr")

        # Time unit/precision
        timeunitsDecl = Group(
                Group( Keyword("timeunit") | Keyword("timeprecision") ).setResultsName("keyword") +
                Group(time_literal).setResultsName("time0") +
                Optional(
                    "/" +
                    Group(time_literal).setResultsName("time1")
                ) +
                Suppress(self.semi)
                ).setResultsName("timeunitsDecl")

        # Events
        eventExpr = Forward()
        eventTerm = Group ("*" | ( Optional (posedge | negedge) + ( regReference|  "(" + regReference + ")" ))  | ( "(" + eventExpr + ")" )).setResultsName("eventTerm")
        eventExpr << Group( (delimitedList( eventTerm , (or_|",") )).setResultsName("delimitedOrList") )

        eventControl = Group( "@" + eventExpr ).setName("eventCtrl").setResultsName("eventCtrl")

        # Delays
        delayArg = Group(
                time_literal |
                number.setResultsName("number") |
                Word(alphanums+"$_`").setResultsName("alphanum") |
                ( "(" + Group( delimitedList( mintypmaxExpr | self.expr ) ).setResultsName("expr") + ")" )
            ).setName("delayArg")

        delay = Group( "#" + delayArg ).setName("delay").setResultsName("delay")
        delayOrEventControl = delay | eventControl

        # Assignments
        lvalue = Group(regReference | concat).setResultsName("lvalue")
        self.assignment = Group( lvalue + oneOf("= += -= |= ^= &= *= /= <<= >>= <<<= >>>= %=").setResultsName("op") + Optional(Group(delayOrEventControl).setResultsName("delayOrEventControl")) + Group(self.expr).setResultsName("expr@rvalue") ).setResultsName( "assignment" )

        self.incr_decr   = Group( regReference + oneOf("++ --")).setResultsName( "incr_decr" )

        self.nbAssgnmt = Group(lvalue + Suppress("<=") + Optional(Group(delayOrEventControl).setResultsName("delayOrEventControl")) + Group(self.expr).setResultsName("expr@rvalue")).setResultsName("nbassignment")

        regIdentifier = Group( Group(identifier).setResultsName("name") + Group(ZeroOrMore(self.range | self.size)).setResultsName("unpacked_ranges")).setResultsName("identifier")

        standard_typed_parameter_declaration = (
            Optional(Group(self.data_types).setResultsName("standard_type")) +
            Optional(modifiers.setResultsName("modifiers")) +
            Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
            Group(delimitedList(self.assignment)).setResultsName("params")
        )

        custom_typed_parameter_declaration = (
            custom_type.setResultsName("custom_type") +
            Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
            Group(delimitedList(self.assignment)).setResultsName("params")
        )

        parameterDecl = Group(
            Group( Keyword("parameter") | Keyword("localparam") ).setResultsName("param_type") +
            standard_typed_parameter_declaration +
            Suppress(self.semi)
        ).setResultsName("paramDecl")

        customParameterDecl = Group(
            Group( Keyword("parameter") | Keyword("localparam") ).setResultsName("param_type") +
            custom_typed_parameter_declaration +
            Suppress(self.semi)
        ).setResultsName("paramDecl")

        standard_typed_declaration = (
            Optional(attributes).setResultsName("attributes")+
            Optional(Group(oneOf("static automatic")).setResultsName("lifetime")) +
            (
                (
                    Optional(Group(self.net_types).setResultsName("net_type")) +
                    Group(self.data_types).setResultsName("standard_type")
                ) | Group(self.net_types).setResultsName("net_type")
            ) + Optional(modifiers).setResultsName("modifiers") +
            Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
            Group( delimitedList( regIdentifier )).setResultsName("identifiers")
        )

        custom_typed_declaration = (
            Optional(attributes).setResultsName("attributes")+
            Optional(Group(oneOf("static automatic")).setResultsName("lifetime")) +
            Optional(Group(self.net_types).setResultsName("net_type")) +
            custom_type.setResultsName("custom_type") +
            Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
            Group( delimitedList( regIdentifier )).setResultsName("identifiers")
        )

        type_reference = (
            (
                Group(self.data_types).setResultsName("standard_type") +
                Optional(modifiers).setResultsName("modifiers")
            ) ^ custom_type.setResultsName("custom_type")
        ) + Group(ZeroOrMore(self.range)).setResultsName("packed_ranges")

        standard_type_port = (
            Optional(attributes).setResultsName("attributes")+
            directions.setResultsName("dir") + delimitedList(
            Group(
                standard_typed_declaration
            ).setResultsName("typed") | Group(
                Optional(modifiers).setResultsName("modifiers") +
                Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
                Group( delimitedList( regIdentifier )).setResultsName("identifiers")
            ).setResultsName("untyped")
        ))

        custom_type_port = (
            Optional(attributes).setResultsName("attributes")+
            directions.setResultsName("dir") + delimitedList(
            Group(
                custom_typed_declaration
            ).setResultsName("typed") | Group(
                Optional(modifiers).setResultsName("modifiers") +
                Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
                Group( delimitedList( regIdentifier )).setResultsName("identifiers")
            ).setResultsName("untyped")
        ))

        self.standardTypePortHdr  = Group(standard_type_port).setName("portHdr").setResultsName("portHdr")
        self.standardTypePortBody = Group(standard_type_port + Suppress(self.semi) ).setName("portBody").setResultsName("portBody")
        self.customTypePortHdr  = Group(custom_type_port).setName("portHdr").setResultsName("portHdr")
        self.customTypePortBody = Group(custom_type_port + Suppress(self.semi) ).setName("portBody").setResultsName("portBody")

        portRef = Group(Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") + regIdentifier).setResultsName("portNotAnsi")
        portExpr = portRef | Group( "{" + delimitedList( portRef ) + "}" ).setResultsName("portNotAnsiList")
        self.port = Group(portExpr | Group( ( "." + identifier + "(" + portExpr + ")" ) ).setResultsName("explicitConn") ).setResultsName("port")

        modulePorts = Optional(
                Suppress("(") +
                Optional(Group(
                    (
                        delimitedList(
                            Optional(self.comment) +
                            (self.standardTypePortHdr ^ self.customTypePortHdr) # ANSI
                        ) | delimitedList(
                            Optional(self.comment) +
                            self.port # Non-ANSI
                        )
                    ) +
                    Optional(self.comment)
                ).setResultsName("ports")) +
                Suppress(")")
            )

        self.standardTypeNetDecl = Group(
                standard_typed_declaration +
                Suppress(self.semi)
        ).setResultsName("netDecl")

        self.customTypeNetDecl = Group(
                custom_typed_declaration +
                Suppress(self.semi)
        ).setResultsName("netDecl")

        self.standardTypeNetDeclWAssign = Group(
                Optional(attributes).setResultsName("attributes")+
                Optional(Group(oneOf("static automatic")).setResultsName("lifetime")) +
                (
                    (
                        Optional(Group(self.net_types).setResultsName("net_type")) +
                        Group(self.data_types).setResultsName("standard_type")
                    ) | Group(self.net_types).setResultsName("net_type")
                ) +
                Optional(modifiers).setResultsName("modifiers") +
                Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
                Optional( delay ) +
                Group( delimitedList( self.assignment ) ).setResultsName("assignments") + 
                Suppress(self.semi)
            ).setResultsName("netDeclWAssign")

        self.customTypeNetDeclWAssign = Group(
                Optional(attributes).setResultsName("attributes")+
                Optional(Group(oneOf("static automatic")).setResultsName("lifetime")) +
                Optional(Group(self.net_types).setResultsName("net_type")) +
                custom_type.setResultsName("custom_type") +
                Optional(modifiers).setResultsName("modifiers") +
                Group(ZeroOrMore(self.range)).setResultsName("packed_ranges") +
                Optional( delay ) +
                Group( delimitedList( self.assignment ) ).setResultsName("assignments") +
                Suppress(self.semi)
            ).setResultsName("netDeclWAssign")

        strength0 = oneOf("supply0  strong0  pull0  weak0  highz0")
        strength1 = oneOf("supply1  strong1  pull1  weak1  highz1")
        driveStrength = Group( "(" + ( ( strength0 + "," + strength1 ) |
                                       ( strength1 + "," + strength0 ) ) + ")" ).setName("driveStrength").setResultsName("driveStrength")

        blockDecl = (
            self.standardTypeNetDecl |
            self.standardTypeNetDeclWAssign |
            parameterDecl |
            customParameterDecl |
            self.customTypeNetDecl |
            self.customTypeNetDeclWAssign
            )

        self.stmt = Forward().setName("stmt").setResultsName("stmt")
        stmtOrNull = self.stmt | self.semi
        case_item = Group(
            Group(
                (delimitedList( Group(self.expr) | self.range ) + Suppress(":")) |
                default + Optional(Suppress(":"))
            ) +
            stmtOrNull
        ).setResultsName("caseItem")
        case_stmt = Group(
            Optional(Group(case_if_modifiers).setResultsName("modifier")) +
            Group(case).setResultsName("case_flavour") +
            Suppress("(") + Group(self.expr).setResultsName("expr@case") + Suppress(")") +
            Optional(Keyword("inside").setResultsName("inside")) +
            Group(OneOrMore( case_item | self.comment )).setResultsName("case_items") +
            endcase
        ).setResultsName("case")

        begin_end_block = Group(
                begin + Optional(Suppress(Literal(":")) + identifier.setResultsName("id@blockName")) +
                Group(ZeroOrMore( blockDecl )).setResultsName("declarations") +
                Group(ZeroOrMore( self.stmt )).setResultsName("statements") +
                Suppress(end) + Optional(Suppress(Literal(":")) + identifier.setResultsName("id@blockNameEnd"))
        ).setName("beginend").setResultsName("beginend")

        condition=Group("(" + self.expr + ")").setResultsName("condition")
        self.stmt << (
            self.comment |
            self.compDirective |
            begin_end_block |
            Group( Optional(Group(case_if_modifiers).setResultsName("modifier")) + if_ + condition + Group(stmtOrNull).setResultsName("stmt_true") + Optional( else_ + Group(stmtOrNull).setResultsName("stmt_false")) ).setResultsName("ifelse") | \
            Group( delayOrEventControl + stmtOrNull ).setResultsName("delayStm") |\
            case_stmt |\
            Group( forever + self.stmt ).setResultsName("forever") |\
            Group( repeat + "(" + self.expr + ")" + self.stmt ) |\
            Group( while_ + "(" + self.expr + ")" + self.stmt ) |\
            Group( for_ + Suppress("(") + Group( (self.assignment + Suppress(self.semi)) | self.standardTypeNetDeclWAssign | self.customTypeNetDeclWAssign).setResultsName("for1") + Group( self.expr ).setResultsName("expr@for2") + Suppress(self.semi) + Group(self.assignment | self.incr_decr).setResultsName("for3") + Suppress(")") + Group(self.stmt).setResultsName("stmt") ).setResultsName("forstmt") |\
            Group( self.incr_decr + self.semi ) |\
            Group( fork + ZeroOrMore( self.stmt ) + join ) |\
            Group( fork + ":" + identifier + ZeroOrMore( blockDecl ) + ZeroOrMore( self.stmt ) + end ) |\
            Group( wait + "(" + Group(self.expr).setResultsName("expr@condition") + ")" + Group(stmtOrNull).setResultsName("action") ).setResultsName("wait") |\
            Group( "->" + identifier + self.semi ) |\
            Group( disable + identifier + self.semi ) |\
            Group( assign + self.assignment + self.semi ).setResultsName("assign") |\
            Group( deassign + lvalue + self.semi ) |\
            Group( force + self.assignment + self.semi ).setResultsName("force") |\
            Group( release + lvalue + self.semi ).setResultsName("release") |\
            Group(Suppress("return") + Group(self.expr).setResultsName("expr") + self.semi).setResultsName("return") |\
            attributes |\
            Group( self.assignment + Suppress(self.semi) ).setResultsName("assignmentStm") |\
            Group( self.nbAssgnmt + Suppress(self.semi) ).setResultsName("nbassignmentStm") |\
            funcOrTaskCallStandalone
            # these  *have* to go at the end of the list!!!
        )

        """
        x::=<blocking_assignment> ;
        x||= <non_blocking_assignment> ;
        x||= if ( <expression> ) <statement_or_null>
        x||= if ( <expression> ) <statement_or_null> else <statement_or_null>
        x||= case ( <expression> ) <case_item>+ endcase
        x||= casez ( <expression> ) <case_item>+ endcase
        x||= casex ( <expression> ) <case_item>+ endcase
        x||= forever <statement>
        x||= repeat ( <expression> ) <statement>
        x||= while ( <expression> ) <statement>
        x||= for ( <assignment> ; <expression> ; <assignment> ) <statement>
        x||= <delay_or_event_control> <statement_or_null>
        x||= wait ( <expression> ) <statement_or_null>
        x||= -> <name_of_event> ;
        x||= <seq_block>
        x||= <par_block>
        x||= <task_enable>
        x||= <system_task_enable>
        x||= disable <name_of_task> ;
        x||= disable <name_of_block> ;
        x||= assign <assignment> ;
        x||= deassign <lvalue> ;
        x||= force <assignment> ;
        x||= release <lvalue> ;
        """
        self.alwaysStmt = Group( oneOf("always always_ff always_comb always_latch").setResultsName("type") + Optional(eventControl) + Group(self.stmt).setResultsName("stmt") ).setName("alwaysStmt").setResultsName("always")
        initialStmt = Group( Keyword("initial") + self.stmt ).setName("initialStmt").setResultsName("initialStmt")

        chargeStrength = Group( "(" + oneOf( "small medium large" ) + ")" ).setName("chargeStrength")

        self.continuousAssign = Group(  Suppress(assign)
              + Optional( driveStrength.setResultsName("driveStrength") )
              + Optional( delay )
              + Group(delimitedList( self.assignment )).setResultsName("assignments") + Suppress(self.semi)
            ).setResultsName("continuousAssign")

        tfDecl = (
            self.standardTypePortBody |
            self.standardTypeNetDecl |
            parameterDecl |
            customParameterDecl |
            self.customTypeNetDecl |
            self.customTypePortBody
            ).setResultsName("tfDecl")

        functionDecl = Group(
            Suppress("function") +
            Optional(Group(oneOf("static automatic")).setResultsName("lifetime")) +
            Optional(Group(self.data_types).setResultsName("return_type")) +
            Optional(self.range.setResultsName("return_range")) +
            Group(identifier).setResultsName("name") +
            modulePorts +
            self.semi +
            Group( ZeroOrMore( tfDecl ) ).setResultsName("tfDecl") +
            Group( ZeroOrMore( self.stmt ) ).setResultsName("statements") +
            Suppress("endfunction")
            ).setResultsName("functionDecl")

        task = Group(
            Suppress("task") +
            Optional(Group(oneOf("static automatic")).setResultsName("lifetime")) +
            Group(identifier).setResultsName("name") +
            modulePorts +
            Suppress(self.semi) +
            Group( ZeroOrMore( tfDecl ) ).setResultsName("tfDecl") +
            Group( ZeroOrMore( self.stmt ) ).setResultsName("statements") +
            "endtask"
        ).setResultsName("task")

        self.structDecl = Group(
                "typedef struct packed" +
                Suppress("{") +
                Group(ZeroOrMore( self.standardTypeNetDecl | self.customTypeNetDecl)).setResultsName("fields") +
                Suppress ("}") +
                Group(identifier).setResultsName("id") +
                Suppress(self.semi)
        ).setName("structDecl").setResultsName("structDecl")

        enumElement = Group(
            Group(identifier).setResultsName("name") + Optional(
                Suppress("=") +
                Group(self.expr).setResultsName("expr@rvalue")
            )
        )

        self.enumDecl = Group(
                "typedef enum" + Group(type_reference).setResultsName("type") +
                Suppress("{") +
                Group(delimitedList( enumElement )).setResultsName("fields") +
                Suppress ("}") +
                Group(identifier).setResultsName("id") +
                Suppress(self.semi)
        ).setName("enumDecl").setResultsName("enumDecl")

        self.typedefDecl = Group(
                "typedef" + Group(type_reference).setResultsName("type") +
                Group(identifier).setResultsName("id") +
                Suppress(self.semi)
        ).setName("typedefDecl").setResultsName("typedefDecl")

        self.unionDecl = Group(
                "typedef union packed" +
                Suppress("{") +
                Group(ZeroOrMore( self.standardTypeNetDecl | self.customTypeNetDecl)).setResultsName("fields") +
                Suppress ("}") +
                Group(identifier).setResultsName("id") +
                Suppress(self.semi)
        ).setName("unionDecl").setResultsName("unionDecl")

        self.genVarDecl = Group(
                Keyword("genvar") +
                Group(delimitedList(Group(identifier))).setResultsName("names") +
                Suppress(self.semi)
                ).setResultsName("genVarDecl")

        # Simple assignment with declaration (in for loops, for instance)
        self.genVarDeclWAssign = Group(
                Keyword("genvar") +
                Group(identifier).setResultsName("name") +
                Suppress("=") +
                Group(self.expr).setResultsName("expr@rvalue") +
                Suppress(self.semi)
                ).setResultsName( "genVarDeclWAssign" )

        gateType = oneOf("and  nand  or  nor xor  xnor buf  bufif0 bufif1 "
                         "not  notif0 notif1  pulldown pullup nmos  rnmos "
                         "pmos rpmos cmos rcmos   tran rtran  tranif0  "
                         "rtranif0  tranif1 rtranif1"  )
        gateInstance = Group(
                Optional( Group( identifier + Optional( self.range ) ).setResultsName("name_and_range") ) + \
                Suppress("(") + Group( delimitedList( self.expr ) ).setResultsName("ports") + Suppress(")")
        )

        self.gateDecl = Group( gateType +
            Optional( driveStrength ).setResultsName("driveStrength") +
            Optional( delay ) +
            Group(delimitedList( gateInstance )).setResultsName("instances") +
            Suppress(self.semi) ).setResultsName("gateDecl")

        udpInstance = Group( Group( identifier + Optional(self.range | self.size) ) +
            "(" + Group( delimitedList( self.expr ) ) + ")" )
        udpInstantiation = Group( identifier -
            Optional( driveStrength ) +
            Optional( delay ) +
            delimitedList( udpInstance ) +
            self.semi ).setName("udpInstantiation")

        self.namedPortConnection = Group( "." + identifier + "(" + Group(Optional(self.expr)) + ")" ).setResultsName("namedPortConnection")

        parameterValueAssignment = Group ( Suppress(Literal("#")) +
                                           Suppress("(") +
                                           Group( delimitedList(self.namedPortConnection) ) + # expresion is only for compatibility reasons
                                                                                              # if someone tries to TMR expresion (unnamed conenection)
                                                                                              # it will crash ....
                                           Suppress(")")
                                         ).setResultsName("parameterValueAssignment")

        moduleArgs = Group(
            Suppress("(") +
            Optional(
                delimitedList(
                    Optional(self.comment) +
                    (self.namedPortConnection | Group(self.expr).setResultsName("expr@unnamedPortConnection"))
                )
            ) +
            Optional(self.comment) + 
            Suppress(")")
        ).setResultsName("moduleArgs")

        moduleInstance = Group( Group ( identifier + Optional(self.range) ) + moduleArgs ).setResultsName("moduleInstance")

        self.moduleInstantiation = Group( Group(Optional(attributes)).setResultsName("attributes") +
                                          Group(identifier).setResultsName("moduleName") +
                                          Group(Optional( parameterValueAssignment )) +
                                          Group(delimitedList( moduleInstance )).setResultsName("moduleInstances") +
                                          Suppress(self.semi)
                                        ).setResultsName("moduleInstantiation")

        parameterOverride = Group( Keyword("defparam") + Group(delimitedList(self.assignment)).setResultsName("assignments") + Suppress(self.semi) ).setResultsName("defparam")

        specparamDecl = Group( "specparam" + delimitedList(self.assignment) + self.semi )

        pathDescr1 = Group( "(" + regReference + "=>" + regReference + ")" )
        pathDescr2 = Group( "(" + Group( delimitedList( regReference ) ) + "*>" +
                                  Group( delimitedList( regReference ) ) + ")" )
        pathDescr3 = Group( "(" + Group( delimitedList( regReference ) ) + "=>" +
                                  Group( delimitedList( regReference ) ) + ")" )
        pathDelayValue = Group( ( "(" + Group( delimitedList( mintypmaxExpr | self.expr ) ) + ")" ) |
                                 mintypmaxExpr |
                                 self.expr )
        pathDecl = Group( ( pathDescr1 | pathDescr2 | pathDescr3 ) + "=" + pathDelayValue + self.semi ).setName("pathDecl")

        portConditionExpr = Forward()
        portConditionTerm = Optional(unop) + regReference
        portConditionExpr << portConditionTerm + Optional( binop + portConditionExpr )
        polarityOp = oneOf("+ -")
        levelSensitivePathDecl1 = Group(
            if_ + Group("(" + portConditionExpr + ")") +
            regReference + Optional( polarityOp ) + "=>" + regReference + "=" +
            pathDelayValue +
            self.semi )
        levelSensitivePathDecl2 = Group(
            if_ + Group("(" + portConditionExpr + ")") +
            self.lpar + Group( delimitedList( regReference ) ) + Optional( polarityOp ) + "*>" +
                Group( delimitedList( regReference ) ) + self.rpar + "=" +
            pathDelayValue +
            self.semi )
        levelSensitivePathDecl = levelSensitivePathDecl1 | levelSensitivePathDecl2

        edgeIdentifier = posedge | negedge
        edgeSensitivePathDecl1 = Group(
            Optional( if_ + Group("(" + self.expr + ")") ) +
            self.lpar + Optional( edgeIdentifier ) +
            regReference + "=>" +
            self.lpar + regReference + Optional( polarityOp ) + ":" + self.expr + self.rpar + self.rpar +
            "=" +
            pathDelayValue +
            self.semi )
        edgeSensitivePathDecl2 = Group(
            Optional( if_ + Group("(" + self.expr + ")") ) +
            self.lpar + Optional( edgeIdentifier ) +
            regReference + "*>" +
            self.lpar + delimitedList( regReference ) + Optional( polarityOp ) + ":" + self.expr + self.rpar + self.rpar +
            "=" +
            pathDelayValue +
            self.semi )
        edgeSensitivePathDecl = edgeSensitivePathDecl1 | edgeSensitivePathDecl2

        edgeDescr = oneOf("01 10 0x x1 1x x0").setName("edgeDescr")

        scalarConst = Regex("0|1('[Bb][01xX])?")
        timCheckEventControl = Group( posedge | negedge | (edge + "[" + delimitedList( edgeDescr ) + "]" ))
        timCheckCond = Forward()
        timCondBinop = oneOf("== === != !==")
        timCheckCondTerm = ( self.expr + timCondBinop + scalarConst ) | ( Optional("~") + self.expr )
        timCheckCond << ( ( "(" + timCheckCond + ")" ) | timCheckCondTerm )
        timCheckEvent = Group( Optional( timCheckEventControl ) +
                                regReference +
                                Optional( "&&&" + timCheckCond ) )
        timCheckLimit = self.expr
        controlledTimingCheckEvent = Group( timCheckEventControl + regReference +
                                            Optional( "&&&" + timCheckCond ) )
        notifyRegister = identifier

        systemTimingCheck1 = Group( "$setup" +
            self.lpar + timCheckEvent + "," + timCheckEvent + "," + timCheckLimit +
            Optional( "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck2 = Group( "$hold" +
            self.lpar + timCheckEvent + "," + timCheckEvent + "," + timCheckLimit +
            Optional( "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck3 = Group( "$period" +
            self.lpar + controlledTimingCheckEvent + "," + timCheckLimit +
            Optional( "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck4 = Group( "$width" +
            self.lpar + controlledTimingCheckEvent + "," + timCheckLimit +
            Optional( "," + self.expr + "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck5 = Group( "$skew" +
            self.lpar + timCheckEvent + "," + timCheckEvent + "," + timCheckLimit +
            Optional( "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck6 = Group( "$recovery" +
            self.lpar + controlledTimingCheckEvent + "," + timCheckEvent + "," + timCheckLimit +
            Optional( "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck7 = Group( "$setuphold" +
            self.lpar + timCheckEvent + "," + timCheckEvent + "," + timCheckLimit + "," + timCheckLimit +
            Optional( "," + notifyRegister ) + self.rpar +
            self.semi )
        systemTimingCheck = (FollowedBy('$') + ( systemTimingCheck1 | systemTimingCheck2 | systemTimingCheck3 |
            systemTimingCheck4 | systemTimingCheck5 | systemTimingCheck6 | systemTimingCheck7 )).setName("systemTimingCheck")
        sdpd = if_ + Group("(" + self.expr + ")") + \
            ( pathDescr1 | pathDescr2 ) + "=" + pathDelayValue + self.semi

        specifyItem = ~Keyword("endspecify") +(
            specparamDecl |
            pathDecl |
            levelSensitivePathDecl |
            edgeSensitivePathDecl |
            systemTimingCheck |
            sdpd
            )

        package_import_item = Group(Group(identifier).setResultsName("id@packageName") + "::" + Group(identifier | "*").setResultsName("id@itemName")).setResultsName("package_import_item")
        package_import = Group(Keyword("import") + Group(delimitedList(package_import_item) + Suppress(self.semi)).setResultsName("packages")).setResultsName("package_import")

        modport_decl = Group(
            Keyword("modport") +
            Group(identifier).setResultsName("name") +
            Suppress("(") +
            Group(delimitedList(
                Group(
                    directions.setResultsName("dir") +
                    Group(delimitedList(
                        Group(identifier).setResultsName("id")
                    )).setResultsName("signals")
                )
            )).setResultsName("ports") +
            Suppress(")") + Suppress(self.semi)
        ).setResultsName("modport")

        interface_statement = (
            Group( self.standardTypeNetDecl | self.customTypeNetDecl ).setResultsName("signal") |
            modport_decl |
            timeunitsDecl
        )

        self.interfaceDecl = Group(
            Keyword("interface") +
            Group(identifier).setResultsName("id") +
            Suppress(self.semi) +
            Group(ZeroOrMore(interface_statement)).setResultsName("elems") +
            Keyword("endinterface")
        ).setResultsName("interfaceDecl")

        """
        x::= <specparam_declaration>
        x||= <path_declaration>
        x||= <level_sensitive_path_declaration>
        x||= <edge_sensitive_path_declaration>
        x||= <system_timing_check>
        x||= <sdpd>
        """
        specifyBlock = Group( "specify" + ZeroOrMore( specifyItem ) + "endspecify" )

        self.moduleOrGenerateItem = Forward()
        generate_module_named_block = Group(Suppress("begin") +
                                            Optional(":" + Group(identifier).setResultsName("id@begin")) +
                                            Group(ZeroOrMore(self.moduleOrGenerateItem)).setResultsName("members") +
                                            Suppress("end") +
                                            Optional(":" + Group(identifier).setResultsName("id@end"))
                                            ).setResultsName("generate_module_named_block")

        self.moduleOrGenerateItemOrNamed = self.moduleOrGenerateItem | generate_module_named_block

        generate_if_else_statement =  Group(
            Suppress(if_) +
            condition +
            Group(self.moduleOrGenerateItemOrNamed).setResultsName("if_stmt") +
            Optional(
                Suppress(else_) +
                Group(self.moduleOrGenerateItemOrNamed).setResultsName("else_stmt")
            )
        ).setResultsName("generate_if_else_statement")

        generate_case_item = Group(
            Group(
                (delimitedList( Group(self.expr) | self.range ) + Suppress(":")) |
                (default + Optional(Suppress(":")))
            ).setResultsName("case_item_expr") +
            Group(self.moduleOrGenerateItemOrNamed).setResultsName("case_item_body")
        ).setResultsName("generate_case_item")
        generate_case_statement = Group(
            Group(case).setResultsName("case_flavour") +
            Suppress("(") + Group(self.expr).setResultsName("expr@case") + Suppress(")") +
            Group(OneOrMore( generate_case_item | self.comment )).setResultsName("case_items") +
            endcase
        ).setResultsName("generate_case_statement")

        generate_for_statement = Group( Suppress(for_) +
            Suppress("(")  +
            Group(Optional( (self.assignment + Suppress(self.semi)) | self.genVarDeclWAssign|self.standardTypeNetDeclWAssign|self.customTypeNetDeclWAssign)).setResultsName("for1") +
            Group(self.expr).setResultsName("expr@for2") +
            Suppress(self.semi) +
            Group(Optional(self.assignment | self.incr_decr)).setResultsName("for3") +
            Suppress(")") +
            Group(self.moduleOrGenerateItemOrNamed).setResultsName("generateStmt") #generate_module_named_block
        ).setResultsName("generate_for_statement")

        self.moduleOrGenerateItem << (
            self.comment |
            self.compDirective |
            self.standardTypePortBody |
            self.standardTypeNetDecl |
            parameterDecl |
            customParameterDecl |
            self.continuousAssign |
            self.genVarDecl |
            package_import |
            self.structDecl |
            self.enumDecl |
            self.typedefDecl |
            self.unionDecl |
            self.gateDecl |
            parameterOverride |
            specifyBlock |
            initialStmt |
            self.alwaysStmt |
            task |
            functionDecl |
            self.standardTypeNetDeclWAssign |
            self.customTypeNetDeclWAssign |
            self.moduleInstantiation |
            generate_for_statement |
            generate_if_else_statement |
            generate_case_statement |
            self.customTypeNetDecl |
            self.customTypePortBody
        )

        generate_body =  OneOrMore(self.moduleOrGenerateItem)

        self.generate = Group( Suppress(Keyword("generate")) + generate_body  + Suppress(Keyword("endgenerate"))).setResultsName("generate")

        # Only allow comments as a standalong module item
        self.moduleItem = self.generate | self.moduleOrGenerateItem | timeunitsDecl

        """  All possible moduleItems, from Verilog grammar spec
        x::= <parameter_declaration>
        x||= <input_declaration>
        x||= <output_declaration>
        x||= <inout_declaration>
        ?||= <net_declaration>  (spec does not seem consistent for this item)
        x||= <reg_declaration>
        x||= <time_declaration>
        x||= <integer_declaration>
        x||= <real_declaration>
        x||= <event_declaration>
        x||= <gate_declaration>
        x||= <UDP_instantiation>
        x||= <module_instantiation>
        x||= <parameter_override>
        x||= <continuous_assign>
        x||= <specify_block>
        x||= <initial_statement>
        x||= <always_statement>
        x||= <task>
        x||= <function>
        """

        moduleHdr = Group ( oneOf("module macromodule") +
            identifier.setResultsName("id@moduleName") +
            Optional(package_import) +
            Optional(
                Suppress("#") +
                Suppress("(") +
                Group(
                    Optional(delimitedList(
                        Optional(self.comment) +
                        Group(
                            Optional(Group(Keyword("parameter") | Keyword("localparam")).setResultsName("param_type")) +
                            (standard_typed_parameter_declaration | custom_typed_parameter_declaration)
                        )
                    ) +
                    Optional(self.comment)
                )).setResultsName("parameterList") +
                Suppress(")")
            ) +
            modulePorts +
            Suppress(self.semi) ).setName("moduleHdr").setResultsName("moduleHdr")
        
        self.endmodule = Keyword("endmodule").setResultsName("endModule")

        self.module = Group(
                 Group(Optional(attributes)).setResultsName("attributes") +
                 moduleHdr +
                 Group( ZeroOrMore( self.moduleItem ) ).setResultsName("moduleBody") +
                 self.endmodule +
                 Group(Optional(":" + identifier)) ).setName("module").setResultsName("module")

        udpDecl = self.standardTypePortBody | self.standardTypeNetDecl | self.customTypeNetDecl | self.customTypePortBody
        udpInitVal = (Regex("1'[bB][01xX]") | Regex("[01xX]")).setName("udpInitVal")
        udpInitialStmt = Group( "initial" +
            identifier + "=" + udpInitVal + self.semi ).setName("udpInitialStmt")

        levelSymbol = oneOf("0   1   x   X   ?   b   B")
        levelInputList = Group( OneOrMore( levelSymbol ).setName("levelInpList") )
        outputSymbol = oneOf("0   1   x   X")
        combEntry = Group( levelInputList + ":" + outputSymbol + self.semi )
        edgeSymbol = oneOf("r   R   f   F   p   P   n   N   *")
        edge = Group( "(" + levelSymbol + levelSymbol + ")" ) | \
               Group( edgeSymbol )
        edgeInputList = Group( ZeroOrMore( levelSymbol ) + edge + ZeroOrMore( levelSymbol ) )
        inputList = levelInputList | edgeInputList
        seqEntry = Group( inputList + ":" + levelSymbol + ":" + ( outputSymbol | "-" ) + self.semi ).setName("seqEntry")
        udpTableDefn = Group( "table" +
            OneOrMore( combEntry | seqEntry ) +
            "endtable" ).setName("table")

        """
        <UDP>
        ::= primitive <name_of_UDP> ( <name_of_variable> <,<name_of_variable>>* ) ;
                <UDP_declaration>+
                <UDP_initial_statement>?
                <table_definition>
                endprimitive
        """
        udp = Group( "primitive" + identifier +
            "(" + Group( delimitedList( identifier ) ) + ")" + self.semi +
            OneOrMore( udpDecl ) +
            Optional( udpInitialStmt ) +
            udpTableDefn +
            "endprimitive" )

        self.packageItem = self.structDecl | self.enumDecl | self.typedefDecl | self.unionDecl | customParameterDecl | parameterDecl | self.compDirective | timeunitsDecl | package_import

        self.package = Group(
            Keyword("package") + Group(identifier).setResultsName("id@packageName") + self.semi +
            Group( ZeroOrMore( self.packageItem ) ).setResultsName("packageBody") +
            Keyword("endpackage")
        ).setResultsName("packageDecl")

        self.verilogbnf = (OneOrMore( self.module | self.structDecl | self.enumDecl | self.typedefDecl | self.unionDecl | self.interfaceDecl | self.package | udp | self.compDirective | self.comment) + StringEnd()).setName("top").setResultsName("top")

        self.tmrg_translate     = Regex(r"//\s*tmrg\s+translate\s+off\s*\n(?P<content>[\s\S]*?)//\s*tmrg\s+translate\s+on\s*")
        self.tmrg_copy          = Regex(r"//\s*tmrg\s+copy\s+start\s*\n(?P<content>[\s\S]*?)//\s*tmrg\s+copy\s+stop\s*"      )
        self.tmrg_ignore        = Regex(r"//\s*tmrg\s+ignore\s+start\s*\n(?P<content>[\s\S]*?)//\s*tmrg\s+ignore\s+stop\s*"  )
        self.pragma_include     = Regex(r"\s*`include\s+(\"*)(?P<name>[^\n]+)\1", flags=re.MULTILINE)
        self.inline_comment     = Regex(r"(?<!//)(?P<preserve>[^ \t])[ \t]+?//(?P<comment>.+)")
        self.non_pragma_comment = Regex("//(?!(\\s*" + "\\b|\s*".join(["tmrg"] + self.pragmas + self.extra_pragmas) + "\\b)).*")

    def preParsingTranstorm(self, text):
        def remove_inline_comment(text):
            words = text.get("comment").split()
            if len(words) and words[0] in ["tmrg"] + self.pragmas + self.extra_pragmas:
                logging.debug("Preserving inline comment, as it is a valid `%s` pragma: %s" % (words[0], text.get("comment")))
                return text

            logging.debug("Removing inline comment: %s" % text.get("comment"))
            return text.get("preserve")

        def remove_non_pragma_comment(text):
            logging.debug("Removing comment: %s" % text)
            return ""

        def ignore_content_function(text):
            logging.debug("Removing:\n%s\n---" % "\n".join(["> " + x for x in text.get("content").splitlines()]))
            return ""

        def copy_content_function(text):
            next_idx = len(self.as_is_buffers)
            logging.debug("Copying content into index %d:\n%s\n---" % (next_idx, "\n".join(["> " + x for x in text.get("content").splitlines()])))
            self.as_is_buffers.append(text.get("content"))
            return "// tmrg as_is_index " + str(next_idx) + "\n"

        def include_function(text):
            if not self.include:
                return text
            
            filename = text.get("name").strip()

            logging.info("Including '%s'" % filename)
            for dir in self.inc_dir:
                fullname=os.path.join(dir, filename)
                if not os.path.isfile(fullname):
                    continue

                logging.info("File '%s' found in '%s'" % (filename, fullname))
                with open(fullname) as fin:
                    fcontent = fin.read()

                text = fcontent
                return text + "\n"

            logging.warning("File '%s' not found. Retaining include directive as it is" % (filename))
            return text

        self.tmrg_translate.setParseAction(ignore_content_function)
        self.tmrg_ignore.setParseAction   (ignore_content_function)
        self.tmrg_copy.setParseAction     (copy_content_function)
        self.inline_comment.setParseAction(remove_inline_comment)
        if not VerilogParser.comments:
            self.non_pragma_comment.setParseAction(remove_non_pragma_comment)
        cStyleComment.setParseAction(remove_non_pragma_comment)
        self.pragma_include.setParseAction(include_function)

        while True:
            text_with_includes = self.pragma_include.transformString(text)
            if text_with_includes == text:
                break
            text = text_with_includes
        text = self.tmrg_translate.transformString(text)
        text = self.tmrg_ignore.transformString(text)
        text = self.tmrg_copy.transformString(text)
        text = self.inline_comment.transformString(text)
        if not VerilogParser.comments:
            text = self.non_pragma_comment.transformString(text)
        text = cStyleComment.transformString(text)

        self.tmrg_translate.setParseAction()
        self.tmrg_ignore.setParseAction   ()
        self.tmrg_copy.setParseAction     ()
        self.inline_comment.setParseAction()
        self.non_pragma_comment.setParseAction()
        self.pragma_include.setParseAction()

        return text

    def parseString(self, text):
        self.verilog = self.preParsingTranstorm(text)
        logging.debug("Preparsing done. Starting parsing.")

        try:
            self.tokens = self.verilogbnf.parseString(self.verilog)
        except:
            for i,l in enumerate(self.verilog.split("\n")):
                logging.debug("[%d] %s"%(i,l))
            raise

        return self.tokens
    
    def parseFile(self,fname):
        logging.debug("Parsing file '%s'" % fname)
        self.fname=fname

        with open(fname,"r") as f:
            body=f.read()
            return self.parseString(body)

if __name__=="__main__":
    print("This module is not ment to be run!")
