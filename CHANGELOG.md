This file contains the updates of on the version.
It follows these [rules](https://keepachangelog.com/en/1.0.0/).
The script handling the release, uses it as parsing rules, so please maintain formatting.

The latest version (highest in this file) is the running version.
When updating it, keep in mind.
A [template](#[template]) is available at the end with the different sections.

## [1.1.0]
### Added
- Documentation and testing for the supported SV feature set
- Support for `struct` and `enum` SV constructs via `typedef`
- Support for structure and enumeration definition both inside and outside modules
- Support for structure and enumeration module ports
- Support for multiple dimensions in packed vectors and unpacked arrays
- Support for time precision specification in delays
- Support for `sync/async_set_reset` synthesis pragmas
- Support for unnamed gate instantiations
- Voted signals can be specified also via `assign` statements. This can be used to vote unpacked arrays.
- Test cases for the features above
- Test cases for reg initialization in declaration
- Test case for left-hand side concatenation with assign
- [Issue 80](https://gitlab.cern.ch/tmrg/tmrg/-/issues/80): Basic support for inline attributes
- Test case for [Issue 86](https://gitlab.cern.ch/tmrg/tmrg/-/issues/86)
- Test case class with comparison vs golden reference
- [Issue 83](https://gitlab.cern.ch/tmrg/tmrg/-/issues/83): pylint checks in CI
- [MR 174](https://gitlab.cern.ch/tmrg/tmrg/-/merge_requests/174): extras (dependencies) definition for developers
- [Issue 190](https://gitlab.cern.ch/tmrg/tmrg/-/merge_requests/190): Add basic sanity checks for wrg
- Test cases for [Issue 93](https://gitlab.cern.ch/tmrg/tmrg/-/issues/93) and [Issue 94](https://gitlab.cern.ch/tmrg/tmrg/-/issues/94)
- Test case for having the same file input file multiple times
- Support for `package` and inline `import` in module headers. Types and parameters defined therein are imported in the modules.
- Namespace classes using `__slots__` to prevent the creation of unforeseen attributes
- Added GlobalNamespace class to hold globally shared types and params
- Added test case for importing triplicated library
- Added test case to check for preservation of param/import at the start of the modules
- Provisional support for cPyparsing to increase tmrg speed
- Added `functools` requirement for caching support, and `functools32` backport for python2
- Added support for general typedefs
- Supporting array/struct assignment patterns with explicitly named field
- [Issue 99](https://gitlab.cern.ch/tmrg/tmrg/-/issues/99): parameters and localparams support for custom types
- Module declarations can have both parameter and localparams
- Sizes of builtin types are hardcoded and are available for voter size calculation
- Test cases for [Issue 110](https://gitlab.cern.ch/tmrg/tmrg/-/issues/110)
- [Issue 115](https://gitlab.cern.ch/tmrg/tmrg/-/issues/115): support for streaming expressions
- [Issue 118](https://gitlab.cern.ch/tmrg/tmrg/-/issues/118): Adding support for case/if modifiers (priority, unique, unique0)
- [Issue 119](https://gitlab.cern.ch/tmrg/tmrg/-/issues/119): adding support for macros in size specification for literal numbers
- Added support for concatenation at the LHS of assignments and relative test cases
- [Issue 121](https://gitlab.cern.ch/tmrg/tmrg/-/issues/121): support for timeunits declarations
- Added support for local variables and genvar name conflicts with nets
- [Issue 126](https://gitlab.cern.ch/tmrg/tmrg/-/issues/126): adding support for package import inside packages
- [Issue 133](https://gitlab.cern.ch/tmrg/tmrg/-/issues/133): Added support for local variables in nested begin-end blocks
- Hierarchical defparam support
- Added support for code retaining through the `copy start/stop` tmrg directive
- Added alias for tmrg directives `translate off/on` : `ignore start/stop`
- Added a shortcut for --no-header --no-common-definitions: -b/--bare-minimum
- Added formatting for `wait` statement
- [Issue 145](https://gitlab.cern.ch/tmrg/tmrg/-/issues/145): Added support for custom pragmas via -P/--pragmas, added superlint to default ones
- Added support for single line comment retaining via -C/--comments
- [Issue 144](https://gitlab.cern.ch/tmrg/tmrg/-/issues/144): Added support for `genearte case`
- [Issue 142](https://gitlab.cern.ch/tmrg/tmrg/-/issues/142): Added support for `case inside` in both generate and statement blocks
- [Issue 148](https://gitlab.cern.ch/tmrg/tmrg/-/issues/148): Added support for comments among port declarations and instantiations
- Added support for net lifetime specification
- [Issue 150](https://gitlab.cern.ch/tmrg/tmrg/-/issues/150): Added support for namespace in function calls
- Added support for unions

### Changed
- Rely on `pyparsing` dependency
- Object deepcopy is now *Pickle*-based
- Elaboration, Triplication and Formatting relies more heavily on named parsing results, instead of positional
- Improved reusability and modularity by removing and unifying redundant code
- Using objects instead of lists to ease code maintainability, readability and reusability
- Improved expressions readability
- Introduced Module class to store module attributes, instead of dictionary
- Introduced File class to store filenames and tokens, instead of dictionary
- Removed unnecessary ternary logic from start/stop values in generate loops for voters/fanouts, when they are integers
- Introduced Namespace class, switched from `current_module` to `current_namespace`
- speed up tmr phase by switching `deepcopy` strategy for ParseResults to a custom tree-traversing one with caching support
- Unified parameter/localparam parsing elements, created a new one for custom typed parameters/localparams
- Reg reference checking is performed both for lhs and rhs
- custom types become a ParseResults group everywhere and are treated as such during elaboration and formatting
- introduced `get_data_type` method in `Signal` class
- type-related functions are now methods of the `Type` class
- caching during the triplication phase help reduce runtime and warnings (if any)
- [Issue 73](https://gitlab.cern.ch/tmrg/tmrg/-/issues/73): improved usability for formal fault injections by providing dut and tb name
- [Issue 74](https://gitlab.cern.ch/tmrg/tmrg/-/issues/74): improved usability for formal fault injections by wrapping blocks of code in ifdef
- [Issue 125](https://gitlab.cern.ch/tmrg/tmrg/-/issues/125): support for local variable declarations in begin/end blocks
- skipped union size checks when one of the fields doesn't have an integer size

### Fixed
- [Issue 6](https://gitlab.cern.ch/tmrg/tmrg/-/issues/6): Reg with initial value is not supported
- [Issue 82](https://gitlab.cern.ch/tmrg/tmrg/-/issues/82): prevented crash on _is_enum_element
- [MR 175](https://gitlab.cern.ch/tmrg/tmrg/-/merge_requests/175): various errors reported by pylint
- [Issue 86](https://gitlab.cern.ch/tmrg/tmrg/-/issues/86): wrong tmr error signal naming
- [Issue 88](https://gitlab.cern.ch/tmrg/tmrg/-/issues/88): end label now correctly placed
- [Issue 89](https://gitlab.cern.ch/tmrg/tmrg/-/issues/89): function declarations are not triplicated, return statement ends with semicolon
- [Issue 92](https://gitlab.cern.ch/tmrg/tmrg/-/issues/92): tbg crashes for modules with parameters
- [Issue 93](https://gitlab.cern.ch/tmrg/tmrg/-/issues/93): globally defined types after module declaration fail to elaborate
- [Issue 94](https://gitlab.cern.ch/tmrg/tmrg/-/issues/94): file re-ordering previous to elaboration causes failure
- [Issue 97](https://gitlab.cern.ch/tmrg/tmrg/-/issues/97): error in triplicating unpacked array declared with assignment
- Elaboration checks for `reg_reference`
- [Issue 98](https://gitlab.cern.ch/tmrg/tmrg/-/issues/98): performance issues with latest releases
- signed/unsigned modifiers are to be placed after the data type, not before
- [Issue 101](https://gitlab.cern.ch/tmrg/tmrg/-/issues/101): WRG/TBG does not define correctly the voter width
- [Issue 102](https://gitlab.cern.ch/tmrg/tmrg/-/issues/102): packages cannot contain preprocessor macros
- [Issue 81](https://gitlab.cern.ch/tmrg/tmrg/-/issues/81): unpacked array declarations retain size/range style
- [Issue 106](https://gitlab.cern.ch/tmrg/tmrg/-/issues/106): crashes when referencing a field of an undeclared structure
- [Issue 107](https://gitlab.cern.ch/tmrg/tmrg/-/issues/107): incorrect syntax for custom types in parameter list
- [Issue 111](https://gitlab.cern.ch/tmrg/tmrg/-/issues/111): enum in FSM state produces warnings when elaborating in xcelium
- `typedef enum` formatting doesn't retain custom data types
- [Issue 110](https://gitlab.cern.ch/tmrg/tmrg/-/issues/110): localparam reordering with custom types causes undefined error
- tbg no longer log error messages if only top-level module is provided as input
- [Issue 112](https://gitlab.cern.ch/tmrg/tmrg/-/issues/112): real numbers with underscore cause parsing error
- [Issue 113](https://gitlab.cern.ch/tmrg/tmrg/-/issues/113): unsupported empty parameter list
- [Issue 117](https://gitlab.cern.ch/tmrg/tmrg/-/issues/117): tmrg translate directive working properly also outside modules
- [Issue 128](https://gitlab.cern.ch/tmrg/tmrg/-/issues/128): pylint possibly-used-before-assignment warnings
- [Issue 129](https://gitlab.cern.ch/tmrg/tmrg/-/issues/129): synopsys pragmas breaking triplication
- [Issue 127](https://gitlab.cern.ch/tmrg/tmrg/-/issues/127): support for multiple instances with same name
- [Issue 130](https://gitlab.cern.ch/tmrg/tmrg/-/issues/130): generate loop item triplication error for single (non begin-end) statements
- [Issue 134](https://gitlab.cern.ch/tmrg/tmrg/-/issues/134): re-declaration of triplicated signals, due to fanin net triplication
- [Issue 135](https://gitlab.cern.ch/tmrg/tmrg/-/issues/135): begin-end blocks created during triplication miss a newline character
- [Issue 136](https://gitlab.cern.ch/tmrg/tmrg/-/issues/136): forever now correctly formatted
- [Issue 132](https://gitlab.cern.ch/tmrg/tmrg/-/issues/132): tmrg removes cadence pragmas
- [Issue 139](https://gitlab.cern.ch/tmrg/tmrg/-/issues/139): missing formatting for continuous assignment delay spec
- defparam triplication takes into account both triplicate and do_not_touch
- [Issue 138](https://gitlab.cern.ch/tmrg/tmrg/-/issues/138): support for task lifetime
- [Issue 143](https://gitlab.cern.ch/tmrg/tmrg/-/issues/143): casting operators can be followed by other operations
- [Issue 146](https://gitlab.cern.ch/tmrg/tmrg/-/issues/146): parser mistakenly matches last (not first) `//` for inline comments
- [Issue 147](https://gitlab.cern.ch/tmrg/tmrg/-/issues/147): timeunits must remain in place after triplication
- [Issue 137](https://gitlab.cern.ch/tmrg/tmrg/-/issues/137): insert begin/end around triplicated single statements in generate blocks
- [Issue 152](https://gitlab.cern.ch/tmrg/tmrg/-/issues/152): support task/function calls with empty parentheses
- union field size checks formatting error
- [Issue 153](https://gitlab.cern.ch/tmrg/tmrg/-/issues/153): Parser struggles with multiple include files

### Deprecated
- [MR 179](https://gitlab.cern.ch/tmrg/tmrg/-/merge_requests/179): psyco support

### Removed
- Local copies of `pyparsing`.
- Mention of MIC cluster installation (not as it is not maintained anymore)
- Prune unnecessary imports
- Custom types no longer accept modifiers, which are not SV-compliant
- paramAssgnmt has been replaced with general purpose assignment
- forbidden character and pre-parsing of tmrg/synopsys/cadence directives and preprocessor macros

## [1.0.0]
### Added
- Changelog
- Release script
- Limited support for SystemVerilog
- Verible in CI for SystemVerilog files
- Testcase for DFF with asynchronous reset when using SystemVerilog `always_ff` construct [Issue 50](https://gitlab.cern.ch/tmrg/tmrg/-/issues/50)
- Support for Verilog `param` construct in libraries
- [Issue 57](https://gitlab.cern.ch/tmrg/tmrg/-/issues/57): Support for Verilog attributes
- [MR 167](https://gitlab.cern.ch/tmrg/tmrg/-/merge_requests/167): python-gitlab package to ubuntu20 docker

### Changed
- Increased python recursion limit to 4000
- Fix the TMRG paper reference in the README file

### Fixed
- [Issue 26](https://gitlab.cern.ch/tmrg/tmrg/-/issues/26): improved testing for `casez`/`casex` construct
- [Issue 25](https://gitlab.cern.ch/tmrg/tmrg/-/issues/25): SystemVerilog labelled `endmodule` construct support
- [Issue 28](https://gitlab.cern.ch/tmrg/tmrg/-/issues/28): SystemVerilog `logic` construct support
- [Issue 29](https://gitlab.cern.ch/tmrg/tmrg/-/issues/29): SystemVerilog `always_ff` construct support
- [Issue 31](https://gitlab.cern.ch/tmrg/tmrg/-/issues/31): SystemVerilog `always_comb` construct support
- [Issue 32](https://gitlab.cern.ch/tmrg/tmrg/-/issues/32): SystemVerilog `always_latch` construct support
- [Issue 33](https://gitlab.cern.ch/tmrg/tmrg/-/issues/33): SystemVerilog `import <package>::<parameter>` construct support
- [Issue 27](https://gitlab.cern.ch/tmrg/tmrg/-/issues/27): SystemVerilog `unique case` construct support
- [Issue 36](https://gitlab.cern.ch/tmrg/tmrg/-/issues/36): SystemVerilog testing using Verible
- [Issue 34](https://gitlab.cern.ch/tmrg/tmrg/-/issues/34): SystemVerilog `for` construct support
- [Issue 38](https://gitlab.cern.ch/tmrg/tmrg/-/issues/38): SystemVerilog `'0` construct on right-hand side support
- [Issue 43](https://gitlab.cern.ch/tmrg/tmrg/-/issues/43): SystemVerilog `int'()` cast construct in `for`-loop support
- [Issue 40](https://gitlab.cern.ch/tmrg/tmrg/-/issues/40): SystemVerilog `size`-definition of unpacked array construct support
- [Issue 41](https://gitlab.cern.ch/tmrg/tmrg/-/issues/41): SystemVerilog unpacked array construct in ports declaration support
- [Issue 42](https://gitlab.cern.ch/tmrg/tmrg/-/issues/42): SystemVerilog unpacked array of packed arrays in port definition construct support
- [Issue 44](https://gitlab.cern.ch/tmrg/tmrg/-/issues/44): SystemVerilog voting of unpacked array of packed arrays construct support
- [Issue 45](https://gitlab.cern.ch/tmrg/tmrg/-/issues/45): SystemVerilog localparam construct support
- [Issue 48](https://gitlab.cern.ch/tmrg/tmrg/-/issues/48): SystemVerilog genvar in for-loop was not correctly triplicated
- [Issue 47](https://gitlab.cern.ch/tmrg/tmrg/-/issues/47): SystemVerilog package parameter in module IO width support
- [Issue 46](https://gitlab.cern.ch/tmrg/tmrg/-/issues/46): SystemVerilog right-hand side cast to certain number of bits construct support
- [Issue 49](https://gitlab.cern.ch/tmrg/tmrg/-/issues/49): SystemVerilog right-hand side cast to certain number of bits construct support
- [Issue 53](https://gitlab.cern.ch/tmrg/tmrg/-/issues/53): SystemVerilog unpacked array of packed array as `localparam` construct support
- [Issue 52](https://gitlab.cern.ch/tmrg/tmrg/-/issues/52): Improved reproducibility across different systems
- [Issue 58](https://gitlab.cern.ch/tmrg/tmrg/-/issues/58): Expressions in case items are not output correctly
- [Issue 59](https://gitlab.cern.ch/tmrg/tmrg/-/issues/59): Blocking procedural assingment
- [Issue 60](https://gitlab.cern.ch/tmrg/tmrg/-/issues/60): TMRG silently ignores buf and similar primitives
- [Issue 62](https://gitlab.cern.ch/tmrg/tmrg/-/issues/62): Add support for integer parameter type
- [Issue 63](https://gitlab.cern.ch/tmrg/tmrg/-/issues/63): Add lifetime for functions
- [Issue 64](https://gitlab.cern.ch/tmrg/tmrg/-/issues/64): Add integer type for inline assignment
- [Issue 67](https://gitlab.cern.ch/tmrg/tmrg/-/issues/67): Add support for label after end statement
- [Issue 56](https://gitlab.cern.ch/tmrg/tmrg/-/issues/56): TBG does not correctly recognises unpacked arrays as ports

### Deprecated

### Removed


## [0.0.0]
### Added
Original version of tmrg before releases were introduced

## [Template]
### Added
- new features
### Changed
- changes in existing functionality
### Deprecated
- soon-to-be removed features
### Removed
- now removed features
### Fixed
- any bug fixes
### Security
- vulnerability addressed
